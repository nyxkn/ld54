#!/usr/bin/python

from docopt import docopt
import subprocess
import os
import sys

# for the game name, use only alphanum and underscore: [a-zA-Z0-9_]
GAME_NAME = "corridor-duels"
ITCH_USER = "nyxkn"

EXPORT_PATH = "../distrib/builds"
PROJECT_PATH = "../godot"

presets = {
        "web": {
            'ext': ".html"
            },
        "linux": {
            'ext': ".x86_64"
            },
        "win": {
            'ext': ".exe"
            },
        # "android": {
        #     'ext': ".apk"
        #     },
        }


# this then contains a subfolder for every preset
def get_build_path(build):
    return EXPORT_PATH + "/" + build


# get the full path of the exported preset
def get_export_path(build, preset):
    return EXPORT_PATH + "/" + build + "/" + preset


# where the actual build files are
def get_build_content_path(build, preset):
    return get_build_path(build) + "/" + preset


def get_executable_name(preset):
    executable_name = ''

    if preset == "web":
        executable_name = "index"
    else:
        executable_name = GAME_NAME
    executable_name += presets[preset]['ext']

    return executable_name


def get_git_version():
    build = ''

    process = subprocess.run(["git", "describe"], capture_output=True)
    git_tag = process.stdout.rstrip().decode()
    if git_tag:
        build = git_tag
        print("git tag: " + build)
    else:
        # build name if git describe doesn't return anything
        build = 'build'

    return build


def build(preset, custom_build_name):
    # determine build name
    build_name = ''
    if custom_build_name:
        build_name = custom_build_name
    else:
        build_name = get_git_version()

    executable_name = get_executable_name(preset)
    build_path = get_build_path(build_name)

    # we store this path segment just to check if the directory exists later
    export_dir = build_path + "/" + preset
    # godot export wants the full path of the executable
    export_fullpath = export_dir + "/" + executable_name

    print("Building preset [" + preset + "] with build name [" + build_name + "]")

    if os.path.isdir(export_dir):
        print("ERROR: build directory already exists (" + export_dir + ")")

        prompt_text = "Should we overwrite? (y/N)"
        answer = "N"
        if yes_to_all:
            print(prompt_text)
            answer = "y"
        else:
            answer = input(prompt_text)

        if answer == "y":
            print("Deleting " + export_dir + "\n")
            # you can add -I for interactive
            subprocess.run(["rm", "-rv", export_dir])
        else:
            print("\nAborting\n")
            exit()

    build_cmd = "--export-release"
    if debug_build:
        build_cmd = "--export-debug"

    # print("Exporting to " + export_fullpath)
    subprocess.run(["mkdir", "-p", export_dir])

    print("\n======== GODOT EXPORT START ========\n")
    subprocess.run(["godot", "--path", PROJECT_PATH, build_cmd, preset, export_fullpath])
    print("\n======== GODOT EXPORT END ========\n")

    return build_name


def distrib(preset, custom_build_name, tag):
    build_name = build(preset, custom_build_name)
    push(build_name, preset, tag)


def push(build_name, preset, tag):
    export_path = get_export_path(build_name, preset)
    channel = preset
    if tag:
        channel = tag + "-" + channel

    itch_project = ITCH_USER + "/" + GAME_NAME
    push_target = itch_project + ":" + channel

    print("========\n")
    print("Pushing build " + build_name + " [" + preset + "]")
    print("\n---")
    print("Project: " + itch_project + "\nChannel: " + channel +
          "\nUserversion: " + build_name)
    print("---\n")

    prompt_text = "Does this look right? (y/N)"
    answer = "N"
    if yes_to_all:
        print(prompt_text)
        answer = "y"
    else:
        answer = input(prompt_text)

    if answer == "y":
        print("OK! Proceeding")
    else:
        print("\nAborting")
        exit()

    subprocess.run(["butler", "push", export_path, push_target, "--userversion", build_name])


def archive(build, preset, tag):
    build_path = get_build_path(build)
    build_content_path = get_build_content_path(build, preset)

    archive_name = GAME_NAME + "-" + (tag if tag else build) + "-" + preset
    archive_path = get_build_path(build) + "/" + archive_name
    archive_path_zip = archive_path + ".zip"

    print("Archiving build " + build + " [" + preset + "]")

    if not os.path.isdir(build_content_path):
        print("ERROR: folder [" + build_content_path + "] does not exist.")
        print("Skipping\n")
        return

    if os.path.islink(archive_path):
        print("ERROR: build archive symlink already exists (" + archive_path + ")")
        print("Skipping\n")
        return

    if os.path.isfile(archive_path_zip):
        print("ERROR: build archive zipfile already exists (" + archive_path_zip + ")")
        print("Skipping\n")
        return

    print("Archiving as " + archive_path)
    subprocess.run(["ln", "-sr", build_content_path, archive_path])
    subprocess.run(["zip", "-r", archive_name + ".zip", archive_name], cwd=build_path)
    subprocess.run(["rm", archive_path])



doc = """Export helper for godot

Usage:
    distrib.py build <preset> [--name=<name>] [-y] [-d]
    distrib.py distrib <preset> [--name=<name>] [--tag=<tag>] [-y]
    distrib.py push <build> <preset> [--tag=<tag>] [-y]
    distrib.py archive <build> <preset> [--tag=<tag>]
    distrib.py (-h | --help)
    distrib.py --version


build
    export the specified preset
    name is an optional custom name for the build

distrib
    runs build and then push all in one go
    takes the same parameters as build, plus an optional tag for pushing

push
    push a build folder to itch.io
    the format of the archive name is gamename-channel.zip
    channel is the preset name with an optional tag prefix (tag-preset)

archive
    archive an existing build
    the format of the archive name is gamename-build-preset.zip
    the optional tag argument will replace the build string


Arguments:
    <build>     the name of the build to select
    <preset>    export preset as named in export_presets.cfg
                also accepts "all" to build all available presets


Options:
    -y              answer yes to all prompts
    -d              debug build instead of default release
    --name=<name>   name is an optional custom name for the build
    --tag=<tag>     a custom tag
                    in archive: tag replaces the build name of the generated archive
                    in push: tag is prepended to the channel name


Examples:
    distrib.py build all
    distrib.py build all --name=ludum-dare
    distrib.py archive 1.0 linux --tag=ld50
    distrib.py push 1.0 linux --tag=ld50

"""


# debug_print = True
debug_print = False
yes_to_all = False
debug_build = False

if __name__ == '__main__':
    args = docopt(doc, version='Export 1.1.0')
    if debug_print:
        print(args)
        print("--------\n")

    build_mode = args['build']
    archive_mode = args['archive']
    push_mode = args['push']
    distrib_mode = args['distrib']

    build_name = args['<build>']
    preset_name = args['<preset>']

    # also_archive = args['-a']
    # also_archive = False
    yes_to_all = args['-y']
    debug_build = args['-d']
    name = args['--name']
    tag = args['--tag']

    # enable this after the jam is over
    if name == "ludum-dare":
        print("Ludum Dare builds are closed")
        exit()

    if not preset_name in presets.keys() and preset_name != "all":
        print("Provided preset name [" + preset_name + "] does not exist")
        exit()

    selected_presets = [preset_name]
    if preset_name == "all":
        selected_presets = presets.keys()

    for preset in selected_presets:
        if build_mode:
            build(preset, name)
        elif archive_mode:
            archive(build_name, preset, tag)
        elif push_mode:
            push(build_name, preset, tag)
        elif distrib_mode:
            distrib(preset, name, tag)
