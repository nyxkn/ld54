#!/bin/sh

script_dir=$(dirname "$(realpath "$0")")
cd "$script_dir" || exit

# ./distrib.py build android -d -y && find builds/build -name "*.apk" -exec adb install --user 0 {} \;
./distrib.py build android -d -y && find builds/build -name "*.apk" -exec adb install {} \;
