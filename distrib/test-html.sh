#!/bin/sh

script_dir=$(dirname "$(realpath "$0")")
cd "$script_dir" || exit

echo $script_dir

serve_script="$HOME/apps/godot-nyxkn/platform/web/serve.py"
# serve="$serve_script --no-browser --port 8060 --root $script_dir/builds/build/web"
serve="$serve_script --port 8060 --root $script_dir/builds/build/web"
# python -m http.server 8000 --directory builds/build/web

if [[ "$1" = "serve" ]]; then
	$serve
else
	./distrib.py build web -d -y && $serve
fi
