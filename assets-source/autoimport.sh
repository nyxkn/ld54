#!/bin/bash

# https://superuser.com/questions/181517/how-to-execute-a-command-whenever-a-file-changes

# this command returns the git repository root path
root_dir="$(git rev-parse --show-toplevel)"
[[ "$root_dir" ]] || root_dir=".."

# setup_monitor()
# args:
#   monitor_dir: relative path of folder to monitor for changes
#   extension: extension to consider for moving to assets
#   assets_subfolder: name of subfolder relative to godot/assets/
function setup_monitor {
    monitor_dir="$1"
    extension="$2"
    assets_subfolder="$3"

    assets_dir="$root_dir/godot/assets/$assets_subfolder"
    mkdir -p "$assets_dir"

    echo "setting up monitoring for $extension files at $monitor_dir"

    # move everything that needs to be moved
    # -i flag doesn't seem to work properly when * matches multiple files
    # this doesn't take care of subfolders so it's incompatible with the notifywait move behaviour
    # if you forget to run this script before you create a file, recreate or move manually
    # find "$monitor_dir" -maxdepth 1 -name "*$extension" -exec mv {} "$assets_dir" \;

    inotifywait -q -r -e moved_to,close_write -m "$monitor_dir" |
    while read -r directory events filename; do
        if [[ "$filename" == *"$extension" ]]; then
            printf -v timestamp "[%s]" "$(date +%T.%3N)"

            # echo "$timestamp" "[$monitor_dir]" "$filename" "->" "assets/$assets_subfolder"
            subdir=$(echo "$directory" | cut -d/ -f2-)
            echo "$timestamp" "[$monitor_dir]" "$directory" "$subdir" "$filename"
            mkdir -p "$assets_dir/$subdir"
            mv "$directory$filename" "$assets_dir/$subdir"
        fi
    done
}

setup_monitor "krita" ".png" "krita" &
setup_monitor "krita/export" ".png" "krita" &
setup_monitor "aseprite" ".png" "aseprite" &
#setup_monitor "$HOME/projects/ardour/ld52/export" ".wav" "sfx" &
#setup_monitor "$HOME/projects/reaper/ld52" ".ogg" "music" &

# keeps the script alive instead of exiting
# useful for closing it later with ctrl-c
# otherwise you have to hunt for the various processes that get created
wait
