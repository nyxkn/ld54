extends Node

# things to do at startup


# setup config values in here. just a convenience function to quickly change settings for release
func config_setup():
	Log.i("config values set")
	Config.save_cfg()


# load things in here
func load_things():
	Log.d("loading things")

	SoundManager.load_samples_directory("res://assets/sfx/")

	# load particles and save them. this is to make sure all of the latest changes have been saved
	# take note that the save only runs in debug builds
	var ParticlesWorkspace = preload("res://tools/particles_workspace/particles_workspace.tscn")
	var pw = ParticlesWorkspace.instantiate()
	pw.visible = false
	# to get _ready to run and save properly we have to add_child. then we remove immediately
	add_child(pw)
	pw.queue_free()

	# load test images. 10 files for about 200mb total
#	for i in range(1, 10):
#		# $TextureRect.texture = load("res://assets/temp/chan/" + str(i) + ".png")
#		load("res://assets/temp/chan/" + str(i) + ".png")
#		print("loaded asset " + str(i))


# things to do after loading is finished
func _loading_finished():
	G.totaltime_stopwatch.start()
