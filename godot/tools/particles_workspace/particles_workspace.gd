@tool
extends Node2D

## simply duplicate particle0 as your starting point
## then reload the scene with c-s-r to save
## set one_shot to true when you're using the particles
## the reason each particle should be inside a node is so that you can move the node2d freely
## without it affecting the transform of the saved particle


func _ready() -> void:
	# XXX take care that if you edit a particle and don't run the build it won't save
	# for this reason we are temporarily loading this scene in main.gd
	# you can also manually reload the scene to save
	if Base.debug_build:
		save_all_in(self)


func save_all_in(node: Node) -> void:
	for child in node.get_children():
		if child is GPUParticles2D and child.process_material:
			FileUtils.save_scene("res://assets/particles", child)
			Log.d(["saving", child])
		elif child is Node2D:
			save_all_in(child)
