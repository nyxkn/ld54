extends VBoxContainer

var id

@onready var sp = SoundManager.sample_players[id]

var SliderValue = preload("res://addons/nframework/ui/combo/slider_value.tscn")

func _ready() -> void:
	#print(sp.sample_id)
	$Label.text = sp.sample_id

	var params = [ "gain", "pitch_offset", "pitch_randomization", "volume_randomization" ]
	for param in params:
		var v = SliderValue.instantiate()
		v.name = param
		v.min_value = -100
		v.step = 0.1
		v.components = 2
		v.value = sp.get(param)
		v.value_changed.connect(on_value_changed.bind(param))
		$Params.add_child(v)


func on_value_changed(value, param):
	sp.set(param, value)


func _on_play_pressed() -> void:
	sp.play()


func _on_volume_value_changed(value: float) -> void:
	sp.gain = value


func _on_stop_pressed() -> void:
	sp.stop()


