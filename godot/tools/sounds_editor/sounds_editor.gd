extends Control

var SampleControls = preload("res://tools/sounds_editor/sample.tscn")

func _ready() -> void:
	for id in SoundManager.sample_players:
		var sc = SampleControls.instantiate()
		sc.id = id
		%GridContainer.add_child(sc)

func _on_save_pressed() -> void:
	SoundManager.save_cfg()
	#for id in SoundManager.sample_players:
		#var node = SoundManager.sample_players[id]
		#for prop in Utils.get_export_properties(node):
			#config.set_value(id, prop.name, node.get(prop.name))
#
		#Utils.save_cfg(Base.config_path + "sounds.cfg", )

func _on_test_pressed() -> void:
	SoundManager.play("a4", {"pitch_offset": -10})
