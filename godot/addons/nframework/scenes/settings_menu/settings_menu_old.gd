extends Control


#signal menu_closed
#
#enum ControlsDisplay { AUTO, LIST, TABS }
#
#const NTabs = preload("res://addons/nframework/ui/tabs.tscn")
#const ControlBind = preload("controls/control_bind.tscn")
#
## storing a list of all the ControlBind nodes that we have added
## this is a better system of storing things than looping and comparing names
#var control_bind_nodes := {}
#
#var _controls_display: int = ControlsDisplay.LIST
#
##onready var controls_list: VBoxContainer = find_child("ControlsList")
#@onready var controls_list: VBoxContainer = %ControlsList
#
##onready var controls_tabs: VBoxContainer = find_child("ControlsTabs")
#@onready var controls_tabs: VBoxContainer = %ControlsTabs
#
#
#@onready var controls_tabs_container: MarginContainer = controls_tabs.get_node("TabsContainer")
#
#@onready var rebind_popup: Popup = find_child("RebindPopup")
#
#@onready var button_back: Button
#@onready var main_tab_buttons: HBoxContainer = $MarginContainer/TabBar/TabButtons
#@onready var button_main_tab_controls: Button = main_tab_buttons.get_node("Controls")
#@onready var control_schemes_tab_buttons: HBoxContainer = controls_tabs.get_node("TabButtons")
#
#@onready var button_fullscreen: Button = find_child("Fullscreen")
#@onready var button_borderless: Button = find_child("Borderless")
#@onready var button_vsync: Button = find_child("VSync")
#@onready var button_fps: Button = find_child("FPS")
#
#
#func _ready():
#	setup_gui()
#
##	setup_controls_gui()
#
#	if Config.html5:
#		button_borderless.visible = false
#		# what about vsync and fullscreen?
#
##	setup_custom_tabs()
#
#	if Config.simple_settings:
#		button_back = %SinglePageBack
#		%SinglePage.show()
#		%TabBar.queue_free()
#		%TabsBack.queue_free()
#		Utils.grab_first_focus_recursive($MarginContainer/SinglePage/PanelContainer/ScrollContainer/VBoxContainer)
#
#	else:
#		button_back = %TabsBack
#		%TabBar.show()
#		%TabsBack.show()
#		%SinglePage.queue_free()
#
#		if Config.menu_control & Config.MenuControl.KEYBOARD:
#			setup_tabs_focus()
#
#		button_fullscreen.button_pressed = SettingsVideo.fullscreen
#		button_borderless.button_pressed = SettingsVideo.borderless
#		button_vsync.button_pressed = SettingsVideo.vsync
#		button_fps.button_pressed = FPSOverlay.enabled
#		%GamepadSwap.button_pressed = SettingsControls.gamepad_swap
#
#		button_fullscreen.connect("pressed",Callable(self,"_on_Fullscreen_pressed"))
#		button_borderless.connect("pressed",Callable(self,"_on_Borderless_pressed"))
#		button_vsync.connect("pressed",Callable(self,"_on_VSync_pressed"))
#		button_fps.connect("pressed",Callable(self,"_on_FPS_pressed"))
#
#
#	button_back.connect("pressed",Callable(self,"_on_Back_pressed"))
#
#	get_tree().root.connect("gui_focus_changed",Callable(self,"on_gui_focus_changed"))
#
#
#func on_gui_focus_changed(control: Control):
#	Log.d(["focus switched to control", control.name])
#
#
#func setup_gui() -> void:
#	theme = load(Config.theme)
#	if !Config.low_res:
#		Utils.set_margins($MarginContainer, 16)
#		Utils.set_margins($MarginContainer/VBoxContainer/MarginContainer, 0, 400)
#		for child in find_child("TabsContainer").get_children():
#			Utils.set_margins(child.get_node("MarginContainer"), 16, 32)
#			pass
#
#
#func _input(event) -> void:
#	if event.is_action_pressed("ui_cancel"):
#		if Config.simple_settings:
#			%SinglePageBack.grab_focus()
#		else:
#			button_back.grab_focus()
##		button_back.button_pressed = true
#		await get_tree().create_timer(0.15).timeout
#		_on_Back_pressed()
#
#
#func setup_tabs_focus():
#	Utils.setup_focus_grabs_on_mouse_entered($MarginContainer)
#
##	for button in main_tab_buttons.get_children():
##		button.focus_neighbor_top = button_back.get_path()
#
#	var button_video = main_tab_buttons.get_node("Video")
#
#	button_back.focus_neighbor_top = button_back.get_path()
#	button_back.focus_neighbor_right = button_video.get_path()
#	button_back.focus_neighbor_bottom = button_video.get_path()
#	# disable autograb of focus so we don't get on this button from anywhere else
#	# other than what we have already defined
#	button_back.focus_mode = Control.FOCUS_CLICK
#
#	button_video.focus_neighbor_left = button_back.get_path()
#	button_video.focus_neighbor_top = button_back.get_path()
#
#	button_fullscreen.grab_focus()
#
#
#func setup_controls_gui() -> void:
#	pass


#func setup_controls_gui() -> void:
#	#	if controls_display == ControlsDisplay.AUTO:
#	if SettingsControls.schemes_enabled.size() > 1:
#		_controls_display = ControlsDisplay.TABS
#	else:
#		_controls_display = ControlsDisplay.LIST
##	else:
##		_controls_display = controls_display
#
#	# ld52 hack
#	_controls_display = ControlsDisplay.LIST
#
#	match _controls_display:
#		ControlsDisplay.TABS:
#			controls_tabs.visible = true
#
##			var tabs_container = controls_tabs.get_node("TabsContainer")
#
##			for i in SettingsControls.schemes_enabled.size():
#			for i in 2:
#				var control_scheme_name: String = SettingsControls.schemes_enabled[i]
#				var vbox = VBoxContainer.new()
#				vbox.name = control_scheme_name.capitalize()
#				controls_tabs_container.add_child(vbox)
#				control_bind_nodes[control_scheme_name] = {}
#
#		ControlsDisplay.LIST:
#			controls_list.visible = true
#			var control_scheme_name: String = SettingsControls.schemes_enabled[0]
#			control_bind_nodes[control_scheme_name] = {}
#
#	for action in SettingsControls.action_controls.keys():
#		# looping over all schemes so we create entries even for unbound keys
##		for scheme in SettingsControls.schemes_enabled:
#		for scheme in SettingsControls.schemes_enabled.slice(0,0):
#			var control = SettingsControls.action_controls[action][scheme]
#			add_control_entry(action, control, scheme)
#
#	# must be done after binds have been added with add_control_entry
#	if _controls_display == ControlsDisplay.TABS:
#		controls_tabs.init()
#		setup_controls_tabs_focus()


#func setup_controls_tabs_focus():
#	for b in control_schemes_tab_buttons.get_children():
#		b.connect("toggled",Callable(self,"_control_schemes_tab_button_toggled").bind(b))
#
#	# retrigger the initial toggle
#	control_schemes_tab_buttons.get_child(0).button_pressed = false
#	control_schemes_tab_buttons.get_child(0).button_pressed = true
#
#	for scheme_tab in controls_tabs_container.get_children():
#		var control = scheme_tab.get_child(0).get_node("Main/Rebind")
#		var tab_button = control_schemes_tab_buttons.get_node(scheme_tab.name)
#		tab_button.focus_neighbor_top = tab_button.get_path_to(button_main_tab_controls)
#		control.focus_neighbor_top = control.get_path_to(tab_button)
#
#
#func add_control_entry(action, control, scheme) -> void:
#	var entry = ControlBind.instantiate()
#
#	if _controls_display == ControlsDisplay.LIST:
#		controls_list.add_child(entry)
#	else:
#		controls_tabs_container.get_node(scheme.capitalize()).add_child(entry)
#
#	# keep track of the bind nodes
#	control_bind_nodes[scheme][action] = entry
#	entry.get_node("Action").text = str(action.capitalize()) # optionally add colon after action
#	entry.get_node("Main/Rebind").connect("pressed",Callable(self,"_on_Rebind_pressed").bind(action, scheme))
#	update_bind(action, control, scheme)
#
#
#func update_bind(action, control, scheme) -> void:
#	var entry = control_bind_nodes[scheme][action]
#	entry.get_node("Main/Rebind").text = control.get_inputevent_name()
#	entry.get_node("Main/Rebind").focus_neighbor_left = entry.get_path()
#
#
#func _control_schemes_tab_button_toggled(toggled, button):
#	pass
##	if toggled:
##		button_main_tab_controls.focus_neighbor_bottom = button_main_tab_controls.get_path_to(button)
#
#
#func _on_Rebind_pressed(action, scheme) -> void:
#	rebind_popup.popup_centered()
#	await rebind_popup.control_rebound
#	if rebind_popup.new_event == null:
#		return
#	var event: InputEvent = rebind_popup.new_event
#	var control = SettingsControls.rebind_from_menu(action, event, scheme)
#	update_bind(action, control, scheme)
#	control_bind_nodes[scheme][action].get_node("Main/Rebind").grab_focus()
#
#
#func _on_Back_pressed() -> void:
#	Settings.save_cfg()
#
#	if get_tree().get_root().get_node_or_null(self.name): # if we were loaded as a scene
#		F.change_scene(Config.scenes.main_menu)
#	else: # or as an overlay above the pause/game screen
#		visible = false
#		queue_free()
#		emit_signal("menu_closed")
#
#
#func _on_Fullscreen_pressed() -> void:
#	SettingsVideo.fullscreen = button_fullscreen.pressed
#
#
#func _on_Borderless_pressed() -> void:
#	SettingsVideo.borderless = button_borderless.pressed
#
#
#func _on_VSync_pressed() -> void:
#	SettingsVideo.vsync = button_vsync.pressed
#
#
#func _on_FPS_pressed() -> void:
#	FPSOverlay.enabled = button_fps.pressed
#
#
#func _on_GamepadSwap_toggled(button_pressed: bool) -> void:
#	SettingsControls.gamepad_swap = button_pressed
