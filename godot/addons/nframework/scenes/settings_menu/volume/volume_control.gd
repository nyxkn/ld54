extends Control


@export var use_node_name_as_label: bool = false
@export var bus_name: String = "Master"

var label: String = ""


func _ready() -> void:
	var bus_volume = SettingsAudio.get_volume(bus_name)
	Log.d("setting slider", bus_volume)
	assert(bus_volume != -1, str("specified bus name (", bus_name, ") does not exist"))

	if use_node_name_as_label:
		label = name
	else:
		label = bus_name

	$MenuSlider/Label.text = label
	$AudioStreamPlayer.bus = bus_name

	$MenuSlider/HSlider.value = bus_volume
	$MenuSlider/HSlider.value_changed.connect(_on_value_changed)


func _on_value_changed(value):
	SettingsAudio.set_volume(bus_name, value)
	# here it's best to use our own audiostreamplayer rather than relying on soundmanager
	# this is because we have to play from all audio buses, so can't use play_ui
	$AudioStreamPlayer.play()
