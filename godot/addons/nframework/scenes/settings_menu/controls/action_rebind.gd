extends HBoxContainer


var action: String
var control: InputControl : set = set_ctrl_pressed


func _ready() -> void:
	$Control/RebindButton.text = ""
	$Control/RebindButton.focus_neighbor_left = get_path()
	$Action.text = str(action.capitalize()) # optionally add colon after action


func set_ctrl_pressed(value):
	control = value
	print(control)
	if control != null:
		$Control/RebindButton.text = control.get_inputevent_name()
	else:
		$Control/RebindButton.text = ""
