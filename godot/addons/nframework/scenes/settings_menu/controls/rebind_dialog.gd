extends Popup


signal rebind_completed(operation, control)

var device: String


func _ready() -> void:
	exclusive = true
	var focus_owner = get_viewport().gui_get_focus_owner()
	if focus_owner:
		focus_owner.release_focus()

	if device == "gamepad":
		%Message.text = "Press new button!"


func _unhandled_input(event: InputEvent) -> void:
	# by returning, you make it so that nothing happens with these specific keypresses
	# the dialog stays open
	if device == "gamepad":
#		if not (event is InputEventJoypadButton or event is InputEventJoypadMotion):
		# we're currently not even reading motions
		# the problem is that trigger buttons are motions, and they should ideally be assignable
		# sono toki wa sono toki
		if not event is InputEventJoypadButton:
			return
	elif device == "keymouse":
		var code
		if event is InputEventKey:
			code = event.keycode
		elif event is InputEventMouseButton:
			code = event.button_index
		else:
			# not an acceptable key
			return

		if code == KEY_ESCAPE:
			# escape closes dialog
			# actually redundant cause escape already closes ui by default from gui_input
			_on_cancel_pressed()
		if code in SettingsControls.unbindable_buttons:
			# show helpful message
			return

	if not event.is_pressed():
		return

	# if we got here, it means we got a valid control
	var control := InputControl.new().from_event(event)
	rebind_completed.emit("rebind", control)
	hide_and_free()


func hide_and_free():
	visible = false
	queue_free()


func _on_cancel_pressed() -> void:
	rebind_completed.emit("cancel", null)
	hide_and_free()


func _on_unbind_pressed() -> void:
	# not an ideal system. overloading the control rebound signal
	# ideally you'd have different signals. but this is a quick hack
	rebind_completed.emit("unbind", null)
	hide_and_free()
