extends Control


signal menu_closed

enum ControlsDisplay { AUTO, LIST, TABS }

const NTabs = preload("res://addons/nframework/ui/tabs.tscn")

const ActionRebind = preload("controls/action_rebind.tscn")
const RebindDialog = preload("controls/rebind_dialog.tscn")

# storing a list of all the ControlBind nodes that we have added
# this is a better system of storing things than looping and comparing names
var action_rebind_nodes := {}

var _controls_display: int = ControlsDisplay.LIST

#onready var controls_list: VBoxContainer = %ControlsList
#onready var gamepad_controls_list: VBoxContainer = %GamepadControlsList

#onready var controls_tabs: VBoxContainer = find_child("ControlsTabs")
#onready var controls_tabs_container: MarginContainer = controls_tabs.get_node("TabsContainer")
#onready var control_schemes_tab_buttons: HBoxContainer = controls_tabs.get_node("TabButtons")
#onready var button_main_tab_controls: Button = main_tab_buttons.get_node("Controls")
#onready var main_tab_buttons: HBoxContainer = $MarginContainer/TabBar/TabButtons

#onready var rebind_popup: Popup = find_child("RebindPopup")

#@onready var button_back: Button =
@onready var button_back: Button = %SinglePageBack


#@onready var button_fullscreen: Button = find_child("Fullscreen")
#@onready var button_borderless: Button = find_child("Borderless")
#@onready var button_vsync: Button = find_child("VSync")
#@onready var button_fps: Button = find_child("FPS")


func _ready():
	setup_gui()

	setup_controls_ui()

	#if Base.platform == Base.Platform.WEB:
		#button_borderless.visible = false
		## what about vsync and fullscreen?

	Utils.grab_first_focus_recursive(%Settings)

#	else:
#		button_back = %TabsBack
#		%TabBar.show()
#		%TabsBack.show()
#		%SinglePage.queue_free()
#
#		if Config.menu_control & Config.MenuControl.KEYBOARD:
#			setup_tabs_focus()
#
#		button_fullscreen.button_pressed = SettingsVideo.fullscreen
#		button_borderless.button_pressed = SettingsVideo.borderless
#		button_vsync.button_pressed = SettingsVideo.vsync
#		button_fps.button_pressed = FPSOverlay.enabled
##		%GamepadSwap.button_pressed = SettingsControls.gamepad_swap
#
#		button_fullscreen.connect("pressed",Callable(self,"_on_Fullscreen_pressed"))
#		button_borderless.connect("pressed",Callable(self,"_on_Borderless_pressed"))
#		button_vsync.connect("pressed",Callable(self,"_on_VSync_pressed"))
#		button_fps.connect("pressed",Callable(self,"_on_FPS_pressed"))


	get_tree().root.gui_focus_changed.connect(on_gui_focus_changed)


func on_gui_focus_changed(control: Control):
	Log.d(["focus switched to control", control.name])


func setup_gui() -> void:
	theme = load(Config.theme)
	if !Config.low_res:
		Utils.set_margins($MarginContainer, 16)
		Utils.set_margins($MarginContainer/VBoxContainer/MarginContainer, 0, 400)
		for child in find_child("TabsContainer").get_children():
			Utils.set_margins(child.get_node("MarginContainer"), 16, 32)
			pass


func _input(event) -> void:
	if event.is_action_pressed("ui_cancel"):
		button_back.grab_focus()
#		button_back.button_pressed = true
		await F.wait(0.15)
		_on_back_pressed()


func setup_controls_ui() -> void:
	if SettingsControls.mode != SettingsControls.Mode.REBIND:
		Log.d("not in rebind mode. nothing do to")
		return

	for device in SettingsControls.enabled_devices:
		action_rebind_nodes[device] = {}
		for action in SettingsControls.enabled_actions:
			if device == "gamepad" and action in SettingsControls.directions:
				# we don't add gamepad directions
				continue
			add_action_rebind_entry(device, action)

	update_controls_ui()


func update_controls_ui():
	Log.d("updating controls")
	for device in SettingsControls.enabled_devices:
		for action in SettingsControls.enabled_actions:
			if device == "gamepad" and action in SettingsControls.directions:
				# we don't add gamepad directions
				continue
			var controls_array = SettingsControls.controls[device][action]
			var control = null
			if controls_array.size() >= 1:
				# here we just assume we're only working with one control
				# if you want to rebind more controls this needs reworking
				control = controls_array[0]
			update_rebind_entry(device, action, control)


# this only gets done once when menu is being setup
func add_action_rebind_entry(device, action) -> void:
	if action in action_rebind_nodes[device]:
		Log.w("action rebind entry already exists")
		return

	var entry = ActionRebind.instantiate()
	entry.action = action
	entry.get_node("Control/RebindButton").pressed.connect(_on_RebindButton_pressed.bind(device, action))

	var controls_list = %ControlsList
	if device == "gamepad":
		controls_list = %GamepadControlsList
	controls_list.add_child(entry)

	action_rebind_nodes[device][action] = entry


func update_rebind_entry(device, action, new_control: InputControl) -> void:
	var entry = action_rebind_nodes[device][action]
	entry.control = new_control


func _on_RebindButton_pressed(device, action) -> void:
	if Config.d_spoof_colemak:
		Log.e(["not allowed to rebind in spoof mode"])
		return

	var rebind_dialog = RebindDialog.instantiate()
	rebind_dialog.device = device
	add_child(rebind_dialog)
	rebind_dialog.popup_centered()


	var res = await rebind_dialog.rebind_completed
	var operation = res[0]

	var action_rebind_entry = action_rebind_nodes[device][action]
	var current_control = action_rebind_entry.control

	if operation == "cancel":
		Log.d(["rebind canceled"])
		# do nothing
	elif operation == "unbind":
		Log.d(["unbindinding"])
		SettingsControls.remove_control(device, action, current_control)
	elif operation == "rebind":
		var new_control: InputControl = res[1]
		SettingsControls.replace_control(device, action, current_control, new_control)

	update_controls_ui()
	action_rebind_entry.get_node("Control/RebindButton").grab_focus()





func _on_back_pressed() -> void:
	Settings.save_cfg()

	if get_tree().current_scene == self:
		# if we were loaded as a scene
		F.change_scene(Config.scenes.main_menu)
	else:
		# or as an overlay above the pause/game screen
		visible = false
		queue_free()
		menu_closed.emit()


#func _on_Fullscreen_pressed() -> void:
	#SettingsVideo.fullscreen = button_fullscreen.button_pressed
#
#
#func _on_Borderless_pressed() -> void:
	#SettingsVideo.borderless = button_borderless.button_pressed
#
#
#func _on_VSync_pressed() -> void:
	#SettingsVideo.vsync = button_vsync.button_pressed
#
#
#func _on_FPS_pressed() -> void:
	#FPSOverlay.enabled = button_fps.button_pressed


func _on_GamepadSwap_toggled(button_pressed: bool) -> void:
	SettingsControls.gamepad_swap = button_pressed




#func setup_tabs_focus():
#	Utils.setup_focus_grabs_on_mouse_entered($MarginContainer)
#
##	for button in main_tab_buttons.get_children():
##		button.focus_neighbor_top = button_back.get_path()
#
#	var button_video = main_tab_buttons.get_node("Video")
#
#	button_back.focus_neighbor_top = button_back.get_path()
#	button_back.focus_neighbor_right = button_video.get_path()
#	button_back.focus_neighbor_bottom = button_video.get_path()
#	# disable autograb of focus so we don't get on this button from anywhere else
#	# other than what we have already defined
#	button_back.focus_mode = Control.FOCUS_CLICK
#
#	button_video.focus_neighbor_left = button_back.get_path()
#	button_video.focus_neighbor_top = button_back.get_path()
#
#	button_fullscreen.grab_focus()

#func setup_controls_ui() -> void:
#	#	if controls_display == ControlsDisplay.AUTO:
#	if SettingsControls.schemes_enabled.size() > 1:
#		_controls_display = ControlsDisplay.TABS
#	else:
#		_controls_display = ControlsDisplay.LIST
##	else:
##		_controls_display = controls_display
#
#	match _controls_display:
#		ControlsDisplay.TABS:
#			controls_tabs.visible = true
#
##			var tabs_container = controls_tabs.get_node("TabsContainer")
#
#			for i in SettingsControls.schemes_enabled.size():
#				var control_scheme_name: String = SettingsControls.schemes_enabled[i]
#				var vbox = VBoxContainer.new()
#				vbox.name = control_scheme_name.capitalize()
#				controls_tabs_container.add_child(vbox)
#				control_bind_nodes[control_scheme_name] = {}
#
#		ControlsDisplay.LIST:
#			controls_list.visible = true
#			var control_scheme_name: String = SettingsControls.schemes_enabled[0]
#			control_bind_nodes[control_scheme_name] = {}
#
#	for action in SettingsControls.action_controls.keys():
#		# looping over all schemes so we create entries even for unbound keys
#		for scheme in SettingsControls.schemes_enabled:
#			var control = SettingsControls.action_controls[action][scheme]
#			add_control_entry(action, control, scheme)
#
#	# must be done after binds have been added with add_control_entry
#	if _controls_display == ControlsDisplay.TABS:
#		controls_tabs.init()
#		setup_controls_tabs_focus()


#func setup_controls_tabs_focus():
#	for b in control_schemes_tab_buttons.get_children():
#		b.connect("toggled",Callable(self,"_control_schemes_tab_button_toggled").bind(b))
#
#	# retrigger the initial toggle
#	control_schemes_tab_buttons.get_child(0).button_pressed = false
#	control_schemes_tab_buttons.get_child(0).button_pressed = true
#
#	for scheme_tab in controls_tabs_container.get_children():
#		var control = scheme_tab.get_child(0).get_node("Main/Rebind")
#		var tab_button = control_schemes_tab_buttons.get_node(scheme_tab.name)
#		tab_button.focus_neighbor_top = tab_button.get_path_to(button_main_tab_controls)
#		control.focus_neighbor_top = control.get_path_to(tab_button)

#func _control_schemes_tab_button_toggled(toggled, button):
#	if toggled:
#		button_main_tab_controls.focus_neighbor_bottom = button_main_tab_controls.get_path_to(button)

