extends Control


var keypress_enabled: bool = true


func _ready() -> void:
	await get_tree().create_timer(3).timeout
#	F.change_scene(Config.scenes.new_game)
	F.change_scene(Config.scenes.main_menu)


func _input(event) -> void:
	if keypress_enabled:
		if (event.is_action_pressed("ui_accept")
			or event.is_action_pressed("ui_select")
			or event.is_action_pressed("ui_cancel")):

			F.change_scene(Config.scenes.main_menu)
