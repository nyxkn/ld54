extends Control


@onready var button_newgame: Button = %NewGame
@onready var button_settings: Button = %Settings
@onready var button_exit: Button = %Exit
@onready var button_help: Button = %Help


var sound_volume
var music_volume


func _ready():
#	theme = load(Config.theme)
#	$Control/Title.text = ProjectSettings.get_setting("application/config/name")

	if Base.platform == Base.Platform.WEB:
		button_exit.visible = false

	setup_focus()

	if Score.best_score:
		%BestScore.value = Score.best_score.score
	else:
		%BestScore.value = "0"


func setup_focus():
	if Config.menu_control & Config.MenuControl.KEYBOARD:
		Utils.setup_focus_grabs_on_mouse_entered(%MenuOptions)
		Utils.grab_first_focus_recursive(%MenuOptions)
#		button_newgame.grab_focus()


func _on_new_game_pressed() -> void:
	# example for ensuring loading has finished before starting
	var loader = get_tree().root.get_node_or_null("PreLoader")
	if not loader or loader.finished:
		G.start_stopwatch()
		F.change_scene(Config.scenes.game, {}, 1.0)
	else:
		Log.e("preloader resources are still being loaded. we shouldn't let this happen", name)


func _on_settings_pressed() -> void:
	#F.change_scene(Config.scenes.settings_menu, {}, 0.1, 0.1)
	F.change_scene(Config.scenes.settings_menu)


func _on_credits_pressed() -> void:
	pass # Replace with function body.


func _on_help_pressed() -> void:
	F.change_scene(Config.scenes.help)


func _on_exit_pressed() -> void:
	get_tree().quit()


func _on_sound_toggled(button_pressed: bool) -> void:
	if button_pressed:
		sound_volume = SettingsAudio.get_volume("SFX")
		SettingsAudio.set_volume("SFX", 0)
	else:
		SettingsAudio.set_volume("SFX", sound_volume)


func _on_music_toggled(button_pressed: bool) -> void:
	if button_pressed:
		music_volume = SettingsAudio.get_volume("Music")
		SettingsAudio.set_volume("Music", 0)
	else:
		SettingsAudio.set_volume("Music", music_volume)


func _on_leaderboard_pressed() -> void:
	F.change_scene(Config.scenes.leaderboard)
