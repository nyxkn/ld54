extends Control


const ScoreEntry = preload("score_entry.tscn")
@onready var scores_table: VBoxContainer = %ScoresTable


func _ready() -> void:
	if not Config.silentwolf:
		return

	%Loading.show()
	var scores = await Score.get_scores_online()
	%Loading.hide()

	for i in scores.size():
		var score = scores[i]
		var score_entry = ScoreEntry.instantiate()

		score_entry.get_node("Rank").text = str(i+1)
		score_entry.get_node("PlayerName").text = score.player_name
		score_entry.get_node("Score").text = str(score.score)
		if score.metadata:
			if score.metadata.play_time:
				var play_time = score.metadata.play_time
				play_time = TimeUtils.seconds_to_minutes(play_time)
				score_entry.get_node("Time").text = play_time.split(".")[0] + "m"
		else:
			score_entry.get_node("Time").text = "-"

		scores_table.add_child(score_entry)


func _on_restart_pressed() -> void:
	F.change_scene(Config.scenes.game)


func _on_menu_pressed() -> void:
	F.change_scene(Config.scenes.main_menu)
