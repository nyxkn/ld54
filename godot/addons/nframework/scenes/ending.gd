extends Control


var scene_change_data
#var keypress_enabled: bool = false


func _ready() -> void:
	if Score.last_score:
		%Score.text = "%d" % Score.last_score.score
		# you don't always want to show the best score (although it's always good to show in main menu)
		%BestScore.text = "%d" % Score.best_score.score

	if scene_change_data:
		var play_time = scene_change_data.stats.play_time
#		%PlayTime.text = "%.1fs" % play_time
		play_time = TimeUtils.seconds_to_minutes(play_time)
		#%PlayTime.text = play_time.split(".")[0] + " (min:sec)"
		%PlayTime.text = play_time.split(".")[0]


	await get_tree().create_timer(1.5).timeout
#	keypress_enabled = true
	%Restart.grab_focus()


func _input(event) -> void:
	pass
#	if keypress_enabled:
	# if the game is keyboard/gamepad only, use a keypress instead of the ui button
	# but make sure this is not a key player were spamming, or use the keypress enabled delay
	# if the screen won't be shown again, make sure there's no way for player to skip it inadvertently
#	if event is InputEventKey:
#		if event.keycode == KEY_ENTER:
#			_on_Restart_pressed()



func _on_Restart_pressed() -> void:
	F.change_scene(Config.scenes.game, {}, 0.25)


func _on_MainMenu_pressed() -> void:
	F.change_scene(Config.scenes.main_menu, {}, 0.25)



