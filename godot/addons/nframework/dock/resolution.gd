@tool
extends Container


enum ResType { MAIN, TEST }

var resolutions := {}
const resolutions_data := {
	"16:9": [
		"160x90",
		"256x144",
		"320x180",
		"384x216",
		"426x240",
		"480x270",
		"640x360",
		"854x480",
		"960x540",
		"1280x720",
		"1366x768",
		"1600x900",
		"1920x1080",
		],
	"16:10": [
		"160x100",
		"256x160",
		"320x200",
		"384x240",
#		"426x240",
		"480x300",
		"640x400",
#		"854x480",
		"960x600",
		"1280x800",
#		"1366x768",
		"1440x900",
		"1600x1000",
		"1680x1050",
		"1920x1200",
		],
	"3:2": [
		"192x128",
		"216x144",
		"240x160",
		"384x256",
		"480x320",
		"720x480",
		"960x640",
		"1200x800",
		"1440x960",
		"1680x1120",
		"1920x1280",
		],
	"retro": {
		'128x128': 'Pico8',
		'160x144': 'Gameboy',
		'240x136': 'TIC-80',
		'240x160': 'GBA',
		'256x224': 'NES/SNES', # non-square pixels: 8:7
		'320x200': 'DOS', # non-square pixels: 5:6 (1:1.2)
		'320x224': 'Genesis',
		'320x240': 'PlayStation',
		'640x480': 'Dreamcast',
		}
	}

const aspect_ratios := [
	"16:9",
	"16:10",
	"3:2",
	"4:3",
	"8:7",
	"10:9",
	"1:1",
	]

#var option_buttons := {}
var selected_category: String = ""
var main_resolution: Resolution
var test_resolution: Resolution
var sync_test_resolution: bool = false
#var test_resolution_scale: int = 100
var test_resolution_mult: float = 0

@onready var ob_category: OptionButton = $MainRes/NewRes/Category
@onready var ob_resolutions: OptionButton = $MainRes/NewRes/Resolutions

@onready var aspect_ratio: = $MainRes/MarginContainer/HBoxContainer/VBoxContainer/AspectRatio
@onready var tiles: = $MainRes/MarginContainer/HBoxContainer/VBoxContainer/Tiles


func _ready() -> void:
	# hack to get the padding right
	# using a monospaced font instead would be ideal
	find_child("ScaleValue").get_node("Value").custom_minimum_size.x = 40

	for category in resolutions_data:
		generate_resolutions(category, resolutions_data[category])

	ob_category.clear()
	for category in resolutions:
		ob_category.add_item(category)
	selected_category = "16:9"

	rebuild_resolutions_list()

	update_ui()

	ar_info_menu()


func ar_info_menu():
#	var vbox = VBoxContainer.new()
	var container = GridContainer.new()
	container.columns = 2
#	vbox.size_flags_vertical = SIZE_EXPAND_FILL
	$AspectRatioInfo.content.add_child(container)
	for r in aspect_ratios:
		pass
		var label_ratio := Label.new()
		var label_decimal := Label.new()

		var values = r.split_floats(":")
		var decimal_ratio = values[0] / float(values[1])

		label_ratio.text = r
#		label_decimal.text = str(" (", decimal_ratio, ")")
		label_decimal.text = str(decimal_ratio)
		container.add_child(label_ratio)
		container.add_child(label_decimal)


func generate_resolutions(category, resolutions_list):
	var res_dict := {}
	# convert array to dict
	if resolutions_list is Array:
		for r in resolutions_list:
			res_dict[r] = ""
	else:
		res_dict = resolutions_list

	resolutions[category] = []
	for r in res_dict:
		var values = r.split_floats("x")
		resolutions[category].push_back(Resolution.new(values[0], values[1], res_dict[r]))


func rebuild_resolutions_list() -> void:
	var res = resolutions[selected_category].duplicate()
	res.reverse()

	# add the current resolution at the top
	ob_resolutions.clear()
	ob_resolutions.add_item(str(get_resolution()), 0)
	ob_resolutions.set_item_disabled(0, true)
	ob_resolutions.add_separator()

	for r in res:
		var entry = str(r)
		if r.description:
			entry += " - " + r.description
		ob_resolutions.add_item(str(entry))


func update_ui() -> void:
	main_resolution = get_resolution()
	$CurrentRes/ResValue.value = str(main_resolution)

	test_resolution = get_resolution(ResType.TEST)
	$TestRes/HBoxContainer/ResValue.value = str(test_resolution)

	var ar_string = main_resolution.aspect_ratio_string()
	var ar_float = str(main_resolution.aspect_ratio_float())
	aspect_ratio.value = ar_string + "   (" + ar_float + ")"

	tiles.value = str(main_resolution.w / 16.0, " x ", main_resolution.h / 16.0)


func set_resolution(res: Resolution, res_type: int = ResType.MAIN) -> void:
	if res_type == ResType.MAIN:
		ProjectSettings.set_setting("display/window/size/viewport_width", res.w)
		ProjectSettings.set_setting("display/window/size/viewport_height", res.h)

	if res_type == ResType.TEST or sync_test_resolution:
		ProjectSettings.set_setting("display/window/size/window_width_override", res.w)
		ProjectSettings.set_setting("display/window/size/window_height_override", res.h)

	ProjectSettings.save()
	update_ui()


func get_resolution(res_type: int = ResType.MAIN) -> Resolution:
	var w; var h

	if res_type == ResType.MAIN:
		w = ProjectSettings.get_setting("display/window/size/viewport_width")
		h = ProjectSettings.get_setting("display/window/size/viewport_height")
	if res_type == ResType.TEST:
		w = ProjectSettings.get_setting("display/window/size/window_width_override")
		h = ProjectSettings.get_setting("display/window/size/window_height_override")

	return Resolution.new(w, h)


func scaled_resolution(res: Resolution, scale_mult: float = 1.0) -> Resolution:
	var sx = res.w * scale_mult
	var sy = res.h * scale_mult
	return Resolution.new(sx, sy)


func apply_scale() -> void:
	var mult := float($TestRes/Scale/ScaleInput.text)
	var scaled_res = scaled_resolution(main_resolution, mult)
	set_resolution(scaled_res, ResType.TEST)


func set_test_resolution_from_screen(div: float) -> void:
	var screen_res = DisplayServer.screen_get_size()
	var res = Resolution.new(screen_res.x / div, screen_res.y / div)
	set_resolution(res, ResType.TEST)


func _on_Category_item_selected(index: int) -> void:
	var value := ob_category.get_item_text(index)
	selected_category = value
	rebuild_resolutions_list()


func _on_Resolutions_item_selected(index: int) -> void:
	var values := ob_resolutions.get_item_text(index).split_floats("x")
	var new_res := Resolution.new(values[0], values[1])
	set_resolution(new_res)


func _on_Refresh_pressed() -> void:
	update_ui()


func _on_Half_pressed() -> void:
	set_test_resolution_from_screen(2)
func _on_3rd_pressed() -> void:
	set_test_resolution_from_screen(3)
func _on_4th_pressed() -> void:
	set_test_resolution_from_screen(4)


func _on_1to1_pressed() -> void:
	set_resolution(main_resolution, ResType.TEST)


func _on_ApplyScale_pressed() -> void:
	apply_scale()

func _on_ScaleInput_text_entered(new_text: String) -> void:
	apply_scale()

