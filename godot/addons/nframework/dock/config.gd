@tool
extends MarginContainer

# hack to run manually triggered code
# discard the passed value; we don't care
@export var trigger: bool = false : set = on_trigger
func on_trigger(_value) -> void:
	trigger = false
#	print(Config.editor_interface)



func _ready() -> void:
	populate_toggles()


func _on_SaveCfg_pressed() -> void:
	Config.save_cfg()


func _on_PrintValues_pressed() -> void:
	for prop in Utils.get_export_properties(self):
		print(str(prop.name, " = ", Config.get(prop.name)))


func _on_Test_pressed() -> void:
	pass
#	print(Config.editor_interface)


func populate_toggles() -> void:
	var toggles = %Toggles
	var ConfigToggle = preload("config_toggle.tscn")

	for prop in Utils.get_export_properties(Config):
		if prop.type == TYPE_BOOL:
			var new_toggle = ConfigToggle.instantiate()
			new_toggle.name = prop.name
			new_toggle.tracking_property = prop.name
			toggles.add_child(new_toggle)


func _on_DeleteCfg_pressed() -> void:
	FileUtils.delete_file(Config.cfg_save_path)


func _on_DeleteSettings_pressed() -> void:
	FileUtils.delete_file(Settings.cfg_path)


func _on_sounds_editor_pressed() -> void:
	Config.d_custom_launch = "res://tools/sounds_editor/sounds_editor.tscn"
	Config.save_cfg()

	Config.editor_interface.play_main_scene()
