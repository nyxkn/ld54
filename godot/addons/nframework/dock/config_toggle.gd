@tool
extends CheckButton


@export var tracking_property: String = ""

# you can set tracking property in the editor as an export
# or through the init method here through code
#func setup(tracking_property: String) -> Node:
#	self.tracking_property = tracking_property
#	return self

func _ready() -> void:
	assert(tracking_property in Config, "tracking_property = \"" + str(tracking_property) + "\". " +
			"this does not exist in Config")

	text = tracking_property
	button_pressed = Config.get(tracking_property)

	# connect signal after initialization so that setting pressed doesn't trigger the toggle
	toggled.connect(_on_ConfigToggle_toggled)


func _on_ConfigToggle_toggled(button_pressed: bool) -> void:
	Log.p(['configtoggle', tracking_property, button_pressed])
	var prev_value = Config.get(tracking_property)
	Config.set(tracking_property, button_pressed)
	Config.save_cfg()

