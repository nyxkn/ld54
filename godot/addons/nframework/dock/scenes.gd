@tool
extends VBoxContainer


const SceneSetButton = preload("scene_set_button.tscn")


func _ready() -> void:
	for scene in Config.scenes:
		var entry = SceneSetButton.instantiate()
		entry.scene_name = scene
		add_child(entry)
