@tool
extends EditorPlugin

const BASE_PATH: String = "res://addons/nframework/"
const AUTOLOADS_PATH: String = BASE_PATH + "autoloads/"
const NULL_AUTOLOAD: String = "null.gd"
const CFG_PATH: String = "res://config/nframework.cfg"

# order matters!
# the autoloads will be loaded and appear in the scenetree in this order
# in any case, the scenetree, get_tree(), won't be accessible until all autoloads have finished loading
# you can disable autoloads by adding them to disabled_autoloads
# almost all autoloads require the core modules, so don't disable those
# you mostly just want to disable the overlays
# things that can be loaded independently:
# core (base + core)
# settings (save/load of settings and controls layer)
# overlays + systems (all the other autoloads)
const autoloads := {
	base = {
		## logger is the most fundamental module. and it can be loaded standalone as well
		# logger should be the first thing loaded, because everything should be able to print
		"Log": "logger.gd",
		# consts and platform stuff
		"Base": "base.gd",
		},

	core = {
		## core modules
		# everything else depends on these
		"Config": "config.gd",
		},

	settings = {
		# runtime player settings
		"SettingsAudio": "settings/settings_audio.gd",
		"SettingsControls": "settings/settings_controls.gd",
		"SettingsVideo": "settings/settings_video.gd",
		"Settings": "settings/settings.gd",
		},

	overlays = {
		# overlays
		"TransitionOverlay": "overlay/transition_overlay.tscn",
		"FPSOverlay": "overlay/fps_overlay.tscn",
		"DebugDraw": "overlay/debug_draw/debug_draw.tscn",
		"Pause": "overlay/pause.tscn",
		"TestBench": "overlay/test_bench.tscn",
		"DevInfo": "overlay/dev_info.tscn",
		},

	systems = {
		# systems
		# framework depends on transition overlay and pause system
		"F": "framework.gd",
		"SoundManager": "sound_system/sound_manager.gd",
	#	"Music": "sound_system/music.gd",
		"Score": "score/score.gd",
		},

	project = {
		"G": "res://global.tscn",
		},
}

const disabled_autoloads := [
#	"Settings",
#	"SettingsAudio",
#	"SettingsControls",
#	"SettingsVideo",
#	"Pause",
#	"Transition",
#	"FPS"
#	"F",
	]

var _added_autoloads := []


var dock_loaded := false
var dock_file_editor
var dock_config
var dock_resolution


var cfg


func _ready():
	print('nf ready')


func load_section(section):
	# add_autoload is adding the autoload to the list
	# then through some magic something else loads those autoloads
	# and we have to wait a little while for them to be loaded

	print("nf loading section: " + section)
	for key in autoloads[section]:
		if key in disabled_autoloads:
			# we do this not to break all things that use the autoload keywords
			add_autoload_singleton(key, AUTOLOADS_PATH + NULL_AUTOLOAD)
		else:
			var filepath = autoloads[section][key]
			if section != "project":
				filepath = AUTOLOADS_PATH + filepath
			add_autoload_singleton(key, filepath)
			print("nf added autoload: " + key)

		_added_autoloads.append(key)


# this reads a configfile into a dictionary of dictionaries
func read_cfg(file_path: String) -> Dictionary:
	var config = ConfigFile.new()
	config.load(file_path)
	var section = "nframework"
	var data = {}
#	config.get_value(section, "minimal")
	for key in config.get_section_keys(section):
		data[key] = config.get_value(section, key)
	return data


# this is an editor plugin
# it ONLY gets run when you're in the editor
# it does not get run with the game
func _enter_tree():
	print('nf enter tree')
	# Initialization of the plugin goes here.

	cfg = read_cfg(CFG_PATH)

	var override_loading = false
	if override_loading:
		# custom/testing load code here
		load_section("base")
		load_section("core")
	else:
		load_section("base")
		#await get_tree().create_timer(1).timeout
		if not cfg["minimal"]:
			for section in autoloads:
				if section == "base" or section == "project":
					continue
				load_section(section)
		load_section("project")

		#for section in autoloads:
			#if cfg["minimal"]:
				#if section != "base" and section != "project":
					#continue
#
			#load_section(section)


	# ensure everything is loaded
	await get_tree().create_timer(1).timeout
	print('nf autoloads setup likely complete (we waited 1 second)')

	# dock requires core autoloads, so call this last
	if not cfg["minimal"]:
	#	get_tree().root.set_meta("editor_interface", get_editor_interface())
		get_node("/root/Config").editor_interface = get_editor_interface()
		load_dock()

	# ensure dock is loaded
	await get_tree().create_timer(1).timeout
	print('nf dock setup complete')

	if not cfg["minimal"]:
		if ProjectSettings.get_setting("application/run/main_scene") == "":
			ProjectSettings.set_setting("application/run/main_scene", "res://addons/nframework/main.tscn")


func load_dock():
	# Load the dock. Load from file here so it's reloaded on every _enter_tree (?)
	dock_resolution = load("res://addons/nframework/dock/resolution.tscn").instantiate()
	add_control_to_dock(DOCK_SLOT_LEFT_BR, dock_resolution)

	dock_file_editor = load("res://addons/nframework/dock/file_editor.tscn").instantiate()
	add_control_to_dock(DOCK_SLOT_LEFT_BR, dock_file_editor)

	dock_config = load("res://addons/nframework/dock/config.tscn").instantiate()
	add_control_to_bottom_panel(dock_config, "NFramework")

	dock_loaded = true


func unload_dock():
	if dock_loaded:
		remove_control_from_bottom_panel(dock_config)
		dock_config.queue_free()

		remove_control_from_docks(dock_resolution)
		dock_resolution.queue_free()

		remove_control_from_docks(dock_file_editor)
		# for an unknown reason, using queue_free() to cleanup file_editor causes a segfault
		# but free() seems to work fine
		# the only error we get is a segfault and no other message
		# this happens e.g. when you try to reload the project
		# attempts: it's not because it's the last free, and awaiting after the free doesn't help
		#dock_file_editor.queue_free()
		dock_file_editor.free()


func _exit_tree():
	print('nf exit tree')
	# Clean-up of the plugin goes here.

	# if you have to save to create the file
#	FileUtils.save_cfg(CFG_PATH, cfg)

	# Remove the dock.
	unload_dock()

#	remove_autoload_singleton("G")
#	for section in autoloads:
#		for key in autoloads[section]:
#			remove_autoload_singleton(key)
	for key in _added_autoloads:
		remove_autoload_singleton(key)


