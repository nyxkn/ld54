class_name StopwatchRT
extends Node

# this is realtime
# it's unaffected by engine slowdowns and pause
# this doesn't use _process so you don't need to add as child


var paused: bool = false : set = set_pause
var time: float : get = get_time


var _start_time: int
var _last_pause_time: int
var _time_paused: int
var _stop_time: int = -1


func _init():
	name = "StopwatchRT"
	process_mode = Node.PROCESS_MODE_PAUSABLE


func start() -> void:
	if self.paused:
		Log.w("starting (and resetting) a paused stopwatch. was this intended?")
		paused = false # resetting the value without calling set_pause
	else:
		Log.d("starting (and resetting) stopwatch")

	_start_time = Time.get_ticks_usec()
	_time_paused = 0


func stop() -> void:
	_stop_time = Time.get_ticks_usec()


func get_time() -> float:
	var last_time = _stop_time
	if _stop_time == -1:
		last_time = Time.get_ticks_usec()

	return (last_time - _start_time - _time_paused) * pow(10, -6)


func set_pause(value) -> void:
	if value == paused:
		return
	else:
		paused = value

	if paused == true:
		# set mark
		_last_pause_time = Time.get_ticks_usec()
	else:
		# add length of pause
		_time_paused += Time.get_ticks_usec() - _last_pause_time
