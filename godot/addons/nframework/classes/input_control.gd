class_name InputControl


enum EventType { UNBOUND, KEY, PKEY, MOUSE, JOY, AXIS }
var event_type: int = EventType.UNBOUND

# default to an empty inputeventkey. inputevent base class cannot be initialized
var input_event: InputEvent = InputEventKey.new()
var action: String = ""

# this cannot distinguish between regular key and physical key
var event_type_map := {
	EventType.KEY: InputEventKey.new().get_class(),
	EventType.MOUSE: InputEventMouseButton.new().get_class(),
	EventType.JOY: InputEventJoypadButton.new().get_class(),
	EventType.AXIS: InputEventJoypadMotion.new().get_class(),
	}


# public constructors
func new_key(keycode):
	return _new_event(EventType.KEY, keycode)

func new_pkey(physical_keycode):
	return _new_event(EventType.PKEY, physical_keycode)

func new_mouse(button_index):
	return _new_event(EventType.MOUSE, button_index)

func new_joy(button_index):
	return _new_event(EventType.JOY, button_index)

func new_axis(axis, axis_value):
	return _new_event(EventType.AXIS, [axis, axis_value])

func from_event(input_event: InputEvent) -> InputControl:
	self.input_event = input_event
	event_type = Utils.dict_get_key_of_value(event_type_map, input_event.get_class())
	if event_type == EventType.KEY and input_event.keycode and input_event.physical_keycode:
		# if we get both values from an inputevent, choose one depending on settingscontrols mode
		if SettingsControls.keyboard_mode == SettingsControls.KeyboardMode.PHYSICAL:
			event_type = EventType.PKEY
			input_event.keycode = 0
		else:
			# is it necessary to delete the other keycode?
			input_event.physical_keycode = 0

	return self

func from_serialized(serialized):
	var event_type = EventType[serialized[0]]
	if event_type == EventType.UNBOUND:
		return self

	var serialized_event = serialized[1]

	match event_type:
		EventType.KEY, EventType.PKEY:
			# we read the keycode the same way, but it will be treated different in new_event
			_new_event(event_type, OS.find_keycode_from_string(serialized_event))
		EventType.MOUSE:
			_new_event(event_type, serialized_event)
		EventType.JOY:
			_new_event(event_type, get_joy_button_index_from_string(serialized_event))
		EventType.AXIS:
			var event_data = [
				get_joy_axis_index_from_string(serialized_event[0]),
				serialized_event[1]
				]
			_new_event(event_type, event_data)

	return self


func serialize() -> Array:
	var serialized_event
	match event_type:
		EventType.KEY:
			serialized_event = OS.get_keycode_string(input_event.keycode)
		EventType.PKEY:
			# we store this as the name of the physical keycode key
			# which will actually be different to the actual key if you are on a different layout
			# but that's good enough
			serialized_event = OS.get_keycode_string(input_event.physical_keycode)
		EventType.MOUSE:
			serialized_event = input_event.button_index
		EventType.JOY:
			serialized_event = get_joy_button_string(input_event.button_index)
		EventType.AXIS:
			var axis_name = get_joy_axis_string(input_event.axis)
			var axis_value = input_event.axis_value
			serialized_event = [axis_name, axis_value]

	if event_type == EventType.UNBOUND:
		return ["UNBOUND"]
	else:
		return [EventType.keys()[event_type], serialized_event]


func get_inputevent_name():
	return SettingsControls.get_inputevent_name(input_event)


func _new_event(event_type: int, event_id) -> InputControl:
	match event_type:
		EventType.KEY:
			input_event = InputEventKey.new()
			input_event.keycode = event_id
		EventType.PKEY:
			input_event = InputEventKey.new()
			input_event.physical_keycode = event_id
		EventType.MOUSE:
			input_event = InputEventMouseButton.new()
			input_event.button_index = event_id
			print(input_event)
		EventType.JOY:
			input_event = InputEventJoypadButton.new()
			input_event.button_index = event_id
		EventType.AXIS:
			input_event = InputEventJoypadMotion.new()
			input_event.axis = event_id[0]
			input_event.axis_value = event_id[1]

	self.event_type = event_type
	return self


func _to_string() -> String:
	return "InputControl: %s" % get_inputevent_name()



const _COLEMAK_TRANSLATION = [
	[KEY_E, KEY_F],
	[KEY_R, KEY_P],
	[KEY_T, KEY_G],
	[KEY_S, KEY_R],
	[KEY_D, KEY_S],
	[KEY_F, KEY_T],
	[KEY_G, KEY_D],

	[KEY_Y, KEY_J],
	[KEY_U, KEY_L],
	[KEY_I, KEY_U],
	[KEY_O, KEY_Y],
	[KEY_P, KEY_MINUS],
	[KEY_SEMICOLON, KEY_O],
	[KEY_J, KEY_N],
	[KEY_K, KEY_E],
	[KEY_L, KEY_I],
	[KEY_N, KEY_K],
]


func spoofed_input_event() -> InputEventKey:
	if not input_event is InputEventKey:
		return null

	var spoofed_event = InputEventKey.new()
	var keycode = input_event.keycode
	if not keycode:
		keycode = input_event.physical_keycode

	# change keycode to colemak
	var colemak_scancode = keycode
	for pair in _COLEMAK_TRANSLATION:
		if keycode == pair[0]:
			colemak_scancode = pair[1]
			break
	spoofed_event.keycode = colemak_scancode

	return spoofed_event





# these functions were part of Input in godot3, but they were removed in godot4

# these names are taken from core/input/input.cpp, but you can change them to anything you want
# https://github.com/godotengine/godot/blob/92bee43adba8d2401ef40e2480e53087bcb1eaf1/core/input/input.cpp#L38-L69
const JOY_BUTTONS = [
	"a",
	"b",
	"x",
	"y",
	"back",
	"guide",
	"start",
	"leftstick",
	"rightstick",
	"leftshoulder",
	"rightshoulder",
	"dpup",
	"dpdown",
	"dpleft",
	"dpright",
	"misc1",
	"paddle1",
	"paddle2",
	"paddle3",
	"paddle4",
	"touchpad",
]

const JOY_AXES = [
	"leftx",
	"lefty",
	"rightx",
	"righty",
	"lefttrigger",
	"righttrigger",
]


static func get_joy_button_index_from_string(string: String) -> int:
	return JOY_BUTTONS.find(string)


static func get_joy_axis_index_from_string(string: String) -> int:
	return JOY_AXES.find(string)


static func get_joy_button_string(index: int) -> String:
	if index >= 0 and index < JOY_BUTTONS.size():
		return JOY_BUTTONS[index]
	else:
		return ""


static func get_joy_axis_string(index: int) -> String:
	if index >= 0 and index < JOY_AXES.size():
		return JOY_AXES[index]
	else:
		return ""

