extends StateMachineState
# 1. replace this with a meaningful name
#class_name PlayerState


# 2. declare your typed node here, e.g.:
#var player: Player


func _ready() -> void:
	pass

	# 3. assign your typed node here, e.g.:
#	player = get_parent().get_parent() as Player

	# this previous assignment fails if variable type is different from "as" type
	# 4. assert that the assignment was successful
#	assert(player != null)

