extends StateMachineState


# careful with using _ready or onready because it gets executed early
# before the rest of the scene is ready
# setup is guaranteed to happen after the whole scene has loaded


func setup() -> void:
	super.setup()


func enter(msg) -> void:
	super.enter(msg)


func exit() -> void:
	super.exit()


func input(event: InputEvent) -> void:
	pass


func process(delta: float) -> void:
	pass


func physics_process(delta: float) -> void:
	pass
