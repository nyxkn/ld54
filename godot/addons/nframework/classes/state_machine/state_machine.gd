extends Node
class_name StateMachine


signal transitioned(state_name)


# if you set this node the state machine will autostart and enter the state
@export var autostart_state := NodePath()

# current active state. initialize to an empty state
var state: StateMachineState = StateMachineState.new()

# list of available states
var states: Array


func _ready() -> void:
	# wait for the whole scene to be _ready
	await owner.ready

	states = get_children()
	for s in states:
		s.state_machine = self
		s.setup()

	Log.d(["available states:", states])

	if autostart_state:
		state = get_node(autostart_state)
		state.enter({})


func _unhandled_input(event: InputEvent) -> void:
	state.input(event)


func _process(delta: float) -> void:
	state.process(delta)


func _physics_process(delta: float) -> void:
	state.physics_process(delta)


func change_state(state_name: String, msg: Dictionary = {}) -> void:
#	state_name = state_name.to_pascal_case()

	if not has_node(state_name):
		Log.e(["trying to change to inexistent state:", state_name])
		return

	state.exit()
	state = get_node(state_name)
	state.enter(msg)
	transitioned.emit(state.name)
