extends Node
class_name StateMachineState


# Reference to the state machine, to call its `transition_to()` method directly.
# That's one unorthodox detail of our state implementation, as it adds a dependency between the
# state and the state machine objects, but we found it to be most efficient for our needs.
# The state machine node will set it.
# the alternative is to let state_machine listen to return values for the function it calls on state
# if the state wants to change, it returns the target state
# e.g. in process you can return the name of the new state, and state_machine sees it and changes

# we cannot explicit type because of cyclic reference
# i guess we can in godot4?
var state_machine: StateMachine = null

# the node we're tracking (assumes "owner")
# if you want a different node or you want autocompletion, use state_extend instead
# if you don't need autocompletion, you can derive from here directly
var node


func setup() -> void:
	pass
	Log.d(["setup state", name])

	# assume the node we're tracking is owner (scene root)
	node = owner
	# or alternatively the parent of state_machine
	node = state_machine.get_parent()


func enter(msg: Dictionary) -> void:
	pass
	Log.d(["entered state", name, "with data:", msg])


func exit() -> void:
	pass
	Log.d(["exited state", name])


func input(_event: InputEvent) -> void:
	pass


func process(_delta: float) -> void:
	pass


func physics_process(_delta: float) -> void:
	pass


func change_state(state, msg: Dictionary = {}) -> void:
	state_machine.change_state(state, msg)

