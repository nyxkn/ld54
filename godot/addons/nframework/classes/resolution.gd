class_name Resolution

## resolutions in godot are stored as Vector2

var w: int
var h: int
var description: String
var _aspect_ratio: Fraction : get = get_aspect_ratio


func _init(w: int, h: int,description: String = ""):
	self.w = w
	self.h = h
	self.description = description


func _to_string() -> String:
	return "%sx%s" % [w, h]


func aspect_ratio_string() -> String:
	return str(self._aspect_ratio).replace("/", ":")


func aspect_ratio_float() -> float:
	return self._aspect_ratio.decimal()


func get_aspect_ratio():
	return Fraction.new(w, h).simplified()
