class_name Fraction


var n: int
var d: int


func _init(n: int, d: int):
	self.n = n
	self.d = d


func _to_string() -> String:
	return "%s/%s" % [n, d]


func simplified() -> Fraction:
	# if denumerator is 0 we run into division by zero issues
	# there's nothing to simplify anyway
	if d == 0:
		return self

	var gcd: int = Math.gcd(n, d)
	var sn: int = n / gcd
	var sd: int = d / gcd

	# using the class name inside the class file itself isn't allowed
	# you can use get_script() instead, or load("res://script")
	# TODO hopefully this ugly hack won't be needed in future versions of Godot
#	return Fraction.new(sn, sd)
	return get_script().new(sn, sd)


func decimal() -> float:
	return n / float(d)
