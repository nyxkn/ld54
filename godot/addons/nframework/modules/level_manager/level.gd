extends Node2D

@export var level_name: String = "Level"

func level_cleanup():
	Log.d("level_cleanup called")
	# time to do additional cleanups or wait for things to finish
	# e.g. yield until a sound has stopped playing before you queue_free()
	# this function should not be blocking. this is just to delay the queue_free until all has played
	queue_free()
