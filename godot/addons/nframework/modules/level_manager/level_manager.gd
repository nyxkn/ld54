extends Node

## Level loading system

# for proper background loading, look here:
# https://docs.godotengine.org/en/stable/tutorials/io/background_loading.html

signal level_faded_out # end of fade_out transition
signal level_loaded # loaded in memory, before being added as child
signal level_changed # current_level changed to new level - happens at end of fadeout
signal level_faded_in # end of fade_in transition

var current_level: Node2D = null
var current_level_id: int = 0
var next_level: Node2D = null
var next_level_id: int = -1

var with_transition: bool
var with_loading_screen: bool

# list of res:// filepaths for the level resource files
var level_files := []

@onready var anim: AnimationPlayer = $Transition/AnimationPlayer


func _ready() -> void:
#	init_levels()
	current_level = $EmptyLevel


func init_levels(custom_path: String = "") -> void:
	var path = Config.levels_path
	if custom_path:
		path = custom_path

	# naming sytles ideas: level_02, level02, l02, map02, m02
	# it's important to be consisted because of automatic ordering
	var regex = RegEx.new()
	regex.compile("level_\\d{2}\\.tscn")

	level_files = FileUtils.get_files_in_dir(path, "", regex)
	level_files.sort()


func change_level(level_id: int, transition: bool = true, loading_screen: bool = false) -> void:
	next_level_id = level_id
	with_transition = transition
	with_loading_screen = loading_screen

	if next_level_id >= level_files.size():
		Log.e("attempting to load inexistent level with id: " + str(next_level_id), name)
		return

	if with_transition:
		anim.play("fade_out")
		await anim.animation_finished
		level_faded_out.emit()

	if with_loading_screen:
		$LoadingScreen/Content/LevelName.text = "Level " + str(next_level_id).pad_zeros(2)
		$LoadingScreen/Timer.start()
		$LoadingScreen/Content.show()
		# start play loading animation
		await get_tree().process_frame # wait 1 frame for things to show up

	load_level()
	level_loaded.emit()

	if with_loading_screen:
		await $LoadingScreen/Timer.timeout
		# if load_level takes longer we just end up waiting on this screen
		# you'll still have to implement async loading if you want to play animations
		$LoadingScreen/Content.hide()

	switch_level()
	level_changed.emit()

	if with_transition:
		anim.play("fade_in")

	# send faded_in as the last signal even if we didn't play the transition
	# so we can always reliably connect to the end of the loading
	level_faded_in.emit()


func load_level() -> void:
	var NextLevel: PackedScene = load(level_files[next_level_id])
	next_level = NextLevel.instantiate()
	next_level.visible = false
#	next_level.layer = -1


func switch_level() -> void:
	add_child(next_level)

	if current_level.has_method("level_cleanup"):
		current_level.level_cleanup()
	else:
		remove_child(current_level)
		current_level.queue_free()

	current_level = next_level
	current_level_id = next_level_id
	next_level = null
	next_level_id = -1

	current_level.visible = true

