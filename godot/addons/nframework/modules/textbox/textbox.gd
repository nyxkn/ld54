extends CanvasLayer
# this could also be autoload

# https://www.youtube.com/watch?v=QEHOiORnXIk
# this needs to be fed one piece of text at a time
# you also have to make sure that you don't feed it too long of a line as it will be clipped
# maybe once you settle on a resolution and font, determine the max amount of characters and give out a warning when longer text is added
#
# an alternative approach would be to have it read the complete text and automatically split it in paragraphs - sounds hard

# USAGE
# drag and drop into your scene. make editable so you can resize it
# simply call queue_text() to queue a single line at a time, or set_text() for an array of lines - textbox will start reading out the text

# !!! setting textbox pause mode to process is broken if it's a canvaslayer. works if it's a node. investigate

signal textbox_reading
signal textbox_finished
signal textbox_cleared

enum State {
	CLEAR,
	READING,
	FINISHED
   }

# characters per second
# don't keep this too low or you'll annoy players
# something around 60 seems good
const CHAR_READ_RATE: int = 60

var always_visible: bool = false
var interactive: bool = true

var current_state: int = State.CLEAR
var text_queue := []

@onready var textbox_container: MarginContainer = $TextBoxContainer
@onready var label_start_symbol: Label = $TextBoxContainer/MarginContainer/HBoxContainer/StartSymbol
@onready var label_end_symbol: Label = $TextBoxContainer/MarginContainer/HBoxContainer/EndSymbol
@onready var label_text: Label = $TextBoxContainer/MarginContainer/HBoxContainer/Text


func _ready() -> void:
	reset_textbox()


func _process(delta) -> void:
#	if !F.paused:
	match current_state:
		State.CLEAR:
			if !text_queue.is_empty():
				show_textbox()
				display_next_text()
		State.READING:
			if Input.is_action_just_pressed("ui_accept") and interactive:
				$Tween.stop_all()
				label_text.percent_visible = 1.0
				text_finished()
		State.FINISHED:
			if Input.is_action_just_pressed("ui_accept") and interactive:
				if text_queue.is_empty():
					change_state(State.CLEAR)
					reset_textbox()
				else:
					display_next_text()



# for non-interactive textbox, you need to call this manually
# otherwise it's just waiting for the ui_accept command
func reset_textbox() -> void:
	change_state(State.CLEAR)
	label_start_symbol.text = ""
	label_end_symbol.text = ""
	label_text.text = ""
	if always_visible:
		textbox_container.show()
	else:
		textbox_container.hide()



func show_textbox() -> void:
	label_start_symbol.text = "*"
	textbox_container.show()


func display_next_text() -> void:
	label_end_symbol.text = ""
	var next_text = text_queue.pop_front()
	label_text.text = next_text
	change_state(State.READING)

	label_text.percent_visible = 0.0
	var tween = create_tween().set_trans(Tween.TRANS_LINEAR).set_ease(Tween.EASE_IN_OUT)
	tween.tween_property(label_text, "percent_visible", 1.0, len(label_text.text) * 1.0/CHAR_READ_RATE)


func change_state(next_state) -> void:
	current_state = next_state
	match current_state:
		State.CLEAR:
			textbox_cleared.emit()
		State.READING:
			textbox_reading.emit()
		State.FINISHED:
			textbox_finished.emit()
#	Log.d(State.keys()[current_state], name)


func queue_text(next_text) -> void:
	text_queue.push_back(next_text)


func set_text(text_array) -> void:
	text_queue = text_array


func text_finished() -> void:
	label_end_symbol.text = "v"
	change_state(State.FINISHED)


func _on_Tween_tween_completed(object, key) -> void:
	text_finished()
