extends Node2D


## https://www.reddit.com/r/godot/comments/nimkqg/how_to_break_a_2d_sprite_in_a_cool_and_easy_way/


const Shard = preload("res://addons/nframework/modules/shatter/shard.tscn")

# sets the number of break points. resulting shards will be (subdivisions + 1) * 2. it's always an even number
@export var subdivisions := 3 # (int, 200)
#export var threshold: Vector2 := Vector2(0.1, 0.1)
@export var min_impulse: float = 10.0 # impulse of the shards upon breaking
@export var max_impulse: float = 200.0
@export var min_torque_magnitude: float = 100.0
@export var max_torque_magnitude: float = 200.0
@export var gravity_scale: float = 0.1
#export var lifetime: float := 5.0 # lifetime of the shards
@export var debug_draw: bool = true # debugging: display sprite triangulation

class GeneratedShards:
	var triangles = []
	var shards = []
	var points = []
var generated_shards

var sprite: Sprite2D


func _ready() -> void:
	assert(get_parent() is Sprite2D, "shatter node needs to be a child of a sprite")
	sprite = get_parent()
#	call_deferred("_generate_shards")


func _draw() -> void:
	if generated_shards and debug_draw:
		DrawUtils.draw_points(self, generated_shards.points, Color.WHITE, 4)

		for t in generated_shards.triangles:
			DrawUtils.draw_triangle(self, t)


# for testing
#func _unhandled_input(event: InputEvent) -> void:
#	if event is InputEventKey and event.is_pressed():
#		if event.keycode == KEY_B:
#			shatter()


func shatter() -> void:
	# you can also move generate_shards to ready so you precompute everything in advance
	generated_shards = _generate_shards()
	queue_redraw()

	sprite.self_modulate.a = 0
#	modulate = Color.RED
	for shard in generated_shards.shards:
		shard.get_node("CollisionPolygon2D").disabled = false
		shard.get_node("VisibleOnScreenNotifier2D").screen_exited.connect(on_Shard_screen_exited.bind(shard))
		shard.gravity_scale = gravity_scale
#		shard.mode = RigidBody2D.MODE_RIGID

		# wait a frame after activating physics before sending impulses
		await get_tree().physics_frame

		# disable the collision mask if you don't want shards to collide with each other
		# but you probably want to actually change shard's collision layer so it doesn't interfere with your game
#		shard.collision_mask = 0b0
		var direction = Vector2.UP.rotated(F.rng.randf_range(0, TAU))
		var impulse = randf_range(min_impulse, max_impulse)
		shard.apply_central_impulse(direction * impulse)
		# apply torque requires an enabled collision shape,
		shard.apply_torque_impulse(
			((F.rng.randi_range(0, 1) * 2) - 1) * F.rng.randf_range(min_torque_magnitude, max_torque_magnitude))
		shard.show()


func _generate_shards() -> GeneratedShards:
	var rect = sprite.get_rect()

	var data = GeneratedShards.new()

	# add outer frame points
	data.points.append(rect.position)
	data.points.append(rect.position + Vector2(rect.size.x, 0))
	data.points.append(rect.position + Vector2(0, rect.size.y))
	data.points.append(rect.end)

	# this is how far away from the edge we start splitting, in % of the size of the sprite
	# prevents slim triangles being created at the sprite edges
	# shard_size is the average size assuming an even subdivision.
	# divided by two because e.g. if there's 6 triangles, that only means 3 per axis
	# the rationale here is that if there's lots of subdivisions and you keep a big percentage
	# you'll have much bigger triangles at the edges
	var shard_size = 1.0 / ((subdivisions+1) * 2 / 2)
	var threshold = Vector2(min(0.1, shard_size), min(0.1, shard_size))
	var offset = rect.size * threshold

	# add random break points, taking edge distance threshold into account
	for i in subdivisions:
		var p = rect.position + Vector2(
			F.rng.randi_range(0 + offset.x, rect.size.x - offset.x),
			F.rng.randi_range(0 + offset.y, rect.size.y - offset.y))
		data.points.append(p)

	# calculate triangles
	var delaunay = Geometry2D.triangulate_delaunay(data.points)
	for i in range(0, delaunay.size(), 3):
		data.triangles.append(
			[data.points[delaunay[i]], data.points[delaunay[i + 1]], data.points[delaunay[i + 2]]])

	# create RigidBody2D shards
	for t in data.triangles:
		var shard = Shard.instantiate()
		shard.hide()
		add_child(shard)
		data.shards.append(shard)

		# setup polygons & collision shapes
		var polygon_2d = shard.get_node("Polygon2D")
		var collision_polygon_2d = shard.get_node("CollisionPolygon2D")
		var visibility_notifier_2d = shard.get_node("VisibleOnScreenNotifier2D")

		polygon_2d.texture = sprite.texture
#		polygon_2d.color = Utils.random_color()
		polygon_2d.polygon = t
		# uv coordinates have top left corner at (0,0) of the texture and bottom right corner at (size,size)
		# also note that in shaders, uv coordinates are normalized [0,1], so the same two corners are (0,0) and (1,1)
		# this is different from texture rect coordinates where (0,0) is at the center
		# so we need to offset our coordinates to get the correct uv values
		var uv_t := []
		for v in t:
			uv_t.append(v + rect.size / 2.0)
		polygon_2d.uv = uv_t

		# you can shrink the polygons by a pixel or two so that they don't overlap. does it matter though?
#		collision_polygon_2d.polygon = Geometry.offset_polygon(t, -2)
		collision_polygon_2d.polygon = t

		# setting this to the whole texture just cause it's easier, even if it's actually smaller
		visibility_notifier_2d.rect = rect

	return data


# alternatively, you could have a timer that simply cleans up all shards at once like after 10s from shatter
# or you could want to not delete them at all and keep them around!
func on_Shard_screen_exited(shard) -> void:
	shard.queue_free()
