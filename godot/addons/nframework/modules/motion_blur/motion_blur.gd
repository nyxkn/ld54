extends Node

@export var copies: int = 4

var sprite
var copiable_sprite

var trail_copies := []
var last_position := Vector2.ZERO


func _ready() -> void:
	var parent = get_parent()
	assert(parent is Sprite2D, "motionblur needs to be attached to a sprite")
	sprite = parent
	copiable_sprite = copy_sprite(sprite)


# you could call this in update rather than in _ready if you need to
# but then something more efficient might be best
# like recreating the sprite with the parameters you need
# we only need the texture, transform, and all sprite2d values
func copy_sprite(sprite: Sprite2D) -> Sprite2D:
	var copy = sprite.duplicate()
	for c in copy.get_children():
		copy.remove_child(c)
	return copy


func _process(delta: float) -> void:
	update_trail()
	last_position = sprite.global_position


func update_trail():
	if last_position != Vector2.ZERO:
		for i in copies:
#			var copy = copy_sprite(sprite)
			var copy = copiable_sprite.duplicate()
			copy.set_as_top_level(true)
			copy.global_position = lerp(last_position, sprite.global_position, (i+1) / float(copies))
			copy.rotation = sprite.rotation
			sprite.add_child(copy)
			trail_copies.append([copy, 1.0])

	for s in trail_copies:
		s[1] = lerp(s[1], 0.0, 0.4)
		s[0].modulate.a = s[1]
		if s[1] <= 0.01:
			s[0].hide()
			s[0].queue_free()
			trail_copies.erase(s)
