extends Node2D


const TileCollision := preload("tile_collision.tscn")

@export var grid_size: int = 32


func _ready() -> void:
	var level_path = "res://assets/ldtk/Level_0/"
	# or you can manually set the texture of the Composite sprite node in the editor
	$Composite.texture = load(level_path + "_composite.png")

	load_collisions()


func load_collisions() -> void:
	var csv = Data.read_csv("res://assets/ldtk/Level_0/Collisions.csv")
	# loop y coord (each line is a y coordinate for the tile)
	for y in csv.size():
		# loop x coord (each column is a x coordinate for the tile)
		for x in csv[y].size():
			var cell = int(csv[y][x])
			if cell == 1:
				var tile_collision: StaticBody2D = TileCollision.instantiate()
				add_child(tile_collision)
				var collision_shape = tile_collision.get_child(0) as CollisionShape2D
				collision_shape.shape.size = Vector2(grid_size, grid_size)
				tile_collision.position = Vector2(x * grid_size + grid_size / 2, y * grid_size + grid_size / 2)


