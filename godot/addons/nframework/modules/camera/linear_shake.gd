extends Node

var camera: Camera2D

var direction := Vector2.ZERO
# pixels
var amount := 0.0
var time := 0.0
var hard := false

@export var shake_speed := 20.0 # (float, 1.0, 50.0)
@export var decay_speed := 5.0 # (float, 0.1, 10.0)

func _ready() -> void:
	var parent = get_parent()
	assert(parent is Camera2D, "attempting to use camera module " + name + " without a parent camera")
	camera = parent


func _process(delta: float) -> void:
	time += delta

	if amount > 0.0:
		var shake_offset = Vector2.ZERO

		# using cos moves the camera suddently on the first frame
		# sin makes the movement smooth
		# both effects work
		var sine
		if hard:
			sine = cos(time * shake_speed)
		else:
			sine = sin(time * shake_speed)
		shake_offset = direction * amount * sine

		camera.offset = shake_offset
		amount -= amount * delta * decay_speed


func shake(direction: Vector2, amount: float, hard: bool = false) -> void:
	self.direction = direction.normalized()
	# reduce the amount of vertical shake based on screen ratio
	# NOTE: in case of screen height > width, this will amplify the vertical component
	self.direction.y *= 1.0 / F.aspect_ratio
	self.amount = amount
	self.hard = hard
	time = 0


func shake_random(amount: float, hard: bool = false) -> void:
	direction.x = F.rng.randf_range(-1.0, 1.0)
	direction.y = F.rng.randf_range(-1.0, 1.0)
	shake(direction, amount, hard)
