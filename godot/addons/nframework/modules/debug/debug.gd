extends Node

# to use: add the debug scene to your scene
# then add things to track with add_reference and add_funcref

# if you move the $Lines node to a different corner, adjust Grow Direction

class Ref:
	var ref
	var tag

	func _init(ref,tag):
		self.ref = ref
		self.tag = tag

var refs := []
var callables := []

var print_vars := {}
#var print_override_tag: String = ""

@onready var ui_lines: Label = $FoldingContent/VBoxContainer/Content/Lines


func _ready():
	pass


func _process(delta):
	ui_lines.text = ""

	for r in refs:
		_add_data(r.ref, r.tag)

	for r in callables:
		var data = r.ref.call()
		_add_data(data, r.tag)

	for tag in print_vars:
		_add_data(print_vars[tag], tag)

	ui_lines.text = ui_lines.text.trim_suffix("\n")


func _add_data(data, tag):
	if typeof(data) == TYPE_ARRAY:
		_add_text_line(tag)
		for i in data.size():
			_add_line(str(data[i]), str("[", i, "]"))
	elif typeof(data) == TYPE_DICTIONARY:
		_add_text_line(tag)
		for k in data:
			_add_line(str(data[k]), str(k))
	else:
		# assume basic unreferenceable type
		_add_line(str(data), tag)


func _add_line(value, tag = ""):
	var prefix = ""
	if tag != "": prefix = str(tag, ": ")
	_add_text_line(str(prefix, value))


func _add_text_line(text):
	if text:
		ui_lines.text += text + "\n"


################################
# public functions

# you can add data to the debug screen in two ways
# 1. you add a reference of your data so it's automatically updated
# 	for this use add_reference and add_funcref
# 2. you manually pass the data in your _process function
# 	for this use print
#
# if data is passed as reference you can add it with add_reference
# if it's a base type (int, float, string, vectors) and can't be passed as reference,
# make a function that returns the data (can be an array of multiple variables)
# and pass the callable of that function
# take note that vector is a base type!
#
# you can optionally pass a tag string to be displayed
# if you use print, the tag is required


func add_reference(ref, tag = ""):
	refs.append(Ref.new(ref, tag))


func add_callable(f, tag = ""):
	callables.append(Ref.new(f, tag))


func print(variable, tag: String):
	print_vars[tag] = variable

