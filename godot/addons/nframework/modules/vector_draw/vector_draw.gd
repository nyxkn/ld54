@tool
extends Node2D


@export var draw_scale: float = 1

var colors := [
	Color(1, 0, 0),
	Color(0, 1, 0),
	Color(0, 0, 1),
	Color(1, 1, 0),
	Color(1, 0, 1),
	Color(0, 1, 1),
]

var vectors := []


func _ready() -> void:
#	randomize()
	for i in 10:
		# this ends up being the same colors every run if you don't call randomize()
		colors.push_back(Utils.random_color())


func _process(delta: float) -> void:
	queue_redraw()


# in 2d, use this as your process function if you're using custom draws. make sure you're calling update() in _process
func _draw() -> void:
	for i in vectors.size():
		var v: Vector2 = vectors[i]
		v = v * draw_scale
		draw_line(position, position+v, colors[i], 4.0)


## draw a set of vectors
## order of the vectors in the array does matter
## the later vectors will be drawn on top of the previous
func draw(vectors: Array) -> void:
	self.vectors = vectors
