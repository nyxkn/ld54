extends Node2D

## add shape nodes as children of this node
## these can be colorrect, polygon2d, and even collisionshape2d
## calling random_point() will return a random point inside of those
## useful for obtaining a random spawn coordinate in a spawn area


## this is the main function you want to call
func random_point() -> Vector2:
	var zone := random_zone()

	var random_point := Vector2.INF

	if zone is CollisionShape2D:
		var size = zone.shape.size

		random_point.x = zone.position.x + F.rng.randi_range(0, size.x) - size.x / 2
		random_point.y = zone.position.y - F.rng.randi_range(0, size.y) + size.y / 2

	elif zone is ColorRect:
		random_point.x = F.rng.randi_range(zone.position.x, zone.position.x + zone.size.x)
		random_point.y = F.rng.randi_range(zone.position.y, zone.position.y + zone.size.y)

	elif zone is Polygon2D:
		random_point = random_polygon_point(zone)

	## NOTE is it better to return this in world space or local space?
	# this transform turns our point into global space coordinates
	return transform * random_point


## choose a random zone
## FIXME to be precise, you should choose an zone with weight proportional to its area
## similar to what we do with weighted_random for polygon triangulations areas
## if you only have one child node then it doesn't matter
func random_zone() -> Node:
	var children = get_children()
	children.shuffle()
	return children[0]


func random_polygon_point(polygon_2d: Polygon2D) -> Vector2:
	# https://www.reddit.com/r/godot/comments/mqp29g/how_do_i_get_a_random_position_inside_a_collision/
	# 1. triangulate the polygon
	# 2. choose a random triangle (the choice being proportional to the triangle's area)
	# 3. find a random point inside the chosen triangle
	var polygon_vertices = polygon_2d.polygon
	var triangulated_polygon = Geometry2D.triangulate_polygon(polygon_vertices)

	var num_triangles: int = triangulated_polygon.size() / 3

	var triangles := []

	# triangulated polygon contains a list of triangle vertices
	# each vertex is in the form of an index value of the polygon vertices
	# so we convert that into a list of actual vertices for easier handling
	# just like an opengl triangle would look like: (v1x, v1y), (v2x, v2y), etc.
	for i in num_triangles:
		var idx = i * 3
		var v1: Vector2 = polygon_vertices[triangulated_polygon[idx]]
		var v2: Vector2 = polygon_vertices[triangulated_polygon[idx+1]]
		var v3: Vector2 = polygon_vertices[triangulated_polygon[idx+2]]
#		triangles.append(v1)
#		triangles.append(v2)
#		triangles.append(v3)
		triangles.append([v1, v2, v3])
	# later thought: why are we doing this 3 at a time? can't we just loop triangulated_polygon?
	# like this: (needs testing, but looks like it should work)
#	for i in polygon_vertices.size():
#		triangles.append(polygon_vertices[triangulated_polygon[i]])

	# calculate areas
	var triangle_areas := []
	for i in num_triangles:
		var sides = get_triangle_sides(triangles, i)
		var a = sides[0]
		var b = sides[1]
		var triangle_area: float = a.cross(b) / 2
		triangle_areas.append(triangle_area)

	var triangle_idx = weighted_random(triangle_areas)

#	var vertices = get_triangle_vertices(triangles, triangle_idx)
	var vertices = triangles[triangle_idx]
	var rtp = random_triangle_point(vertices[0], vertices[1], vertices[2])
	var random_point = Vector2(round(rtp.x), round(rtp.y))

	return random_point


## this takes an array of n weights
## visualize this as number0 having weight0, number1: weight1, number2: weight2, etc...
## then returns a random number [0,n] taking the weight of each number into account
func weighted_random(weights) -> int:
	# https://stackoverflow.com/a/1761646
	var sum_of_weights: float = 0
	for i in weights.size():
		sum_of_weights += weights[i]

	var rnd: int = F.rng.randi_range(0, sum_of_weights)
	var seek: int = 0
	var chosen_idx: int = -1
	for i in weights.size():
		seek += weights[i]
		if rnd <= seek:
			chosen_idx = i
			break
	# just in case there's floating point inaccuracy, just pick the last one
	# but actually everything should be int, not float, no?
	if chosen_idx == -1:
		Log.e(
			["weighted random:", "we should never really get here. but since we did, we got this covered"]
			, name)
		chosen_idx = weights.size() - 1

	return chosen_idx


#func get_triangle_vertices(triangles, triangle_idx) -> Array:
#	var i = triangle_idx * 3
#	var v1: Vector2 = triangles[i]
#	var v2: Vector2 = triangles[i+1]
#	var v3: Vector2 = triangles[i+2]
#
#	return [v1, v2, v3]


func get_triangle_sides(triangles, triangle_idx) -> Array:
#	var vertices = get_triangle_vertices(triangles, triangle_idx)
	var vertices = triangles[triangle_idx]
	var v1 = vertices[0]
	var v2 = vertices[1]
	var v3 = vertices[2]

	var a = v2 - v1
	var b = v3 - v2
	var c = v1 - v3
#	var c = v3 - v1

	return [a, b, c]


# https://www.reddit.com/r/godot/comments/mqp29g/how_do_i_get_a_random_position_inside_a_collision/
# this takes the three vertices of the triangle (not sides)
# confusing as we use a,b,c as the name for the sides in the rest of this script
func random_triangle_point(a, b, c) -> Vector2:
	return a + sqrt(randf()) * (-a + b + randf() * (c - b))
