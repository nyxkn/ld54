@tool
extends Polygon2D

## draw your polygon in in main Wall node
## and we automatically copy that over to the collision polygon

@onready var collision_polygon = $StaticBody2D/CollisionPolygon2D

func _ready() -> void:
	collision_polygon.polygon = PackedVector2Array([])
	collision_polygon.polygon = polygon

