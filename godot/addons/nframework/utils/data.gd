class_name Data

## Helper class for importing data
## this is for dealing with specific data formats
## see fileutils for generic file i/o

## This takes [ [ myname, mytype ] ]
## And turns it into [ { name: myname, type: mytype } ]
## Items are referenced by a number id
## This provides efficient storing and retrieval, but you're not able to access items by named id
static func label_fields_from_array(data: Array, fields: Array) -> Array:
	var array := []
	array.resize(data.size())
	# loop lines
	for l in data.size():
		array[l] = {}
		# loop columns
		for c in fields.size():
			var field_name = fields[c]
			array[l][field_name] = data[l][c]

	return array


## This takes [ [ id, myname, mytype ] ]
## And turns it into [ id = { name: myname, type: mytype, id: id } ]
static func label_fields_from_array_with_id(data: Array, fields: Array) -> Dictionary:
	var dict := {}
	for line in data:
		var id = line.pop_front()
		dict[id] = {}
		dict[id].id = id
		for c in fields.size():
			var field_name = fields[c]
			dict[id][field_name] = line[c]

	return dict


## This takes { id = [ myname, mytype ] }
## And turns it into { id = { name: myname, type: mytype, id: id } }
## Items are referenced by a named id
## Use this instead of labelled_fields_from_array() if you want to access items through a named id
## Requires you to add an additional named id field to all your data
## You can easily derive the standard array above by only taking dict.values()
static func label_fields_from_dict(data: Dictionary, fields: Array) -> Dictionary:
	var dict := {}
	for id in data:
		dict[id] = {}
		# we can optionally add the id key itself as a value
		# so if you only have the object, you can reverse lookup the id easily
		dict[id].id = id
		for c in fields.size():
			var field_name = fields[c]
			dict[id][field_name] = data[id][c]

	return dict



## read csv file into an array of lines
static func read_csv(file_path: String, delim: String = ",", trim_empty_lines: bool = true) -> Array:
	var csv_data := []

	var file = FileAccess.open(file_path, FileAccess.READ)
	if not file.is_open():
		FileUtils.print_file_error(file.get_error(), file_path)
		return []

	while !file.eof_reached():
		var csv_line: Array = file.get_csv_line(delim)
		# skip line if the first field is null
		if trim_empty_lines and csv_line[0] == "":
			continue
		csv_data.push_back(csv_line)

	# don't know why get_csv_line() ends up adding an extra empty line at the end
	# removing it
	if csv_data.back().size() == 1 and csv_data.back()[0] == "":
		Log.d("csv import: removing extra empty line added at the end")
		csv_data.pop_back()
	else:
		Log.d("csv import: no need for removing extra empty line added at the end. investigate")

	file.close()

	return csv_data


## shortcut function to read and import a csv file into a dictionary
static func import_csv_shortcut(file_path: String, delim: String = ";", trim_empty_lines: bool = true):
	var csv_data := read_csv(file_path, delim, trim_empty_lines)
	var fields = csv_data[0]
	return label_fields_from_array_with_id(csv_data, fields)


## Import castledb data into a dictionary
## If id_as_key = true, use the id value as a key to access your item data
static func import_castledb(file_path: String, id_as_key: bool = true) -> Dictionary:
	var file = FileAccess.open(file_path, FileAccess.READ)
	if not file.is_open():
		FileUtils.print_file_error(file.get_error(), file_path)
		return {}

	var test_json_conv = JSON.new()
	test_json_conv.parse(file.get_as_text())
	var cdb_data = test_json_conv.get_data()

	file.close()

	# now let's generate our properly formatted data variable
	# if id_as_key, this assumes the first column is an identification column
	# you probably want to name this "id" or "name"
	# a single word string is ideal but can technically also contain a long "Item Name"
	# it's simply the field by which we'll be accessing our data
	var sheets := {}
	for sheet in cdb_data.sheets:
		var sheet_name: String = sheet.name
		var sheet_columns: Array = sheet.columns # array of columns names and data types
		var id_column_name = sheet_columns[0].name

		if id_as_key:
			var sheet_data: Dictionary = {}
			for line in sheet.lines:
				var entry = line.duplicate()
				entry.erase(id_column_name)
				sheet_data[ line[id_column_name] ] = entry

			sheets[sheet_name] = sheet_data
		else:
			sheets[sheet_name] = sheet.lines

	return sheets



## Import csv data into an array
## This assumes the first line of the csv is a line with the field names
## returns either a dictionary with every entry's key being id
## or a simple array of dictionaries
## DEPRECATED this shouldn't be needed anymore
#static func import_csv(file_path: String, id_as_key: bool = true, delim: String = ";", trim_empty_lines: bool = true):
#	var csv_data := read_csv(file_path, delim, trim_empty_lines)
#
#	# finished reading the csv. now let's generate our properly formatted data variable
#	var fields = csv_data[0]
#	var data = csv_data.slice(1, csv_data.size())
#
#	if id_as_key:
#		var dict_data := {}
#
#		for line in data:
#			dict_data[ line.pop_front() ] = line
#
#		# remove id field from fields
#		fields.pop_front()
#
#		var formatted_data: Dictionary = labelled_fields_from_dict(dict_data, fields)
#		return formatted_data
#	else:
#		var formatted_data: Array = labelled_fields_from_array(data, fields)
#		return formatted_data


