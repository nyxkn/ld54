class_name FileUtils


# check if file exists
static func file_exists(file_path: String) -> bool:
	return FileAccess.file_exists(file_path)


# can file_exists be used instead?
static func dir_exists(dir_path: String) -> bool:
	return DirAccess.dir_exists_absolute(dir_path)


## read content from file
static func read_file(file_path: String, password: String = "") -> String:
	Log.i(["reading from file:", file_path])

	var file
	if password:
		file = FileAccess.open_encrypted_with_pass(file_path, FileAccess.READ, password)
	else:
		file = FileAccess.open(file_path, FileAccess.READ)

	if file == null:
		Log.error(FileAccess.get_open_error(), ["error while opening file: ", file_path])
		return ""

	var content = file.get_as_text()
	file.close()

	return content


## write content to file
static func write_to_file(content: String, file_path: String, password: String = ""):
	Log.i(["writing to file:", file_path])

	# opening with WRITE also truncates the file
	var file
	if password:
		file = FileAccess.open_encrypted_with_pass(file_path, FileAccess.WRITE, password)
	else:
		file = FileAccess.open(file_path, FileAccess.WRITE)

	if file == null:
		Log.error(FileAccess.get_open_error(), ["error while opening file: ", file_path])
		return

	file.store_string(content)
	file.close()


# add popup for confirmation? probably can't from a static function
static func delete_file(file_path: String):
	Log.w(["deleting file:", file_path])
	DirAccess.remove_absolute(file_path)


static func get_files_in_dir_recursive(dir_path: String, ext: String = "", regex = null) -> Array:
	var files := []

	# running once without constraints so that we also read directories
	var dir_files = get_files_in_dir(dir_path)
	for f in dir_files:
		if dir_exists(f):
			files.append_array(get_files_in_dir_recursive(f, ext, regex))

	# running a second time with the required constraints
	files.append_array(get_files_in_dir(dir_path, ext, regex))

	return files


## find files in directory. return matching filenames
## if no ext or regex is provided, list all files (except super.import)
## if ext provided we match file extension. if ext passed has no leading dot we'll add it.
## if regex provided we match regex (ignores ext param)
## TODO named parameters would be better here
## regex is either a compiled RegEx or a string
static func get_files_in_dir(dir_path: String, ext: String = "", regex = null) -> Array:
	if regex is String:
		regex = Utils.compile_regex(regex)
	if ext != "" and not ext.begins_with("."):
		ext = "." + ext

	var matching_files := []

	var dir = DirAccess.open(dir_path)
	if dir == null:
		Log.error(DirAccess.get_open_error(), ["error while opening file: ", dir_path])
		return []


	var dir_files = dir.get_files()
	for file_name in dir_files:
		var matching = false
		if ext == null and regex == null and !file_name.ends_with(".import"):
			matching = true
		elif regex and regex.search(file_name):
			matching = true
		elif ext != null and file_name.ends_with(ext):
			matching = true

		if matching:
			matching_files.append(dir_path + "/" + file_name)

		file_name = dir.get_next()

	return matching_files


## https://docs.godotengine.org/en/stable/classes/class_packedscene.html
## this saves the node and all the nodes it owns
## if you want the children to be saved, make sure node owns them
static func save_scene(dir_path: String, node: Node, tscn: bool = false) -> void:
	if not dir_exists(dir_path):
		Log.e(["directory", dir_path, "does not exist"], "save_scene")
		return

	var scene = PackedScene.new()
	var scene_name = node.name.to_snake_case()
	var extension = ".scn"
	if tscn:
		extension = ".tscn"
	var file_path = dir_path + "/" + scene_name + extension

	var result = scene.pack(node)
	if result == OK:
		var err = ResourceSaver.save(scene, file_path)
		if err != OK:
			Log.error(err, ["an error occurred while saving scene", file_path, "to disk"])


# data is an array of dictionaries, saved as one per line
# this results in a file that's potentially hard to human-read
# while to_json() can technically convert any variable to json,
# you probably want to stick with dictionaries
# this automatically gives you the standard key-value json format
static func save_json_multiline(file_path: String, data: Array) -> void:
	if ! file_path.ends_with(".json") and ! file_path.begins_with(Base.data_path):
		Log.e("for safety, save the json to data/ and use a .json extension", "save_json")
		return

	for dict in data:
		if ! dict is Dictionary:
			Log.e("save_json will only save dictionaries. aborting", "save_json")
			return

	var file = FileAccess.open(file_path, FileAccess.WRITE)
	if file == null:
		Log.error(FileAccess.get_open_error(), ["error while opening file: ", file_path])
		return

	for d in data:
		file.store_line(JSON.new().stringify(d))
	file.close()

	Log.d(["json successfully saved to", file_path])


# returns an array of dictionaries
# json file should contain one dictionary per line
static func read_json_multiline(file_path: String) -> Array:
	var file = FileAccess.open(file_path, FileAccess.READ)
	if file == null:
		Log.error(FileAccess.get_open_error(), ["error while opening file: ", file_path])
		return []

	var data = []
	while file.get_position() < file.get_length():
		# Get the saved dictionary from the next line in the save file
		var test_json_conv = JSON.new()
		test_json_conv.parse(file.get_line())
		var json_data = test_json_conv.get_data()
		data.append(json_data)
	file.close()

	return data


# writes pretty printed json to file
# this takes a single dictionary as data
static func save_json(file_path: String, data: Dictionary) -> void:
	if ! file_path.ends_with(".json") and ! file_path.begins_with(Base.data_path):
		Log.e("for safety, save the json to data/ and use a .json extension", "save_json")
		return

	var file = FileAccess.open(file_path, FileAccess.WRITE)
	if file == null:
		Log.error(FileAccess.get_open_error(), ["error while opening file: ", file_path])
		return

	file.store_string(JSON.stringify(data, "\t"))
	file.close()

	Log.d(["json successfully saved to", file_path])


# reads pretty printed json from file
# (or non-pretty printed, but only one dictionary)
static func read_json(file_path: String) -> Dictionary:
	var file = FileAccess.open(file_path, FileAccess.READ)
	if file == null:
		Log.error(FileAccess.get_open_error(), ["error while opening file: ", file_path])
		return {}

	# does parse_json always return a dictionary?
	var test_json_conv = JSON.new()
	test_json_conv.parse(file.get_as_text())
	var json_data: Dictionary = test_json_conv.get_data()
	file.close()

	return json_data


# data has to be a dictionary of dictionaries
# if you need to save different data types from that, you need to manually implement this
# it's quite simple to do though
static func save_cfg(file_path: String, data: Dictionary, password: String = "") -> void:
	var config = ConfigFile.new()

	for section in data:
		for key in data[section]:
			var value = data[section][key]
			config.set_value(section, key, value)

	if password:
		config.save_encrypted_pass(file_path, password)
	else:
		var err = config.save(file_path)
		if err != OK:
			Log.error(err, ["error while opening file: ", file_path])


# this reads a configfile into a dictionary of dictionaries
static func read_cfg(file_path: String, password: String = "") -> Dictionary:
	var config = ConfigFile.new()

	var err
	if password:
		err = config.load_encrypted_pass(file_path, password)
	else:
		err = config.load(file_path)
	if err != OK:
		Log.error(err, ["error while opening file: ", file_path])
		return {}

	var data = {}
	for section in config.get_sections():
		data[section] = {}
		var keys = config.get_section_keys(section)
		for key in keys:
			data[section][key] = config.get_value(section, key)

	return data


# simply create a script that extends resource
# with no logic and mark variables you want exported with export(type)
# non-export variables won't be saved
# resources are references, and references are unique
# so you cannot instance different versions. you always get a reference to the same thing
static func save_resource(resource: Resource, file_path: String) -> void:
	ResourceSaver.save(resource, file_path)


static func load_resource(file_path: String):
	if ! file_path.ends_with(".tres"):
		# also allow .res?
		Log.e(["make sure you're trying to load a .tres file. current attempt:", file_path], "save_json")
		return

	var resource = load(file_path)
	return resource


# takes an array of variables (any types)
# a note in the docs says that only variables marked with PROPERTY_USAGE_STORAGE will be saved
# not sure what this means
# https://docs.godotengine.org/en/stable/classes/class_file.html#class-file-method-store-var
# objects won't be saved by default but can be. see store_var below
static func save_binary(file_path: String, data: Array) -> void:
	# opening with WRITE also truncates the file
	var file = FileAccess.open(file_path, FileAccess.WRITE)
	if file == null:
		Log.error(FileAccess.get_open_error(), ["error while opening file: ", file_path])
		return

	for d in data:
		# to store full objects, call with second argument true
		# file.store_var(d, true)
		# this can potentially include code
		file.store_var(d)
	file.close()

	Log.d(["binary file successfully saved to", file_path])


# objects won't be loaded by default but can be. see get_var below
static func load_binary(file_path: String) -> Array:
	var file = FileAccess.open(file_path, FileAccess.READ)
	if file == null:
		Log.error(FileAccess.get_open_error(), ["error while opening file: ", file_path])
		return []

	var data = []
	while file.get_position() < file.get_length():
		# to read full objects, call get_var with argument true
		# file.get_var(true)
		# this will run code if it was included in the object
		# so never use this on untrusted sources
		data.append(file.get_var())
	file.close()

	return data


#static func print_file_error(err: int, file_path: String = "", category = "FileUtils"):
#	if category != "FileUtils":
#		category = "FileUtils." + category
#
#	Log.error(["(", category, ")", "error while opening file: ", file_path], err)
