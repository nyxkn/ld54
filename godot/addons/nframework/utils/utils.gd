class_name Utils

## Generic utility functions
## Split into appropriate files if this becomes too large


################################
# fp
################################

static func tail(array: Array):
	return array.slice(1)

static func heads(array: Array):
	return array.slice(0, -1)


################################
# nodes
################################


# recursively get all leaf children of a node
static func get_all_leaf_children(node: Node) -> Array:
	var children := []

	for child in node.get_children():
		if child.get_child_count() > 0:
			children.append_array(get_all_leaf_children(child))
		else:
			children.append(child)

	return children


# recursively get all children of a node
# you can filter by node type. type is just the string name of the class
# you can also get it with Object.new().get_class(), which would return "Object"
# note that this is not as smart as using "is", which checks whether something is or extends a class
# this only matches the class of the node and not its ancestors
static func get_all_children(node: Node, type: String = "") -> Array:
	var children := []

	for child in node.get_children():
		if type == "" or child.get_class() == type:
			children.append(child)

		if child.get_child_count() > 0:
			children.append_array(get_all_children(child))

	return children


static func set_owner_on_all_children(node: Node, owner: Node = null) -> void:
	var children = get_all_children(node)
	for child in children:
		child.owner = owner if owner else node


## store all children of a node into a given array
## node: node to recurse into
## children: array to store children in
# is this the same as get_all_children?
static func store_children_recursive(node: Node, children: Array = [], recurse_level: int = 0) -> void:
#	Log.d("    ".repeat(recurse_level) + "[" + node.name + "]")
	for n in node.get_children():
		if n.get_child_count() > 0:
			children.append(n)
			store_children_recursive(n, children, recurse_level + 1)
		else:
			children.append(n)
#			Log.d("    ".repeat(recurse_level + 1) + "- " + n.name)


## reparent node onto new parent
## optionally update the ownership to the new parent as well
## this isn't generally needed
static func reparent(child: Node, new_parent: Node, reset_owner: bool = false) -> void:
	var old_parent = child.get_parent()
	old_parent.remove_child(child)
	new_parent.add_child(child)
	if reset_owner: child.owner = new_parent


################################
# controls
################################




static func set_margins(margin_container, margin_value: int, margin_rightleft: int = -1) -> void:
	var mc: MarginContainer = margin_container

	if margin_rightleft >= 0:
		mc.add_theme_constant_override("offset_right", margin_rightleft)
		mc.add_theme_constant_override("offset_left", margin_rightleft)
	else:
		mc.add_theme_constant_override("offset_right", margin_value)
		mc.add_theme_constant_override("offset_left", margin_value)

	mc.add_theme_constant_override("offset_top", margin_value)
	mc.add_theme_constant_override("offset_bottom", margin_value)


static func grab_focus_on_mouse_entered(control: Control) -> void:
	control.mouse_entered.connect(control.grab_focus)


# this is useful if using mouse with keyboard because otherwise mouse does not grab focus
static func setup_focus_grabs_on_mouse_entered(control: Control) -> void:
	var children: Array = []
	store_children_recursive(control, children)

	for c in children:
		if c is Button or c is Slider:
			grab_focus_on_mouse_entered(c)


static func grab_first_focus_recursive(control: Control) -> void:
	var children = get_all_children(control)
	for node in children:
		node.grab_focus()
		if node.has_focus():
			Log.d(["set focus on node", node])
			break


################################
# data
################################


# look into a dictionary to find the desired value. return its key.
# because values are not unique, this will return the first match only
# only use this if you know values are unique
static func dict_get_key_of_value(dict: Dictionary, value_search):
	for k in dict.keys():
		if dict[k] == value_search:
			return k
	return null


# dictionaries are ordered, so the order of parameters here matters
# values of dictionary b will overwrite same keys in dictionary a
static func merge_dict(a: Dictionary, b: Dictionary) -> Dictionary:
	var merged = a.duplicate()
	for key in b:
		merged[key] = b[key]

	return merged




# pass the enum itself, and the value you want the name of
static func get_enum_name(enum_def, value):
	var value_position = enum_def.values().find(value)
	return enum_def.keys()[value_position]



################################
# properties
################################


# PROPERTY_USAGE_SCRIPT_VARIABLE marks all the user defined script variables
static func get_properties(node: Node) -> Array:
	var properties = []
	for prop in node.get_property_list():
		if prop.usage & PROPERTY_USAGE_SCRIPT_VARIABLE == PROPERTY_USAGE_SCRIPT_VARIABLE:
			properties.append(prop)
	return properties

# get a node's properties and filter to only the ones that get serialized (exports with hints)
# exported variables will also have PROPERTY_USAGE_DEFAULT set
static func get_export_properties(node: Node) -> Array:
	var properties = []
	for prop in node.get_property_list():
		if prop.usage == (PROPERTY_USAGE_SCRIPT_VARIABLE | PROPERTY_USAGE_DEFAULT):
			# this selects user properties with export flag
			properties.append(prop)

	return properties


################################
# print
################################



## logging of input events in a sane non-overwhelming way
static func log_event(event: InputEvent, function, name):
	var action = "()"
	for a in InputMap.get_actions():
		if event.is_action(a):
			action = str("(", a, ")")

	if event.is_pressed():
		if event is InputEventMouseButton:
			Log.d([event.get_class(), action, function])
		else:
			Log.d([event.as_text(), action, function])


static func print_children(node: Node) -> void:
	for child in node.get_children():
		Log.d(child.name)


static func dump_node(node: Node) -> void:
	print(str("=== ", node.name, " ==="))
	for prop in get_properties(node):
		print(str(prop.name, ": ", node.get(prop.name)))
	print("")


#static func panic(message: String) -> void:
#	# Will automatically print an error message in the console as well.
#	OS.alert(message)
#	# Non-zero exit code to indicate failure.
#	get_tree().quit(1)


## pretty print array
static func printa(array) -> void:
	var string := ""
	for e in array:
		string += str(e, ", ")
	print(string.trim_suffix(", "))


## ideally varargs
static func indent_print(tabs, str):
	print("\t".repeat(tabs), str)


static func pretty_print(data, indent = 0):
	if data is Array:
		indent_print(indent, "[")
	else:
		indent_print(indent, "{")

	for e in data:
		var key
		var value
		if data is Array:
			value = e
		else:
			key = e
			value = data[e]

		if value is Dictionary:
			if key != null:
				indent_print(indent+1, str(key, ":"))
			pretty_print(value, indent + 1)
		elif value is Array:
			pretty_print(value, indent + 1)
		else:
			indent_print(indent+1, str(key, ": ", value))
	indent_print(indent, "]" if data is Array else "}")




################################
# misc
################################


# helper to grab the screen size
# this is the size of the viewport in pixels
# not necessarily the same as the actual window size
# to get the window size, use framework.viewport_size
static func get_screen_size() -> Vector2:
	return Vector2(
		int(ProjectSettings.get_setting("display/window/size/viewport_width")),
		int(ProjectSettings.get_setting("display/window/size/viewport_height"))
	)


## return a random color
static func random_color() -> Color:
	return Color(randf_range(0,1), randf_range(0,1), randf_range(0,1))


## simple benchmark function. call this for 10-20 times to simulate a couple seconds lock
static func expensive_function(n: int = 1) -> void:
	for i in n:
		var lst = []
		for j in 999999:
			lst.append(sqrt(i))


# returns the time in seconds it takes to run function "f", "count" times
# 10^6 is a good number where quick single line operations take about 1s
# 10^7 takes about 10s
static func benchmark_function(f: Callable, argv: Array = [], count: int = pow(10, 6)) -> void:
	var start_time: float = Time.get_ticks_msec()
	for i in count:
		f.call(argv)
	var end_time: float = Time.get_ticks_msec()
	var total_time = end_time - start_time
	total_time = total_time / 1000.0
	Log.d(["function took", total_time, "s to run", count, "times"])


## string or regex
## if regex we just return it
## if string we compile the regex
static func compile_regex(regex_string: String) -> RegEx:
	var re := RegEx.new()
	re.compile(regex_string)
	if not re.is_valid():
		Log.e(['warning: regex', regex_string, 'is not valid'], 'compile_regex')
	return re


# for full filename, simply use String.get_file()
static func get_file_name_no_ext(path: String) -> String:
	var file_name = path.get_file().split(".")[0]
	return file_name



