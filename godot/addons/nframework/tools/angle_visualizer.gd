@tool
extends Control

## input values into radians or degrees to visualize the angle
## for radians, you can input numbers or constants (PI, TAU)
## and even do arithmetics with them, e.g. TAU/4
## also works for remapping values > TAU to [-TAU, TAU]


@export var radians: float = 0.0 : set = set_radians
@export var degrees: float = 0.0 : set = set_degrees


func _ready() -> void:
	pass


func _process(delta: float) -> void:
	queue_redraw()


func set_radians(value) -> void:
	radians = fmod(value, TAU)
	# setting variables in your own class doesn't trigger setget
	# to trigger setget you would have to access the variable through self.
	degrees = rad_to_deg(radians)
	update_ui()


func set_degrees(value) -> void:
	degrees = fmod(value, 360)
	radians = deg_to_rad(degrees)
	update_ui()


func update_ui() -> void:
	# printing for convenience if you need to copy the value
	print("radians: " + str(radians))
	print("degrees: " + str(degrees))
	print("--------")
	$Display/Radians.value = str(snapped(radians, 0.01))
	$Display/Degrees.value = str(snapped(degrees, 0.1))
	# this is used to update the export entry in the inspector to match the new value
	notify_property_list_changed()


# in 2d, use this as your process function if you're using custom draws. make sure you're calling update() in _process
func _draw() -> void:
#	draw_circle(Vector2.ZERO, 10.0, Color.WHITE)
	var radius = 100.0
	draw_arc(Vector2.ZERO, radius, 0, TAU, 128, Color.WHITE, 2.0, false)

	var angle_vector = Vector2(cos(radians), -sin(radians))
	draw_line(Vector2.ZERO, angle_vector * radius, Color.CRIMSON, 2.0)



#func _on_RadiansValue_text_entered(new_text: String) -> void:
#	radians = float(new_text)
#
#
#func _on_DegreesValue_text_entered(new_text: String) -> void:
#	degrees = float(new_text)
