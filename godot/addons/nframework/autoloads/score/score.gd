extends Node

# pretending this is a class - so we know how it's structured
# but not using an actual class because saving that to file is a pain
const SCORE_DATA := {
	"timestamp": 0,
	"score": 0.0,
#	"metadata": {},
#	"online_score_id": "",
}

var default_board_name = "main"

var best_score := {}
var last_score := {}

@onready var save_path = Base.user_path + "scores.json"
@onready var SilentWolf = get_tree().root.get_node_or_null("SilentWolf")

func _ready() -> void:
	if Base.debug_build:
		default_board_name = "main_debug"

	load_scores()


func save_scores() -> void:
	var all_scores := [last_score]

	if FileUtils.file_exists(save_path):
		var data = FileUtils.read_json(save_path)
		if data.all_scores:
	#		all_scores.push_front(data.all_scores)
			all_scores.append_array(data.all_scores)

	var scores = {"best_score": best_score, "last_score": last_score, "all_scores": all_scores}
	FileUtils.save_json(save_path, scores)


func load_scores() -> void:
	if not FileUtils.file_exists(save_path):
		return

	var data = FileUtils.read_json(save_path)
	if data.last_score and data.best_score:
		last_score = data.last_score
		best_score = data.best_score


func new_score(score: float, metadata := {}) -> void:
	last_score = score_data_new(Time.get_unix_time_from_system(), score, metadata)

	if best_score == {} or last_score.score > best_score.score:
		best_score = last_score

	if Config.silentwolf:
		var online_score_id = await save_score_online("player_name", score, "", metadata)

		if online_score_id:
			last_score.online_score_id = online_score_id

	save_scores()


func score_data_new(timestamp, score, metadata) -> Dictionary:
	var score_data = SCORE_DATA.duplicate(true)
#	score_data.timestamp = timestamp
	score_data.timestamp = Time.get_datetime_string_from_unix_time(timestamp)
	score_data.score = score
	if metadata:
		score_data.metadata = metadata
	return score_data


func save_score_online(player_name: String, score: float, board_name: String = "", metadata: Dictionary = {}) -> String:
#	local_scores.append({"player_name": player_name, "score": score, "metadata": metadata, "timestamp": Time.get_unix_time_from_system()})

	if board_name == "":
		board_name = default_board_name

	var sw_result: Dictionary = await SilentWolf.Scores.save_score(
		player_name, score, board_name, metadata
		).sw_save_score_complete
	# do we need this if test?
	if sw_result.score_id:
		Log.d(["score saved online successfully: ", sw_result.score_id])
		return sw_result.score_id
	else:
		return ""


func get_scores_online(board_name: String = "") -> Array:
	if board_name == "":
		board_name = default_board_name

	print(SilentWolf.config)
	print(board_name)

	var sw_result: Dictionary = await SilentWolf.Scores.get_scores(0, board_name).sw_get_scores_complete
#	print("All scores: " + str(SilentWolf.Scores.scores))

	#print(SilentWolf.Scores.leaderboards[board_name])
	#return {}
	return SilentWolf.Scores.leaderboards[board_name]

