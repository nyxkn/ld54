extends Node


var scores := {}

@onready var save_path = Config.scores_path + "highscore.cfg"


func _ready() -> void:
	load_scores()


func save_scores() -> void:
	FileUtils.save_cfg(save_path, scores)


func load_scores() -> void:
	scores = FileUtils.read_cfg(save_path)

#	if not scores:
#		 scores = {
#			"best": { score = -1, timestamp = -1 },
#			"last": { score = -1, timestamp = -1 }
#		}


func new_score(score: int) -> void:
	scores.last.score = score
	scores.last.timestamp = Time.get_unix_time_from_system()

	if score >= scores.best.score:
		scores.best = scores.last.duplicate()

	save_scores()
