extends Control



# [name, score, unix_time ]
# you can then use get_datetime_from_unix_time to read unix_time
var local_scores := [{}]

@onready var save_path = Config.scores_path + "leaderboard.json"
#@onready var SilentWolf = get_tree().root.get_node("SilentWolf")

func _ready() -> void:
	pass
	# TODO should we move silentwolf initialization from main to here?
	local_scores = FileUtils.read_json_serialized(save_path)


func save_local_scores() -> void:
	FileUtils.save_json_serialized(save_path, local_scores)


# metadata should be a dictionary, same as what silentwolf requires
# https://silentwolf.com/leaderboard
func add_score(player_name: String, score: int, board_name: String = "", metadata: Dictionary = {}) -> void:
#	local_scores.append({"player_name": player_name, "score": score, "metadata": metadata, "timestamp": Time.get_unix_time_from_system()})

	if board_name == null:
		board_name = "main"

	if Config.silentwolf:
		var sw_result: Dictionary = await SilentWolf.Scores.save_score(
			player_name, score, board_name, metadata
			).sw_save_score_complete
		Log.d(["score saved successfully: ", sw_result.score_id])


# this all needs rework for godot4
# what is this file anyway?

#func get_local_scores() -> Array:
#	local_scores.sort_custom(Callable(Sort,"by_score"))
#	return local_scores
#
#
#func is_score_in_sw(score):
#	var sw_scores = SilentWolf.Scores.scores
#	for sw_score in sw_scores:
#		if score.player_name == sw_score.player_name and score.score == sw_score.score:
#			# if timestamp == ? or is very close together in time
#			return true
#	return false
#
#
#func merge_sw_with_local_scores():
#	var combined_scores: Array = SilentWolf.Scores.scores
#	for score in local_scores:
#		if ! is_score_in_sw(score):
#			combined_scores.append(score)
#	combined_scores.sort_custom(Callable(Sort,"by_score"))
#
#	return combined_scores
#
#
#func sw_fetch_scores():
#	# the number of top scores to fetch. 0 to fetch all
#	var top_n = 10
#	var board_name = "main"
#
#	await SilentWolf.Scores.get_high_scores(top_n, board_name).sw_scores_received
#
#	# are scores saved in both of these two following variables?
#	Log.d("Scores: " + str(SilentWolf.Scores.scores), name)
#	Log.d("Scores: " + str(SilentWolf.Scores.leaderboards[board_name]), name)
#	for score in SilentWolf.Scores.scores:
#		Log.d([score.player_name, str(int(score.score)), str(score.metadata), str(score.score_id), str(score.timestamp)])
#
#
#class Sort:
#	static func by_score(a, b):
#		if a.score > b.score:
#			return true
#		elif a.score == b.score:
#			if a.timestamp < b.timestamp:
#				return true
#		return false
#
#	#static func by_timestamp(a, b):
#	#	if a.timestamp < b.timestamp:
#	#		return true
#	#	return false
