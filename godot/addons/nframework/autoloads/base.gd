@tool
extends Node

# these are framework-independent settings
# mostly platform / filepaths stuff

################################################################
# CONST

# game data
const data_path: String = "res://data/"
# this folder gets included in the build and in git
const config_path: String = "res://config/"
# this folder gets included in the build but NOT in git
const secret_path: String = "res://secret/"
# everything in user does NOT get included in the build
const user_path: String = "user://"


################################################################
# RUNTIME

enum Platform { LINUX, WINDOWS, WEB, ANDROID, IOS, MACOS, UNKNOWN }
var platform: Platform = Platform.UNKNOWN

var debug_build: bool
var in_editor: bool


func _ready() -> void:
	Log.d('base ready')


func _init():
	Log.d('base init')

	if OS.is_debug_build():
		debug_build = true

	if Engine.is_editor_hint():
		in_editor = true

	Log.d("current platform is", OS.get_name())
	match OS.get_name():
		"Windows", "UWP":
			platform = Platform.WINDOWS
		"macOS":
			platform = Platform.MACOS
		"Linux", "FreeBSD", "NetBSD", "OpenBSD", "BSD":
			platform = Platform.LINUX
		"Android":
			platform = Platform.ANDROID
		"iOS":
			platform = Platform.IOS
		"Web":
			platform = Platform.WEB

