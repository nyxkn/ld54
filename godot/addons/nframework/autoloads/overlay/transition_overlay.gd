extends CanvasLayer


signal fade_out_completed
signal fade_in_completed

@export var default_duration: float = 0.25
@export var fade_in_alpha: float = 0.0
@export var fade_out_alpha: float = 1.0

var fade_progress: float = 0: set = set_fade_progress

var fading: float = false

@onready var black_screen: ColorRect = $BlackScreen


func _ready() -> void:
	black_screen.modulate.a = 0

# if you need to drop this scene into another, you can set colorrect.color to alpha0 in the inspector
# and enable this here so the black screen doesn't get in the way
#	black_screen.color.a = 1


func fade_out(custom_duration: float = -1.0) -> void:
	var duration = default_duration
	if custom_duration > 0:
		duration = custom_duration

	# linear seems nice. possibly try a gentle sine or quad
	var tween = create_tween().set_trans(Tween.TRANS_LINEAR).set_ease(Tween.EASE_IN)
	fading = true
#	fade_progress = fade_in_alpha
	tween.tween_property(self, "fade_progress", fade_out_alpha, duration)
	tween.tween_callback(_faded_out)


func fade_in(custom_duration: float = -1.0) -> void:
	var duration = default_duration
	if custom_duration > 0:
		duration = custom_duration

	# sine or quad seem good
	# my thinking is that you want the final portion of the fade_in to be quick not to confuse the player
	# otherwise it's annoying when you're ready to play but controls won't activate because the transition is still going
	var tween = create_tween().set_trans(Tween.TRANS_SINE).set_ease(Tween.EASE_IN)
#	fade_progress = fade_out_alpha
	tween.tween_property(self, "fade_progress", fade_in_alpha, duration)
	tween.tween_callback(_faded_in)


# you could be animating either color or modulate
# color: only changes the colorrect color - in this case you need canvas to be on a higher layer
# modulate: (property of canvasitem) modulates everything drawn on that canvas. can be on same layer as the rest
# e.g. children of colorrect as well - can be more versatile
func set_fade_progress(value: float) -> void:
	fade_progress = clamp(value, 0.0, 1.0)
	# fade logic
	black_screen.modulate.a = fade_progress


func _faded_out() -> void:
	fade_out_completed.emit()


func _faded_in() -> void:
	fading = false
	fade_in_completed.emit()
