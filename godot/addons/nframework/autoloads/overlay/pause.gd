extends CanvasLayer

## this is the pause system
## all of the ui can be replaced with anything you want
## it doesn't necessarily have to be a full menu
## e.g. a basic alpha colorrect with a "paused" title in the middle
## eventually you might consider splitting the ui part of this in a different scene
## that will make more sense if you want to support different pause screen ui layouts

# public

# each scene is responsible for allowing the pause menu
# it is automatically disabled on scene change
#var allowed: bool = false : set = set_allowed

# private
var settings_opened: bool = false
var settings_menu: Control = null

var allowed_from = null

# this needs to be onready otherwise it will get called before Config has time to initialize scenes
@onready var SettingsMenu = load(Config.scenes.settings_menu)

@onready var button_resume: Button = find_child("Resume")
@onready var pause_menu = %PauseMenu
#onready var control: Control = $Screen

@onready var root_control: Control = $Root


func _ready():
	visible = false
	pause_menu.visible = false
	root_control.theme = load(Config.theme)
	Utils.setup_focus_grabs_on_mouse_entered(root_control)


func _input(event) -> void:
	# _input is still called despite the scenetree being paused
	# because process_mode of this node is set to process

	if get_tree().current_scene != allowed_from:
		return

	# this defines pause button behaviour
	# in this case we're pausing and also showing the menu
	if event.is_action_pressed("menu") or event.is_action_pressed("pause"):
		get_viewport().set_input_as_handled()
		if visible and !settings_opened:
			visible = false
			pause_menu.visible = false
			F.paused = false
		else:
			F.paused = true
			visible = true
			# comment next line to disable pause menu
			pause_menu.visible = true
			button_resume.grab_focus()


func allow_from(scene) -> void:
	allowed_from = scene


func _on_Resume_pressed():
	Log.d("resume pressed")
	visible = false
	F.paused = false


func _on_MainMenu_pressed() -> void:
	F.change_scene(Config.scenes.main_menu)

	# TransitionOverlay has to be set to process during pause for this to work
	# otherwise just do the change_scene_to_file without transitions
	await F.scene_faded_out
	visible = false


func _on_Settings_pressed() -> void:
	# we have to hide this so it doesn't conflict with gui navigation in settings
	pause_menu.visible = false

	settings_opened = true
	settings_menu = SettingsMenu.instantiate()
	settings_menu.menu_closed.connect(on_SettingsMenu_closed)
	root_control.add_child(settings_menu)


func on_SettingsMenu_closed() -> void:
#	remove_child(settings_menu)
#	settings_menu.queue_free()
	pause_menu.visible = true
	button_resume.grab_focus()
	settings_opened = false


func _on_restart_pressed() -> void:
	_on_Resume_pressed()
	G.game.restart_level()
