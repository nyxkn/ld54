extends CanvasLayer


func _ready() -> void:
	if Base.debug_build:
		show()
	else:
		hide()
