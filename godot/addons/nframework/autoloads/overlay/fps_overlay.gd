extends CanvasLayer


var enabled: bool : set = set_enabled

var update_interval: float = 1
var timer: float

@onready var root_control: Control = $Root


func _ready() -> void:
	set_enabled(false)
	root_control.theme = load(Config.theme)
	root_control.get_node("FPS").text = ""


func _process(delta: float) -> void:
	timer += delta
	if timer > update_interval:
		timer = 0
		root_control.get_node("FPS").text = str(Engine.get_frames_per_second())


func set_enabled(value: bool) -> void:
	enabled = value
	set_process(enabled)
	visible = enabled
