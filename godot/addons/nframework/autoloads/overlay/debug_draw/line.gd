@tool
extends Node2D
class_name Line


@export var from: Vector2 = Vector2.ZERO
@export var to: Vector2 = Vector2.ZERO

@export var color: Color = Color.WHITE : set = set_color
@export var width: int = 4 : set = set_width # (int, 1, 10)


func _draw() -> void:
	draw_line(from, to, color, width)


func init(from: Vector2, to: Vector2):
	self.from = from
	self.to = to
	queue_redraw()
	return self


func set_from(value):
	from = value
	queue_redraw()

func set_to(value):
	to = value
	queue_redraw()

func set_color(value):
	color = value
	queue_redraw()

func set_width(value):
	width = value
	queue_redraw()
