@tool
extends Node2D
class_name Circle


@export var radius: int = 16 : set = set_radius
@export var color: Color = Color.WHITE : set = set_color

@export var border: bool = false : set = set_border
@export var border_color: Color = Color.BLACK : set = set_border_color
@export var border_width: int = 2 : set = set_border_width # (int, 1, 20)


func _draw() -> void:
	draw_circle(Vector2.ZERO, radius, color)
	if border:
		draw_arc(Vector2.ZERO, radius - (border_width/2.0), 0.0, 2*PI, 128,
			border_color, border_width, true)


func set_radius(value):
	radius = value
	queue_redraw()

func set_color(value):
	color = value
	queue_redraw()

func set_border(value):
	border = value
	queue_redraw()

func set_border_color(value):
	border_color = value
	queue_redraw()

func set_border_width(value):
	border_width = value
	queue_redraw()

