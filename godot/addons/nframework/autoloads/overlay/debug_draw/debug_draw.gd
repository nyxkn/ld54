extends CanvasLayer
#extends Node2D


#onready var canvas := $Canvas
const Circle = preload("circle.tscn")
const Line = preload("line.tscn")


func _ready() -> void:
	pass
#	var canvas_layer = CanvasLayer.new()
#	get_tree().root.add_child(canvas_layer)


func _process(delta: float) -> void:
	pass
#	canvas.update()
#	update()


func _draw() -> void:
	pass


#func draw_line(from, to, color = Color.WHITE) -> void:
#	canvas.draw_line(from, to, color)
#	canvas.update()


func add_circle(position: Vector2, radius: float = -1) -> Circle:
	var circle = Circle.instantiate()
	add_child(circle)
	circle.position = position
	if radius != -1:
		circle.radius = radius
	return circle


func add_line(from: Vector2, to: Vector2, color: Color = Color.TRANSPARENT, width: int = -1) -> Line:
	var line = Line.instantiate().init(from, to)
	add_child(line)
	if color != Color.TRANSPARENT:
		line.color = color
	if width != -1:
		line.width = width
	return line


func add_circle_temp(position: Vector2):
	var circle = add_circle(position)
	await get_tree().create_timer(1.0).timeout
	circle.queue_free()


func add_line_temp(from: Vector2, to: Vector2):
	var line = add_line(from, to)
	await get_tree().create_timer(1.0).timeout
	line.queue_free()
