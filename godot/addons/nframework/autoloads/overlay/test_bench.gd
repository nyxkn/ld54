extends CanvasLayer


func _ready() -> void:
	visible = false


func _input(event: InputEvent) -> void:
	if event is InputEventKey and event.is_pressed():
		if event.keycode == KEY_F1 and Config.d_test_bench:
			self.visible = !self.visible

	if self.visible:
#		Utils.log_event(event, "_input", name)
		for action in InputMap.get_actions():
			if event.is_action_pressed(action):
				Log.d(["action pressed:", action])

func _on_PrintControls_pressed() -> void:
#	SettingsControls.print_default_action_keys()
#	print('========')
#	SettingsControls.print_action_controls()
	SettingsControls.print_inputmap_controls("all")


func _on_Test_pressed() -> void:
	print(SettingsAudio.volumes)

