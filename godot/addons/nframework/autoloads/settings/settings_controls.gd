extends Node

# necessary ui controls
# left down up right, accept, cancel
# in controllers, this is nowadays X for select, O for cancel
# but in older jrpg it's O for select and X for cancel === A and B in SNES, which makes sense
# (and possibly modern japanese ps systems too?)

#  X    Y     ^    3
# Y A  X B  [] O  2 1
#  B    A     X    0
# SNES XBOX PS    NUM

# joy enums in godot4 are now mirroring xbox :(
# https://docs.godotengine.org/en/stable/classes/class_%40globalscope.html#enum-globalscope-keylist

enum Sony {
	CROSS = JOY_BUTTON_A,
	CIRCLE = JOY_BUTTON_B,
	SQUARE = JOY_BUTTON_X,
	TRIANGLE = JOY_BUTTON_Y,
}


# terminology
# control refers to our InputControl class which is a wrapper around an InputEvent
# action is the name of an action to which a control can be assigned. same meaning as in InputMap


# keycode vs physical keycode
# keycode is the key as sent by your os with the selected layout
# e.g. pressing qwerty Q in azerty, gives you A. because that's the letter your os is actually sending
# physical keycode is the key qwerty-translated key of whatever layout you're using
# e.g. pressing qwerty Q in azerty, gives you Q
# so if you care about the letter being sent (S for save), use regular keycode
# if you care about the location, use physical
# now when rebinding, regardless of how you're storing the scancodes, you want to display keys
# in the layout of the player
# e.g. we're using wasd as physical keys, so an azerty user can press his azerty Q to get the right A moveleft
# but in rebinding, you need to show the key as Q, not A
# you can do that with OS.keyboard_get_scancode_from_physical

# so in conclusion, use physical keys, store as physical keys, display them as regular keys
# or if your application cares about key letter instead of location, use regular keys everywhere
# storing of the keys is an issue, because if you store them ar regular keys, and the user changes layout
# then they are wrong. you should probably store them as physical keys
# in the settings file they will now look confusing for non-qwerty users
# so you can either decide not to expose them, or expose them as is and ignore the non-qwerty issues
# config file is very old school anyway and there's probably no need for it to be exposed


# issue: it's probably not possible to mix usage of physical and regular keys
# because let's say we use physical A for left. this is Q on azerty
# so then you cannot also have a regular Q being used for quit, because it now conflicts in azerty

# issue: if we are using hardcoded keys, we need to store them as physical to make everyone happy
# but if we use rebinds, then storing them as physical adds a layer of position complexity when rebinding (but nbd)
# if you want to store them as scancodes to make config file pretty, then you might as well always use scancodes
# because converting from keycode to physical is currently not exposed and probably a silly thought anyway



# you either have multiple control schemes defined
# or you allow rebind
# but probably not both at the same time
# limit rebind keys to one control per action, but multiple controls will of course have multiple
# if you ever need more you like in sauerbraten you'll figure it out then
# it's actually not so much of a code problem but rather a ui issue
# another system is to allow a main keybind and an alternative one, easily displayed in two colums
# i think that's the most you'll ever need, and no need to do more

# so we have three systems
# hardcoded scheme, no rebind. ideally with multiple compatible versions to keep everyone happy
# no need to store harcoded in settings config
# rebind single key. there can only be one key per action and keys can only be used once
# one key per device actually. so gamepad can have a second key
# rebind multiple key. there can be more keys per actions (tipically two), and keys are still unique
# it only makes sense to allow double rebind for keymouse
# anyway, you don't need to worry about this until you need it

enum Mode { HARDCODE, REBIND }
#var mode = Mode.HARDCODE
var mode = Mode.REBIND

# this forces all controls to be either physical or regular (layout-specific)
# the two types of controls cannot be mixed
# i.e. you cannot have physical wasd and layout P for pause, because it will potentially cause conflicts
# of course you can still use either type manually by checking inputevent scancodes in _input functions
# but be careful you know it won't conflict. good use cases are for separate ui controls, or testing
enum KeyboardMode { PHYSICAL, LAYOUT }
#var keyboard_mode = KeyboardMode.LAYOUT
var keyboard_mode = KeyboardMode.PHYSICAL

# if we are in rebind mode, how many binds per action we allow
# only 1 has been tested
const ALLOWED_BINDS = 1


enum GamepadMovement { DPAD = 1, ANALOG = 2 }
var gamepad_movement = GamepadMovement.DPAD | GamepadMovement.ANALOG


# a device is something that requires two hands
# meaning you'll never combine two of these
# but you might want to switch between them since they can coexist
var enabled_devices := ["keymouse"]

# list of all the actions that we want to expose to the user
var enabled_actions := [
	"up", "left", "down", "right", "attack", "block", "pause",
]


# these have to match enabled devices and actions
# you can set either a single key or an array of keys
# buttons for pause can be p (pause), q (quit), space, esc
# esc is always bound to menu which is the same thing
var default_controls := {
	keymouse = {
		# pause
		"pause": key(KEY_P),
#		"pause": key(KEY_Q),
		#"pause": key(KEY_SPACE),

		# directions
		"up": key(KEY_W),
		"left": key(KEY_A),
		"down": key(KEY_S),
		"right": key(KEY_D),
		#"up": key(KEY_E),
		#"left": key(KEY_S),
		#"down": key(KEY_D),
		#"right": key(KEY_F),
#		"up": [key(KEY_UP), key(KEY_W)],
#		"left": [key(KEY_LEFT), key(KEY_A)],
#		"down": [key(KEY_DOWN), key(KEY_S)],
#		"right": [key(KEY_RIGHT), key(KEY_D)],

		# actions
		"attack": mouse(MOUSE_BUTTON_LEFT),
		"block": mouse(MOUSE_BUTTON_RIGHT),
		#"action2": key(KEY_K),
	},
	gamepad = {
		#"pause": joy(JOY_BUTTON_START),
		#"attack": joy(Sony.CIRCLE),
		#"block": joy(Sony.CROSS),
		#"action1": joy(Sony.CIRCLE),
		#"action2": joy(Sony.CROSS),
	}
}


var unbindable_buttons = [ KEY_ESCAPE ]


# private controls are all the controls that do not need to get exposed to the user to change
# these should either be unbindable or ui_ actions that can coexist with the same button used elsewhere
# e.g. using ui_click doesn't prevent you from assigning lmb for shooting
# all of these will never get into "controls", so they stay hidden from the user
# buttons that you don't want to allow rebinding of, put in unbindable_buttons
var private_controls := {
	# actions that we always want defined and some nframework features depend on them
	# these should not belong to user configurable devices
	core = {
		#"click": mouse(MOUSE_BUTTON_LEFT),
		#"rclick": mouse(MOUSE_BUTTON_RIGHT),
		"menu": key(KEY_ESCAPE),
	},

	# quick and dirty way of adding actions
	# think of it as an alternative way to godot's settings menu
	# these are not meant to be saved to the settings file and they won't show up in rebinding
	custom = {

	},
}

var directions = [ "left", "right", "up", "down" ]
# these core gamepad controls never need rebinding and they don't get added to controls
# actually a valid usage of gamepad rebinding is to make sure people can change if godot recognizes device incorrectly
var gamepad_controls := {
	dpad = {
		"left": joy(JOY_BUTTON_DPAD_LEFT),
		"right": joy(JOY_BUTTON_DPAD_RIGHT),
		"up": joy(JOY_BUTTON_DPAD_UP),
		"down": joy(JOY_BUTTON_DPAD_DOWN),
	},

	analog = {
		"left": axis(JOY_AXIS_LEFT_X, -1),
		"right": axis(JOY_AXIS_LEFT_X, 1),
		"up": axis(JOY_AXIS_LEFT_Y, -1),
		"down": axis(JOY_AXIS_LEFT_Y, 1),
	},
}

# keep track of all assigned controls
# controls[device][action] = [controls]
var controls := {}

var restored_from_cfg := false


# this is called before we've loaded from config
func _init():
	for device in enabled_devices:
		controls[device] = {}
		for action in enabled_actions:
			add_action(device, action)


# this is called after we've loaded from config
func init():
	if not restored_from_cfg:
		for device in default_controls:
			if not device in enabled_devices:
				continue
			for action in default_controls[device]:
				if not action in enabled_actions:
					continue

				var control = default_controls[device][action]
				# convenience so that we can specify keys without the array parentheses
				# but controls are always stored in arrays
				if control is Array:
					for c in control:
						add_control(device, action, control)
				else:
					add_control(device, action, control)


	# these should not get saved to config file
	for set in private_controls:
		for action in private_controls[set]:
			InputMap.add_action(action)
#			add_action("keymouse", action)
			var control = private_controls[set][action]
			if control is Array:
				for c in control:
					InputMap.action_add_event(action, control.input_event)
#					add_control("keymouse", action, control)
			else:
				InputMap.action_add_event(action, control.input_event)
#				add_control("keymouse", action, control)

	if "gamepad" in enabled_devices:
		gamepad_setup()


func gamepad_setup():
	# removing all current controls and setting them up again

	for action in controls["gamepad"]:
		if action in directions:
			for c in controls["gamepad"][action]:
				remove_control("gamepad", action, c)

	for set in gamepad_controls:
		if set == "gamepad_dpad" and not (gamepad_movement & GamepadMovement.DPAD):
			continue
		elif set == "gamepad_analog" and not (gamepad_movement & GamepadMovement.ANALOG):
			continue
		for action in gamepad_controls[set]:
			add_control("gamepad", action, gamepad_controls[set][action])


func add_action(device, action):
	if not InputMap.has_action(action):
		InputMap.add_action(action)
	if not action in controls[device]:
		controls[device][action] = []


func add_control(device, action, control: InputControl) -> void:
#	add_action(device, action)

	if control.input_event in InputMap.action_get_events(action):
		Log.e(["trying to re-add the same control to the action:", action, control], name)
		return

	for a in controls[device]:
		for existing_control in controls[device][a]:
#			Log.d([control.input_event.as_text(), existing_control.input_event.as_text()])
			if control.input_event.is_match(existing_control.input_event):
				Log.w(["control is already being used elsewhere, deleting:", a, existing_control])
				remove_control(device, a, existing_control)

	var size = controls[device][action].size()
	if mode == Mode.REBIND and size >= ALLOWED_BINDS and not (device == "gamepad" and action in directions):
		# we make an exception for gamepad directions, which we need two of, and user can't rebind them anyway
		Log.e(["trying to add more than the allowed binds per action", device, action, control], name)
		Log.d(["controls", controls[device][action]])
		return

	if control.input_event is InputEventKey and Config.d_spoof_colemak:
		InputMap.action_add_event(action, control.spoofed_input_event())
	else:
		InputMap.action_add_event(action, control.input_event)
	controls[device][action].append(control)


func remove_control(device, action, control: InputControl) -> void:
	if not controls[device].has(action):
		Log.w(["inexisting action:", action])
		return

	var current_controls: Array = controls[device][action]
	var control_removed = false
	for c in current_controls:
		if c == control:
			InputMap.action_erase_event(action, c.input_event)
			controls[device][action].erase(c)
			control_removed = true

	if not control_removed:
		Log.d(["failed to remove control", device, action, control, " - doesn't exist"])


func replace_control(device, action, old_control: InputControl, new_control: InputControl) -> void:
	remove_control(device, action, old_control)
	add_control(device, action, new_control)


func export_custom_properties() -> Dictionary:
	# if keybinds are hardcoded, we don't want to expose them to settings
	if mode == Mode.HARDCODE:
		return {}

	var serialized_controls = {}

	for device in enabled_devices:
		serialized_controls[device] = {}
		for action in enabled_actions:
			if device == "gamepad" and action in SettingsControls.directions:
				# we don't save gamepad directions. but technically we could just for simplicity
				# we override them anyway by forcing gamepad_setup
				continue
			serialized_controls[device][action] = []
			for i in controls[device][action].size():
				serialized_controls[device][action].insert(i, controls[device][action][i].serialize())

	return {"keybinds": serialized_controls}


func restore_custom_properties(props: Dictionary):
	if mode == Mode.HARDCODE:
		return

	var serialized_controls = props["keybinds"]

	if serialized_controls:
		for device in serialized_controls:
			for action in serialized_controls[device]:
				# only load as many binds as what we are allowing
				for i in min(serialized_controls[device][action].size(), ALLOWED_BINDS):
					var serialized_control = serialized_controls[device][action][i]
					var control = InputControl.new().from_serialized(serialized_control)
					add_control(device, action, control)
		restored_from_cfg = true


#func swap_gamepad(value):
#	if not actions_initialized:
#		await self.actions_initialized
#
#	gamepad_swap = value
#	# retro consoles: 1 is confirm, 0 is cancel (ps O, X)
#	# western modern consoles: 0 is confirm, 1 is cancel (ps X, O)
##	# by default
#
#	Log.d(["swapping gamepad buttons"])
#
#	var button_0 = joy(JOY_BUTTON_0)
#	var button_1 = joy(JOY_BUTTON_1)
#
#	if value == true:
#		# swap to modern
#		replace_action("action1", button_0, "gamepad")
#		replace_action("action2", button_1, "gamepad")
#	else:
#		# default to retro
#		replace_action("action1", button_1, "gamepad")
#		replace_action("action2", button_0, "gamepad")


# shortcuts to create input_events
func key(keycode):
	if keyboard_mode == KeyboardMode.LAYOUT:
		return InputControl.new().new_key(keycode)
	elif keyboard_mode == KeyboardMode.PHYSICAL:
		return InputControl.new().new_pkey(keycode)
#func pkey(physical_keycode):
#	return InputControl.new().new_pkey(physical_keycode)
func mouse(button_index):
	return InputControl.new().new_mouse(button_index)
func joy(button_index):
	return InputControl.new().new_joy(button_index)
func axis(axis, axis_value):
	return InputControl.new().new_axis(axis, axis_value)


func print_default_ui_keys() -> void:
	for a in InputMap.get_actions():
		# exclude/choose ui_ default mappings
		if a.left(3) == 'ui_':
			var log_str = str(a, ": ")
			for input_event in InputMap.action_get_events(a):
				log_str += get_inputevent_name(input_event)
#				if input_event is InputEventJoypadButton:
##					log_str += str("JoypadButton ", input_event.button_index)
#					log_str += get_inputevent_name(input_event)
#				else:
#					log_str += str(input_event.as_text())
				log_str +=  " | "
			log_str = log_str.trim_suffix(" | ")
			Log.d(log_str)


## type can be ui, scheme, custom, all
func print_inputmap_controls(filter: String = "all") -> void:
	print("arst")
#func clear_all_actions() -> void:
	var actions = InputMap.get_actions()
	for action in actions:
		if filter == "ui" and ! action.begins_with("ui_"):
			continue
		if filter == "controls" and ! enabled_actions.has(action):
			continue
		if filter == "all":
			pass

		var input_events = InputMap.action_get_events(action)
		var log_str = str(action, ": ")
		for input_event in input_events:
			var ie_name = get_inputevent_name(input_event)
			if ie_name:
				log_str += ie_name
#				log_str += input_event.as_text()
			else:
				log_str += "[unbound]"

			log_str += " | "
		log_str = log_str.trim_suffix(" | ")
		Log.d(log_str)


# this is only for presentation purposes. not for internal usage
static func get_inputevent_name(event: InputEvent) -> String:
	var text: String = ""
	if event is InputEventKey:
		# default to treating an event as keycode (even if we also get a physical keycode like from _input functions)
		if event.keycode:
			# if we get both values from an inputevent, choose one depending on settingscontrols mode
#			if SettingsControls.keyboard_mode == SettingsControls.KeyboardMode.PHYSICAL:
#				pass
			text += event.as_text()
		else:
			# this is a hack to get the right name for a physical key converted to local layout
			# essentially get the position of physical into current layout and then print that out
			var physical_event := InputEventKey.new()
			physical_event.keycode = DisplayServer.keyboard_get_keycode_from_physical(event.physical_keycode)
			text += physical_event.as_text()

		# uncomment this line to show physical
#		text = text.replace(" (Physical)", "")
	elif event is InputEventMouseButton:
#		text += "Mouse"
		var as_text = event.as_text()
#		var button_name = as_text.split(", ")[0].split(" : ")[1].split("=")[1]
#		text += button_name.to_pascal_case()
		text += as_text
	elif event is InputEventJoypadButton:
#		if Input.is_joy_known(event.device):
#			text+= str(Input.get_joy_button_string(event.button_index))
#		else:
#			text += "Btn. " + str(event.button_index)
		if event.button_index == JOY_BUTTON_INVALID:
			text += "undefined"
		else:
			text += str(InputControl.get_joy_button_string(event.button_index))
	elif event is InputEventJoypadMotion:
		if event.axis == JOY_AXIS_INVALID:
			text += "undefined"
			return text

#		if Input.is_joy_known(event.device):
#			axis = str(Input.get_joy_axis_string(event.axis))
#			text += axis + " "
#		else:
#			text += "Axis: " + str(event.axis) + " "

		var axis := str(InputControl.get_joy_axis_string(event.axis))
		# technically axis value is a float [0,1]
		var axis_value: int = event.axis_value

		var direction := ""
		if axis.ends_with('X'):
			if axis_value > 0:
				direction = 'Right'
			else:
				direction = 'Left'
			text += axis.replace("X", direction)
		elif axis.ends_with('Y'):
			if axis_value > 0:
				direction = 'Down'
			else:
				direction = 'Up'
			text += axis.replace("Y", direction)
		else:
			text += axis + " " + str(axis_value)

	return text
