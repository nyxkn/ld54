extends Node


@export var fullscreen: bool = false : set = set_fullscreen
@export var borderless: bool = false : set = set_borderless
@export var vsync: bool = true : set = set_vsync


## none of this stuff has been tested on godot4

func init() -> void:
	pass
	# apply values we get from config


func set_fullscreen(value: bool) -> void:
	fullscreen = value
	# mode_fullscreen on all platforms is a borderless window screen-size
	# on windows only, there exists a mode_exclusive_fullscreen, which is the real fullscreen
	# using mode_exclusive_fullscreen will default to mode_fullscreen on non-windows platforms
	get_window().mode = Window.MODE_FULLSCREEN if (fullscreen) else Window.MODE_WINDOWED


func set_borderless(value: bool) -> void:
	borderless = value
	get_window().borderless = borderless


func set_vsync(value: bool) -> void:
	vsync = value
	DisplayServer.window_set_vsync_mode(DisplayServer.VSYNC_ENABLED if (vsync) else DisplayServer.VSYNC_DISABLED)
