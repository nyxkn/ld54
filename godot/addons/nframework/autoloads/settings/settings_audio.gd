extends Node


var bus_ids := {}

# we store volumes as [0,1] floats
# godot instead wants a db value for the volume
# NOTE: these are not reflected in the audio tab of the editor. that only displays the default resource
# NOTE: these only affect the volume levels. we're not adding or removing buses
var volumes := {}


# dictionary with { "category1": data }
func export_custom_properties() -> Dictionary:
	Log.d("exporting volumes", volumes)
	return {"volumes": volumes}


func restore_custom_properties(props: Dictionary):
	Log.d("restoring volumes", props["volumes"])
	volumes = props["volumes"]


func init() -> void:
	# AudioServer bus index is an incremental int starting at 0
	# so if bus_count is 3, we have 3 buses with ids 0, 1, 2
	for i in AudioServer.bus_count:
		bus_ids[AudioServer.get_bus_name(i)] = i

	Log.d(["initial buses", bus_ids])

	# if volumes haven't been restored from file, initialize it with default values
	print(volumes)
	if volumes.is_empty():
		print("volumes 1")
		# read volumes from the default_bus_layout resource, which is the one the godot editor loads by default
		for bus in bus_ids:
			# 0.25 is -12db, 0.5 is -6db
			# -6db is probably a good default value
			var volume = db_to_linear(AudioServer.get_bus_volume_db(bus_ids[bus]))
			set_volume(bus, volume)
	else:
		print("volumes 2")
		for bus in volumes:
			set_volume(bus, volumes[bus])

	Log.d(["volumes set to", volumes])


# we use bus name as our id, rather than AudioServer's int id
# volume is [0,1]
func set_volume(bus: String, volume: float) -> void:
	if not bus in bus_ids.keys():
		Log.e(["attempting to set volume of inexistent bus", bus], name)
		return

	Log.d("setting volume", bus, volume)
#	Log.trace()

	volumes[bus] = volume
	AudioServer.set_bus_volume_db(bus_ids[bus], linear_to_db(volume))


# a float value [0,1]
func get_volume(bus: String) -> float:
	if not bus in bus_ids.keys():
		Log.e(["attempting to get volume of inexistent bus", bus], name)
		return float(-1)

	return volumes[bus]

