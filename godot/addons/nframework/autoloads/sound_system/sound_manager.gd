extends Node


const SamplePlayer := preload("sample_player.gd")
var cfg_path = Base.config_path + "sounds.cfg"

#enum Mode { RETRO, PER_SAMPLE }
#@export var mode: Mode = Mode.PER_SAMPLE

var samples := {}
var sample_players := {}

# use the same sample player for all sounds
# this way you get a retro effect of only having a limited amount of sounds at a given time
@onready var retro_player: SamplePlayer = SamplePlayer.new("SFX", "", 3)
# a separate ui player that uses a different bus, where you could have different processing
# e.g. if game sound if being muffled for underwater, ui sound still stays normal
@onready var ui_player: SamplePlayer = SamplePlayer.new("UI", "", 3)


func _ready() -> void:
	add_child(retro_player)
	add_child(ui_player)

func play(sample: String, audio_params := {}) -> void:
	if samples.has(sample):
		sample_players[sample].play(audio_params)
	else:
		Log.e(["sample not loaded:", sample], name)
		# load on the spot? maybe bad idea

func _play_sample(sample: String, sample_player, audio_params := {}) -> void:
	if samples.has(sample):
		sample_player.play(audio_params, samples[sample])

func play_retro(sample):
	_play_sample(sample, retro_player)

func play_ui(sample):
	_play_sample(sample, ui_player)


# play a provided stream that wasn't part of the samples
func play_stream(audiostream: AudioStream):
	pass

# add samples to the collection. we do the loading
func load_samples(resource_paths: Array) -> void:
	for r in resource_paths:
		r = r.trim_suffix(".import")
		# this gets the filename without extension
		var id = r.get_file().get_basename()

		if samples.has(id):
			Log.e(["sample with id", id, "already exists"], name)

		# TODO we might want to do this asynchronously in a thread
		# gametemplate has an example
		samples[id] = load(r)
		Log.d(["loaded sample:", id])

		sample_players[id] = SamplePlayer.new("SFX", id, 6)
		add_child(sample_players[id])

	load_cfg()


func save_cfg() -> void:
	var config = ConfigFile.new()

	for id in SoundManager.sample_players:
		var node = SoundManager.sample_players[id]

		for prop in Utils.get_export_properties(node):
			config.set_value(id, prop.name, node.get(prop.name))

	config.save(cfg_path)


func load_cfg():
	if not FileUtils.file_exists(cfg_path):
		return
	var cfg := FileUtils.read_cfg(cfg_path)

	for sample in cfg:
		var props = cfg[sample]
		for key in props:
			sample_players[sample].set(key, props[key])


func load_samples_directory(folder_path: String) -> void:
	Log.d(["loading samples from directory", folder_path])
	var ext = ".wav.import"
	var files = FileUtils.get_files_in_dir(folder_path, ext)
	if files.is_empty():
		Log.w(["directory", folder_path, "contains no files with extension", ext])
	else:
		load_samples(files)


# add samples to the collection. you handle the loading
# pass in an array of Resource objects
func add_samples(resources: Array) -> void:
	for r in resources:
		r = r as Resource
		var id = r.get_path().get_file().get_basename()
		samples[id] = r


# free samples from memory
# pass in an array of samples ids
func remove_samples(ids: Array) -> void:
	for id in ids:
		samples[id].queue_free()
		samples.erase(id)
