#class_name SamplePlayer
extends Node

## set this to a low value like 2 to simulate retro consoles
## new sound effect will replace the old ones
## otherwise 32 seems like a sane number
## this is a more custom way of achieving the "max polyphony" setting
## which unfortunately cannot deal with changes of pitch/volume per play
var max_players: int


# var players := []
var pool_size: int = 0
var bus: String = ""

var available_players := []
var busy_players := []

var sample_id: String


## playback params
var valid_params: Array

# change of volume in db
@export var gain: float = 0.0

# pitch offset in semitones. can be fractions of semitones too, which correspond to cents
# e.g. 1.52 semitones is 152 cents
@export var pitch_offset: float = 0.0

# maximum deviation of pitch in semitones, applied equally up and down
# can be fractions of semitones too
# this technically affects sample rate, so it also changes time
# 12 would make the higher octave sound half as long, while the lower octave becomes twice as long
# keep this in mind, so maybe avoid large values to avoid huge length differences
@export var pitch_randomization: float = 0.0

# maximum deviation of volume in db. this we apply only downward
# because if we assume samples to be normalized to 0db, upping the gain will create distortion
# so 10 would make us randomize between 0db and -10db
# db notes:
# +3db means double the power (energy). but +10db is what's perceived as twice as loud
# a 10db change is a factor of 2 change in perceived loudness (+10db is double as loud)
# a 6db change is a factor of 2 change in voltage (+6db is double the voltage)
# a 3db change is a factor of 2 change in power ratio (+3db is double the power)
@export var volume_randomization: float = 0.0


func _init(bus: String, sample: String, max_players: int = 3, pool_size: int = 3):
	self.bus = bus
	sample_id = sample

	var players_num = min(pool_size, max_players)
	for i in players_num:
		add_player()

func _ready() -> void:
	var props = Utils.get_export_properties(self)
	for p in props:
		valid_params.append(p.name)


func add_player() -> void:
	var player := AudioStreamPlayer.new()
	add_child(player)

	available_players.push_back(player)
	pool_size += 1

	player.bus = bus
	if sample_id != "":
		player.stream = SoundManager.samples[sample_id]
# 	player.volume_db = linear_to_db(1)
#	player.pitch_scale = 1 + (pool_size - 1) * 0.2
	player.finished.connect(_playing_finished.bind(player))

#	Log.d(["pool size", pool_size])


func _playing_finished(player: AudioStreamPlayer) -> void:
#	Log.d([ "player finished", player ], name)
	busy_players.erase(player)
	available_players.push_back(player)


func get_available_player() -> AudioStreamPlayer:
	var p: AudioStreamPlayer

	if available_players.size() == 0:
		# if < max players, add player
		if pool_size < max_players:
			Log.d("all players busy, adding new")
			add_player()
			p = available_players.pop_front()
		else:
			Log.d("all players busy, recycling")
			p = busy_players.pop_front()
			# calling stop is redundant. the new play() is going to stop the previous sound
			# p.stop()
	else:
		p = available_players.pop_front()

	busy_players.push_back(p)

	return p


func play(params := {}, audio_stream: AudioStream = null) -> void:
	# _params is the final params to use. default to self parameters
	var _params = {}
	for param in valid_params:
		_params[param] = get(param)

	# but if passed into params, override with those
	for param in params:
		if not param in valid_params:
			Log.e("attempting to use invalid param:", param)
		else:
			_params[param] = params[param]

	var p := get_available_player()

	if audio_stream != null:
		p.stream = audio_stream

	p.volume_db = 0.0 + _params.gain

	p.pitch_scale = pow(2, _params.pitch_offset / 12.0)

	if _params.pitch_randomization != 0.0:
		# converting our pitch_randomization value to a pitch multiplier by using 2^x
		var min_pitch = pow(2, (_params.pitch_offset - _params.pitch_randomization) / 12.0)
		var max_pitch = pow(2, (_params.pitch_offset + _params.pitch_randomization) / 12.0)
		p.pitch_scale = randf_range(min_pitch, max_pitch)

	if _params.volume_randomization != 0.0:
		p.volume_db = p.volume_db + randf_range(-_params.volume_randomization, 0.0)

	p.play()
	#return p

func stop():
	for p in busy_players:
		p.stop()

