@tool
extends Node


## PLS DON'T WASTE TIME ON THIS ANYMORE


enum Level {
	# debug - always meant to be disabled in release
	# VERBOSE, # from godot-logger
	# TRACE, # log4j
	DEBUG, # prints used for debugging behaviour or tracking things

	# informational - these stay in release. they provide meaningful information to the user
	INFO, # inform the user/yourself of something
	WARN, # provide a warning
	ERROR, # notify of an error
	# FATAL, # log4j
}

# format of the {TIME} block
# example with all supported placeholders: "YYYY.MM.DD hh.mm.ss"
const time_format = "hh:mm:ss.ms"


var log_hide: bool = false
# set levels to true to hide them
var log_hide_level: Dictionary = {}

# context will get registered for a new instance by calling get_new()
var _context := ""

# you can call this in a few ways
# load the script and register:
# var Log = preload("res://addons/nframework/autoloads/logger.gd").get_new(self)
# register from the singleton (can't use the same name as the autoload for the variable):
# var l = Log.get_new(self)
# register from the singleton but without using the autoload name so we can use the same name
# onready var Log = get_node("/root/Log").get_new(self)
static func get_new(context):
	var Logger = load("res://addons/nframework/autoloads/logger.gd")
	var logger = Logger.new()

	var context_str
	if context is Object:
		context_str = context.get_script().resource_path.get_file().trim_suffix(".gd")
	else:
		context_str = context

	assert(context_str != "", "logger context cannot be empty. provide self or a valid string")

	logger._context = context_str
	logger.name = logger._context
	# remove this line if you're not using autoload
	Log.add_child(logger)

	return logger


func _init():
	d('logger init')

	if OS.is_debug_build():
		# debug build
		if log_hide:
			print('=== log messages will be hidden ===')
	else:
		# release build
		log_hide_level = {
			Log.Level.DEBUG: true,
		}

# logger does not need to be added to the scenetree to function
# so it will work even before being readied
# but you won't be able to access Config values until logger and config are both ready
func _ready() -> void:
	d('logger ready')


## objects can be an array or a single basic type
## get_stack does not work in release builds, only in debug :(
## https://github.com/godotengine/godot-proposals/issues/105
func _print(objects, level: int = Level.DEBUG, objects_only: bool = false):
	if log_hide:
		return
	if log_hide_level.has(level) and log_hide_level[level] == true:
		# don't log if this level is selectively hidden
		return

	var origin_script = ""
	var origin_function = ""

	# in the call stack, 0 is the current function, 1 is one of the shortcuts below, 2 is the origin
	var stack = get_stack()
	if stack:
		var call_origin = stack[2]
		origin_script = call_origin.source.get_file().trim_suffix(".gd")
		origin_function = call_origin.function

	if _context:
		# if we were registered, force the registered context name
		origin_script = _context


	# format objects depending on type
	var objects_str = ""
	if objects is Array:
		for obj in objects:
			objects_str += str(obj) + " "
		objects_str = objects_str.trim_suffix(" ")
	else:
		objects_str = str(objects)


	var final_str: String
	if objects_only:
		final_str = objects_str
	else:
		final_str = _format(level, origin_script, origin_function, objects_str)


	if level == Level.ERROR:
		printerr(final_str)
		push_error(final_str)
	elif level == Level.WARN:
		print(final_str)
		push_warning(final_str)
	else:
		print(final_str)


func trace():
	var stack = Utils.tail(get_stack())
	Utils.pretty_print(stack)


func varargs_to_array(arg1 = null, arg2 = null, arg3 = null, arg4 = null, arg5 = null, arg6 = null, arg7 = null, arg8 = null, arg9 = null):
	var objects = []
	for argument in [arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9]:
		if argument != null:
			if argument is Array:
				objects.append_array(argument)
			else:
				objects.append(argument)

	return objects


# https://github.com/godotengine/godot-proposals/issues/1034#issuecomment-642460586
#func d(objects):
func d(arg1 = null, arg2 = null, arg3 = null, arg4 = null, arg5 = null, arg6 = null, arg7 = null, arg8 = null, arg9 = null):
	var objects = varargs_to_array(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)
	self._print(objects, Level.DEBUG)


func i(arg1 = null, arg2 = null, arg3 = null, arg4 = null, arg5 = null, arg6 = null, arg7 = null, arg8 = null, arg9 = null):
	var objects = varargs_to_array(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)
	self._print(objects, Level.INFO)


func w(arg1 = null, arg2 = null, arg3 = null, arg4 = null, arg5 = null, arg6 = null, arg7 = null, arg8 = null, arg9 = null):
	var objects = varargs_to_array(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)
	self._print(objects, Level.WARN)


func e(arg1 = null, arg2 = null, arg3 = null, arg4 = null, arg5 = null, arg6 = null, arg7 = null, arg8 = null, arg9 = null):
	var objects = varargs_to_array(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)
	self._print(objects, Level.ERROR)


func error(error_code: int, objects):
	if error_code > 0:
		var error_str = ERROR_MESSAGES[error_code].rstrip(".")
#		var error_message = str("(", error_code, " - ", error_str, ")")
#		var error_message = str("[ ", error_str, " (", error_code, ") ]")
		var error_message = str("--- ", error_str, " (", error_code, ")")
		objects.append(error_message)

	self._print(objects, Level.ERROR)


# generic print. can be used instead of print(). no timestamp and formatting
func p(arg1 = null, arg2 = null, arg3 = null, arg4 = null, arg5 = null, arg6 = null, arg7 = null, arg8 = null, arg9 = null):
	var objects = varargs_to_array(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)
	self._print(objects, Level.DEBUG, true)


# print with time and formatting
func pt(arg1 = null, arg2 = null, arg3 = null, arg4 = null, arg5 = null, arg6 = null, arg7 = null, arg8 = null, arg9 = null):
	var objects = varargs_to_array(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)
	self._print(objects, Level.DEBUG)


# Some of this code is taken from godot-logger
# https://github.com/KOBUGE-Games/godot-logger/blob/master/logger.gd

# in godot 4 we can color the output
# https://github.com/godotengine/godot/pull/60675
func _format(level, script, function, message):
	var output = "({TICKS}) [{LVL}] [{SRC}] {MSG} ({FUN})"

	output = output.replace("{LVL}", "%5s" % Level.keys()[level].to_lower())
	if script:
		output = output.replace("[{SRC}]", "%-16s" % str("[", script, "]"))
	else:
		output = output.replace("[{SRC}]", "")
	if function:
		output = output.replace("{FUN}", "%s" % function)
	else:
		output = output.replace("({FUN})", "")
	output = output.replace("{MSG}", message)
	output = output.replace("{TIME}", _get_formatted_datetime(time_format))
	output = output.replace("{TICKS}", _get_formatted_ticks())

	output = output.replace(" [] ", " ")

	return output


# Format the fields:
# * YYYY = Year
# * MM = Month
# * DD = Day
# * hh = Hour
# * mm = Minutes
# * ss = Seconds
# * ms = Milliseconds
# e.g. time_string can look like "hh:mm:ss.ms"
func _get_formatted_datetime(time_string):
#	var datetime = Time.get_datetime_dict_from_system()
#	var msec_epoch := OS.get_system_time_msecs()
	var sec_epoch := Time.get_unix_time_from_system()
	var datetime = Time.get_datetime_dict_from_unix_time(sec_epoch)
	var msec = int(str(sec_epoch).split(".")[1])

	var result = time_string
	result = result.replace("YYYY", "%04d" % [datetime.year])
	result = result.replace("MM", "%02d" % [datetime.month])
	result = result.replace("DD", "%02d" % [datetime.day])
	result = result.replace("hh", "%02d" % [datetime.hour])
	result = result.replace("mm", "%02d" % [datetime.minute])
	result = result.replace("ss", "%02d" % [datetime.second])
	result = result.replace("ms", "%03d" % msec)

	return result


func _get_formatted_ticks():
	var ticks = Time.get_ticks_usec()
	var seconds = ticks * pow(10, -6)
	return TimeUtils.format_seconds(seconds, TimeUtils.SecondsResolution.US)


# Maps Error code to strings.
# This might eventually be supported out of the box in Godot,
# so we'll be able to drop this.
const ERROR_MESSAGES = {
	OK: "OK.",
	FAILED: "Generic error.",
	ERR_UNAVAILABLE: "Unavailable error.",
	ERR_UNCONFIGURED: "Unconfigured error.",
	ERR_UNAUTHORIZED: "Unauthorized error.",
	ERR_PARAMETER_RANGE_ERROR: "Parameter range error.",
	ERR_OUT_OF_MEMORY: "Out of memory (OOM) error.",
	ERR_FILE_NOT_FOUND: "File: Not found error.",
	ERR_FILE_BAD_DRIVE: "File: Bad drive error.",
	ERR_FILE_BAD_PATH: "File: Bad path error.",
	ERR_FILE_NO_PERMISSION: "File: No permission error.",
	ERR_FILE_ALREADY_IN_USE: "File: Already in use error.",
	ERR_FILE_CANT_OPEN: "File: Can't open error.",
	ERR_FILE_CANT_WRITE: "File: Can't write error.",
	ERR_FILE_CANT_READ: "File: Can't read error.",
	ERR_FILE_UNRECOGNIZED: "File: Unrecognized error.",
	ERR_FILE_CORRUPT: "File: Corrupt error.",
	ERR_FILE_MISSING_DEPENDENCIES: "File: Missing dependencies error.",
	ERR_FILE_EOF: "File: End of file (EOF) error.",
	ERR_CANT_OPEN: "Can't open error.",
	ERR_CANT_CREATE: "Can't create error.",
	ERR_QUERY_FAILED: "Query failed error.",
	ERR_ALREADY_IN_USE: "Already in use error.",
	ERR_LOCKED: "Locked error.",
	ERR_TIMEOUT: "Timeout error.",
	ERR_CANT_CONNECT: "Can't connect error.",
	ERR_CANT_RESOLVE: "Can't resolve error.",
	ERR_CONNECTION_ERROR: "Connection error.",
	ERR_CANT_ACQUIRE_RESOURCE: "Can't acquire resource error.",
	ERR_CANT_FORK: "Can't fork process error.",
	ERR_INVALID_DATA: "Invalid data error.",
	ERR_INVALID_PARAMETER: "Invalid parameter error.",
	ERR_ALREADY_EXISTS: "Already exists error.",
	ERR_DOES_NOT_EXIST: "Does not exist error.",
	ERR_DATABASE_CANT_READ: "Database: Read error.",
	ERR_DATABASE_CANT_WRITE: "Database: Write error.",
	ERR_COMPILATION_FAILED: "Compilation failed error.",
	ERR_METHOD_NOT_FOUND: "Method not found error.",
	ERR_LINK_FAILED: "Linking failed error.",
	ERR_SCRIPT_FAILED: "Script failed error.",
	ERR_CYCLIC_LINK: "Cycling link (import cycle) error.",
	ERR_INVALID_DECLARATION: "Invalid declaration error.",
	ERR_DUPLICATE_SYMBOL: "Duplicate symbol error.",
	ERR_PARSE_ERROR: "Parse error.",
	ERR_BUSY: "Busy error.",
	ERR_SKIP: "Skip error.",
	ERR_HELP: "Help error.",
	ERR_BUG: "Bug error.",
	ERR_PRINTER_ON_FIRE: "Printer on fire error.",
}
