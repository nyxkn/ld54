@tool
extends Node

# this is the place where we configure how nframework behaves and
# where we store configuration type information that is either:
# - used by a lot of different modules
# - needs to be stored/restored across restarts

# these things should be saved and propagated to the builds
# so they cannot be saved in user://, but rather need to be saved in res:// and exported

# these are settings that are NOT available to the user and should never be changed on runtime
# so we should only save to disk from a debug build, never in release

# everything that you want saved/restored needs to be an export(type)


@export var reload: bool = false : set = _reload
func _reload(_value) -> void:
	Log.d("config reloading")
	reload = false
	resetup_on_reload()


################################################################
# LOGGING

# overrides of Logger variables. needed?
@export var log_hide: bool = false
@export var log_hide_level: Dictionary = {}


################################################################
# RESOURCES and PATHS

@export var scenes: Dictionary = {}

var default_scenes := {
	intro = "res://addons/nframework/scenes/intro.tscn",
	ending = "res://addons/nframework/scenes/ending.tscn",
	loading = "res://addons/nframework/scenes/loading.tscn",
	main_menu = "res://addons/nframework/scenes/main_menu.tscn",
	settings_menu = "res://addons/nframework/scenes/settings_menu/settings_menu.tscn",
	leaderboard = "res://addons/nframework/scenes/leaderboard/leaderboard.tscn",
	help = "res://addons/nframework/scenes/help.tscn",

	# this should point to your game actual entry point
	game = "res://game/game.tscn"
	}


################################################################
# OTHER

@export var low_res: bool = true

enum MenuControl {
	KEYBOARD = 1,
	MOUSE = 2,
	GAMEPAD = 4,
	}

# enable or disable menu keyboard controls
@export var menu_control: MenuControl = (
	MenuControl.KEYBOARD | MenuControl.MOUSE
	)

#var PAUSE_ENABLED: bool = true

# Ludum Dare mode
# - disable loading of pre-made assets
# starting with framework/library type of base-code is allowed
# a "logo/intro screen" is allowed, but pre-made assets in menu probably isn't
# as long as the assets are so minimal to pass for a generic theme, we can probably ignore it
# sound effects are fine if they are made with a sound generator
#export var ludum_dare: bool: bool = false

# enable silentwolf
@export var silentwolf: bool = false

@export var simple_settings: bool = false

@export var threaded_loading: bool = false

@export var use_intro: bool = false


################################################################
# debug settings (get disabled on release)
# add them to the disable_debug_settings function below

# jump straight into the game, skipping the menu or intro screens
@export var d_straight_to_game: bool = false

# enable test_bench
@export var d_test_bench: bool = false

@export var d_spoof_colemak: bool = false

@export var d_custom_launch: String = ""


func disable_debug_settings():
	d_straight_to_game = false
	d_test_bench = false
	d_spoof_colemak = false
	d_custom_launch = ""


################################################################
# RUNTIME
# these are not settings we want to save
# they are read/initialized during runtime

var threads_enabled = false

# holds the selected theme
var theme: String

# holds the editor interface. can't static type or it causes issues on export
# the instance will be lost everytime this script is reloaded (e.g. on save)
# you have to reload the whole nframework plugin to get a valid instance again
var editor_interface

var audio_buses: Dictionary

var nframework_cfg: Dictionary


################################################################
# LOGIC

var cfg_path = Base.config_path + "config.cfg"


func _ready() -> void:
	Log.d('config ready')
	resetup_on_reload()


# code to run again after the script reloads (e.g. after save)
func resetup_on_reload() -> void:
	pass


func _init():
	Log.d('config init')

	load_cfg()

	Log.log_hide = log_hide
	Log.log_hide_level = log_hide_level

	if not Base.debug_build:
		# disable debug settings on release build
		disable_debug_settings()

	if Base.platform == Base.Platform.WEB:
		threads_enabled = false
		threaded_loading = false
	else:
		threads_enabled = true

	if scenes.is_empty():
		scenes = default_scenes.duplicate()

	if low_res:
		# testing menus at 320x180
		theme = "res://addons/nframework/assets/themes/lowres_tex.tres"
	else:
		# ideally theme_hires would just be an override on top of lowres,
		# without redefining everything
		# should be possible in godot 4
		theme = "res://addons/nframework/assets/themes/theme_hires_tex.tres"

	# saving here so that we create the file the first time around
	# at this point it should contain just the defaults
	if Base.debug_build:
		save_cfg()


# this needs to be saved to config/ or any other folder that explicitly gets added to the build
func save_cfg() -> void:
	var config = ConfigFile.new()

	for prop in Utils.get_export_properties(self):
		config.set_value("config", prop.name, get(prop.name))

	# should we even bother with encryption?
	# the only way to obfuscate the exported pck is by encrypting the pck itself
	# at which point it does not matter if the config file is encrypted or not
#	config.save_encrypted_pass(cfg_path, OS.get_unique_id())
#	config.save_encrypted_pass(cfg_path, CFG_SAVE_PASS)
	config.save(cfg_path)

	# saving the file unencrypted when running the debug build for our convenience
	# this should not be shipped in export builds
#	if debug_build:
#		config.save(user_path + "config_unencrypted.cfg")

	# cannot use Log just yet, as config loads before logger
#	P.log_or_print("config saved")
	Log.d("config file saved")


func load_cfg() -> void:
	if not FileUtils.file_exists(cfg_path):
		return
#	var cfg = FileUtils.read_cfg(cfg_path, OS.get_unique_id())
#	var cfg = FileUtils.read_cfg(cfg_path, CFG_SAVE_PASS)
	var cfg = FileUtils.read_cfg(cfg_path)

	if cfg:
		var props = cfg["config"]
		for key in props:
			set(key, props[key])

	Log.d("config file loaded")
