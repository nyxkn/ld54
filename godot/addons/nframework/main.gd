extends Node


signal loading_finished


# this is for threaded loading. rename to threaded loading mode?
enum LoadingMode { WAIT_BLANK, WAIT_SPINNER, NO_WAIT }
# if we use no_wait, when changing scene we cannot free main or threads
# while we wait for loading completion
var loading_mode: int = LoadingMode.NO_WAIT


# user defined functions
var startup = preload("res://startup.gd").new()


func _init():
	pass


func _enter_tree() -> void:
	pass
#	F.sleep(5.0)


# this is the true entry point of the whole application
func _ready() -> void:
	loading_finished.connect(startup._loading_finished)

	# adding startup as a child so that we have access to the scenetree from there
	add_child(startup)

	# for peace of mind, actually start doing all your initialization on the next frame
	# so that everything is properly initialized by then
	# otherwise root doesn't seem to be fully initialized
	call_deferred("main")


# our main function where we initialize everything and change to a different scene
func main() -> void:
	assert(get_tree(), "unexpectedly, we can't access the scenetree just yet")
	startup.config_setup()

	# at this point, godot has fully initialized and all autoloads have been loaded
	Log.i(["startup took", F.ticks(), "s"])

	# debug build or running from editor
	if Base.debug_build:
		pass

	# exported release build
	if not Base.debug_build:
		# custom config settings
		Config.log_hide = true
		Config.log_hide_level = {
			Log.Level.DEBUG: true,
		}
		Config.d_straight_to_game = false

	if Config.silentwolf:
		# load at runtime so that it won't complain if silentwolf is missing
		var SilentWolfInit := load("res://addons/nframework/helpers/silentwolf_init.gd")
		get_tree().root.add_child(SilentWolfInit.new())

	var loader
#	var load_function = funcref(startup, "load_things")
#	var load_function = Callable(startup, "load_things")
	var load_function = startup.load_things
	if Config.threaded_loading:
		loader = ThreadedLoader.new(load_function)
		# only works if root has fully loaded. if you call this from _ready it won't work
#		get_tree().root.add_child(loader)
		# adding above this node instead of in the last place like the line above does
		# just to make sure it's not confused for the current_scene
#		get_tree().root.add_sibling(F, loader)
		get_tree().root.add_sibling(loader)
		# just playing it safe after adding the child
		await get_tree().process_frame
	else:
		_unthreaded_load(load_function)

	if Config.d_straight_to_game or Config.d_custom_launch:
		# wait for loading to finish
		if Config.threaded_loading:
			await loader.loading_finished
		if Config.d_custom_launch:
			F.change_scene_soft(Config.d_custom_launch)
			Config.d_custom_launch = ""
			Config.save_cfg()
		else:
			F.change_scene_soft(Config.scenes.game)
	else:
		# instead of waiting here you can also wait in the menu or intro screen
		# waiting in the main menu works well if you have to spend at least a few seconds menuing
		# loading screen with progress spinner is a safe default
		# or if you have an intro screen you can load silently while the intro is playing

		if loading_mode == LoadingMode.WAIT_SPINNER:
			# add loading screen if the load is long
			# otherwise if it would show too briefly then it's best not to show anything at all
			add_child(load(Config.scenes.loading).instantiate())

		if loading_mode == LoadingMode.WAIT_BLANK || loading_mode == LoadingMode.WAIT_SPINNER:
			if Config.threaded_loading:
				await loader.loading_finished

		if loading_mode == LoadingMode.NO_WAIT:
			# simply skip ahead
			pass

		# this also sends a signal
		F.setup_complete = true

		if Config.use_intro:
			F.change_scene_soft(Config.scenes.intro)
		else:
			F.change_scene_soft(Config.scenes.main_menu)


################################################################
# LOADERS

func _unthreaded_load(load_function: Callable):
	var start_time = F.ticks()
	Log.d("unthreaded loading started")

#	startup.load_things()
	load_function.call()

	Log.d("unthreaded loading finished")
	Log.i(["unthreaded loading took", F.ticks() - start_time, "s"])

	loading_finished.emit()


class ThreadedLoader:
	extends Node

	signal loading_finished

	var finished := false
	var thread := Thread.new()
	var load_function: Callable

	func _init(load_function: Callable):
		name = "ThreadedLoader"
		thread.start(start_loading.bind(""))
		self.load_function = load_function


	# even if loading is very brief, this is more flexible than preloading
	# here we only load data that isn't immediately needed
	# everything else like settings and config should be loaded earlier in the main thread
	func start_loading(userdata) -> bool:
		var start_time = F.ticks()
		Log.d("threaded loading started")

		load_function.call()

		Log.d("threaded loading finished")
		Log.i(["threaded loading took", F.ticks() - start_time, "s"])
		finished = true
		# even in g4, this seems like the only way to defer a signal
		call_deferred("emit_signal", "loading_finished")

		call_deferred("thread_end")
		return true


	func thread_end():
		# join completed thread so that we can dispose of it cleanly without debugger complaining
		var ret = thread.wait_to_finish()
		queue_free()
