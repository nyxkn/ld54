extends TextureButton


# hover texture will now be drawn on top of the button
# similar to how focus texture is usually drawn
# use our custom hover texture field instead of the standard texture one


@export var hover_texture_additive: Texture2D


func _ready() -> void:
	$Hover.texture = hover_texture_additive
	$Hover.hide()


func _on_mouse_entered() -> void:
	$Hover.show()


func _on_mouse_exited() -> void:
	$Hover.hide()
