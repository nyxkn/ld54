@tool
extends HBoxContainer


signal value_changed(value: float)

@export var min_value: float = 0 : set = set_min_value
@export var max_value: float = 100 : set = set_max_value
@export var step: float = 1 : set = set_step
@export var exponential: bool = false : set = set_exponential
@export var default_value: float = 0
@export var clean_spinbox: bool = false

@export_flags("Slider", "SpinBox", "ValueLabel") var components = 1|2

# % names are problematic if you end up making this local
#@onready var h_slider: HSlider = $HSlider
#@onready var spin_box: SpinBox = $SpinBox

var h_slider: HSlider
var spin_box: SpinBox


# we're using the private variable to be able to set the value without triggering set_value
# ideally we'd to without that, and set_value should take args to determine behaviour
# so e.g. we could pass no_signal to set_value to avoid triggering signal
# that would be more clear than using a double variable
var _value: float
var value: float :
	get: return _value
	set(v): set_value(v)


# this function can be overridden in case you want to have your controls elsewhere in the scene
# a bit overkill tbh
func find_controls():
	h_slider = $HSlider
	spin_box = $SpinBox


func _ready():
	find_controls()

	# set without signal
	set_value_no_signal(default_value)

	$Label.text = name

	h_slider.visible = components & 1
	spin_box.visible = components & 2
	$Value.visible = components & 4

	if clean_spinbox:
		spin_box.add_theme_icon_override("updown", CompressedTexture2D.new())
	else:
		spin_box.remove_theme_icon_override("updown")


func set_min_value(v) -> void:
	min_value = v

	if not is_node_ready():
		await ready

	h_slider.min_value = min_value
	spin_box.min_value = min_value


func set_max_value(v) -> void:
	max_value = v

	if not is_node_ready():
		await ready

	h_slider.max_value = max_value
	spin_box.max_value = max_value


func set_step(v) -> void:
	step = v

	if not is_node_ready():
		await ready

	h_slider.step = step
	spin_box.step = step


func set_exponential(v) -> void:
	exponential = v

	if not is_node_ready():
		await ready

	h_slider.exp_edit = exponential
	spin_box.exp_edit = exponential


func set_value(new_value: float):
	set_value_no_signal(new_value)

	# it's safer to call this when ready. but is there any need for it to happen before ready?
	value_changed.emit(new_value)


func set_value_no_signal(new_value: float):
	_value = new_value

	if not is_node_ready():
		await ready

	$Value.text = str(new_value)
	spin_box.set_value_no_signal(new_value)
	h_slider.set_value_no_signal(new_value)


func _on_h_slider_value_changed(new_value: float) -> void:
	value = new_value


func _on_spin_box_value_changed(new_value: float) -> void:
	value = new_value
