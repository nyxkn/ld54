extends Button


func _ready() -> void:
	$Popup.hide()


func _process(delta: float) -> void:
	pass



func _on_pressed() -> void:
	$Popup.show()


func _on_close_pressed() -> void:
	$Popup.hide()
