extends Control

# https://gist.github.com/EricEzaM/ce60c60cb8e980b3150892b93ac14580

@export var text: String = "Big news today! Godot is announced as best engine ever!"
@export var scroll_speed: float = 60

@onready var label: Label = $Label


func _ready() -> void:
	label.text = text


func _draw():
	# This gives this CanvasItem a "clipping mask" that only allows children to display within its rect
	RenderingServer.canvas_item_set_clip(get_canvas_item(), true)


func _process(delta):
	label.position.x -= scroll_speed * delta
	if label.position.x < -label.size.x:
		label.position.x = get_rect().size.x
