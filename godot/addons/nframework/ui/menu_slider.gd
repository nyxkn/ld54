@tool
extends HBoxContainer


@export var min_value: float = 0
@export var max_value: float = 100
@export var step: float = 1


func _ready():
	if Base.in_editor:
		$Label.text = name

	$HSlider.min_value = min_value
	$HSlider.max_value = max_value
	$HSlider.step = step

	$Label.custom_minimum_size.x = int(min(64, Utils.get_screen_size().x / 10))
