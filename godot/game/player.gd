extends CharacterBody2D

## movement 8-way

# pixels per second
@export var speed: int = 400
@export var roll_speed: int = 800
var health: int = 0:
	set(v):
		health = v
		G.game.set_health(health)

var rolling_anim = false
var roll_start
var roll_direction
var is_blocking = false
var is_dead = false
var block_cooldown := 0.0
var resume_blocking_on_cooldown = false
var attack_cooldown := 0.0
var can_control := false

@onready var sprite: AnimatedSprite2D = $AnimatedSprite2D
@onready var aniplayer: AnimationPlayer = $AnimationPlayer


func read_input() -> Vector2:
	# this also considers analog input. you might or might not want this
	# if you set the deadzone to 1.0, left stick will only work when moved all the way

	var left_key = SettingsControls.controls["keymouse"]["left"][0].input_event.physical_keycode
	var right_key = SettingsControls.controls["keymouse"]["right"][0].input_event.physical_keycode
	var up_key = SettingsControls.controls["keymouse"]["up"][0].input_event.physical_keycode
	var down_key = SettingsControls.controls["keymouse"]["down"][0].input_event.physical_keycode

	# clear input vector so we can read the new inputs
	var input := Vector2.ZERO

	# this is the old manual way of doing things. get_vector replaces this
	# we sum left and right together, and up and down together
	if Input.is_physical_key_pressed(left_key):
		input.x += -1
	if Input.is_physical_key_pressed(right_key):
		input.x += 1
	if Input.is_physical_key_pressed(down_key):
		input.y += 1
	if Input.is_physical_key_pressed(up_key):
		input.y += -1

	return input

	#var input = Input.get_vector("left", "right", "up", "down", -1.0)
	#return input



func _ready() -> void:
	sprite.play("idle")
	%Block.hide()

	#print(SettingsControls.controls["keymouse"]["left"][0].input_event.physical_keycode)

# called on game start
func setup():
	can_control = true
	health = 3

var knockback_speed
var knockback_duration := 0.1
var in_knockback = false
var knockback_start = 0
var knockback_dir

func knockback(enemy):
	in_knockback = true
	knockback_start = F.time()
	knockback_dir = (enemy.global_position - global_position).normalized()
	knockback_speed = enemy.attack_knockback_power


func is_hit(enemy):
	if is_blocking:
		SoundManager.play("shield-hit")
		G.shake.add_trauma(0.5)
		knockback(enemy)
		is_blocking = false
		block_cooldown = 0.5
	else:
		health -= 1
		SoundManager.play("hurt2")
		G.shake.add_trauma(1.0)
		if health <= 0:
			G.game.player_died()
			is_dead = true
			death_animation()


func death_animation():
	var t = create_tween().set_parallel(true)
	t.tween_property(self, "scale", Vector2.ZERO, 0.5)
	t.tween_property(self, "rotation", -TAU, 0.5)


#func roll():
	##sprite.stop()
	#sprite.play("roll")
	#var mouse_look = get_global_mouse_position() - global_position
	#roll_direction = mouse_look.normalized()
#
	##if roll_direction.x < 0:
		##sprite.flip_h = true
	##else:
		##sprite.flip_h = false
#
	#rolling = true
	#rolling_anim = true
	#await F.wait(0.2)
	#rolling = false
#
	#await sprite.animation_finished
	#rolling_anim = false


func set_anim(animation):
	if sprite.animation == animation:
		return

	if rolling_anim:
		return

	sprite.play(animation)

# we do our movement code in _physics_process
# because our character needs to collide with other physics bodies
func _physics_process(delta):
	if is_dead:
		return

	if block_cooldown > 0:
		block_cooldown -= delta
		#$Node2D/TextureProgressBar.value = block_cooldown / 0.5
	else:
		if resume_blocking_on_cooldown == true and not is_blocking:
			start_block()
		#if not is_blocking and Input.is_action_pressed("block"):
			#start_block()

	if attack_cooldown > 0:
		attack_cooldown -= delta

	# look_at aligns the +x axis to the target
	var diff = get_global_mouse_position() - global_position
	if diff.x < 0:
		sprite.flip_h = true
		#%Gun.flip_v = true
		#%Gun.offset.y = 1
		#%Melee.flip_h = true
		#%Melee.position.x = -3
		%MeleePivot.scale.y = -1
		%Block.scale.x = -1
	else:
		sprite.flip_h = false
		#%Gun.flip_v = false
		#%Gun.offset.y = -1
		#%Melee.flip_h = false
		#%Melee.position.x = 3
		%MeleePivot.scale.y = 1
		%Block.scale.x = 1


	#if Input.is_action_pressed("block") and block_enable:
		#if not is_blocking:
			#SoundManager.play("shield")
		#if block_break:
			#is_blocking = false
		#else:
			#is_blocking = true
	#else:
		#is_blocking = false

	if is_blocking:
#func block():
		%Block.show()
		%Melee.hide()
	else:
		%Block.hide()
		%Melee.show()


	#%Gun.look_at(get_global_mouse_position())
	%MeleePivot.look_at(get_global_mouse_position())
	%MouseLookRaycast.look_at(get_global_mouse_position())

	if can_control:
		var input := read_input().normalized()
		# normalize so diagonal directions move at the same speed as cardinal directions
		velocity = input * speed
		if velocity.length_squared() > 0.1:
			set_anim("walk")
		else:
			set_anim("idle")

	if in_knockback:
		if F.time() > knockback_start + knockback_duration:
			velocity = Vector2.ZERO
			in_knockback = false
		else:
			velocity = -knockback_dir * knockback_speed

	if not is_blocking:
		move_and_slide()

func shoot():
	#print("bang")
	%Gun/HitscanWeapon.shoot()

func melee():
	attack_cooldown = 0.3
	aniplayer.play("melee")
	SoundManager.play("swing")
	var raycast := %MouseLookRaycast
	if raycast.is_colliding():
		var coll = raycast.get_collider()
		if coll.is_in_group("enemy"):
			coll.is_hit()

func start_block():
	is_blocking = true
	SoundManager.play("shield")

func _unhandled_input(event: InputEvent) -> void:
	if not can_control:
		return
	if event.is_action_pressed("attack") and not is_blocking and attack_cooldown <= 0:
		#print("clicked")
		melee()
	elif event.is_action_pressed("block"):
		resume_blocking_on_cooldown = true
		if block_cooldown <= 0:
			start_block()
	elif event.is_action_released("block"):
		resume_blocking_on_cooldown = false
		is_blocking = false
		#block_enable = true
	#elif event.is_action_pressed("roll") and not rolling:
		#roll()
