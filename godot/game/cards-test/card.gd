extends TextureRect


func _ready() -> void:
	pivot_offset = size / 2


func _process(delta: float) -> void:
	pass



func _on_mouse_entered() -> void:
	scale = Vector2.ONE * 2

	z_index = 99


func _on_mouse_exited() -> void:
	scale = Vector2.ONE
	z_index = 0
