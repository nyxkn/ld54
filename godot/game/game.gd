extends Node2D


# this is your main game loop
# if you are changing levels or entering menus, they should probably be added and removed from here
# rather than changing scene
# you only want to change scene to non-game things like main menu and end screen
# i.e. when you can afford to unload everything game-related


var score: float = 0.0

var cheats_enabled := false
var cheats := {}

var stats := {
	"play_time": 0.0,
}

var state: State
class State:
	var score: float = 0.0


var scene_change_data
var stage = 1
var player
var changing_stage = false
var stage_node


@onready var game_over_overlay: CanvasLayer = %GameOverOverlay


func _enter_tree() -> void:
	pass


func _ready() -> void:
	Pause.allow_from(self)
	# to set a background color without adding colorrects
	RenderingServer.set_default_clear_color(Color("272744").darkened(0.7))

	# it's good practice to start logic in its own function, so that it can be called on restart
	# _ready should only be for node initialization things
	# it's also good practice to let logic run decoupled from _ready
	# sometimes other functions wait for _ready signal to know if loading is complete
	# but if you start the logic before running _ready, it might throw things off
	call_deferred("game_setup")

	game_over_overlay.hide()

	#SettingsControls.print_inputmap_controls()


func _exit_tree() -> void:
	pass


#func _input(event: InputEvent) -> void:
#	pass
##	Utils.log_event(event, "_input", name)


#func _gui_input(event: InputEvent) -> void:
#	pass
##	Utils.log_event(event, "_gui_input", name)

func set_health(num):
	var hearts = %Health.get_children()
	for h in hearts:
		h.hide()
	for i in num:
		hearts[i].show()

func _unhandled_input(event: InputEvent) -> void:
	pass
	#Utils.log_event(event, "_unhandled_input", name)

	# don't use frst, wasd
	if Base.debug_build and event is InputEventKey and event.is_pressed():
		if event.keycode == KEY_K:
			F.change_scene(Config.scenes.game, { 'stage': 3 })
		if event.keycode == KEY_E:
			to_end_screen()
		#if event.keycode == KEY_D:
			#for i in player.health:
				#player.is_hit(player)
		if event.keycode == KEY_H: # haxxor!
			cheats_enabled = !cheats_enabled
#		if event.keycode == KEY_: # moar hax!
#			add_score(100)

	if event is InputEventKey and event.is_pressed():
		if event.keycode == KEY_SPACE and game_over_overlay.visible:
			restart_level()


func restart_level():
	F.change_scene(Config.scenes.game, { 'stage': stage })

#func _unhandled_key_input(event: InputEvent) -> void:
#	pass
##	Utils.log_event(event, "_unhandled_key_input", name)


func _process(delta: float) -> void:
	if stage_clear and %StageWalkOut.overlaps_body(player) and not changing_stage:
		changing_stage = true
		#await F.wait(1)
		#var total_stages = FileUtils.get_files_in_dir("res://game/stages/").size() - 1
		var total_stages = 5
		if stage+1 > total_stages:
			to_end_screen()
		else:
			F.change_scene(Config.scenes.game, { 'stage': stage+1 })


func player_died():
	game_over_overlay.show()

# always assume you start from a freshly initialized scene
# absolutely avoid restarting by manually deconstructing and reconstructing
# just queue_free and re instance
func game_setup() -> void:
	state = State.new()
	if scene_change_data:
		stage = scene_change_data.stage

	var stage_scene
	if stage == -1:
		stage_scene = load("res://game/stages/stage-test.tscn").instantiate()
	else:
		stage_scene = load("res://game/stages/stage" + str(stage) + ".tscn").instantiate()
	%StageSlot.add_child(stage_scene)
	stage_node = stage_scene

	G.gamestate = state
	G.game = self
	G.shake = %Shake
	G.enemy_attackers_num = 0
	G.enemy_last_attack = 0
	player = %Player
	player.setup()
	game_start()


func game_start() -> void:


	#if Config.d_straight_to_game:
		#G.music_player.play_and_switch("Song", "Main")
	#else:
	G.music_player.goto_section("Main", NDef.When.LOOP)


	# play music
	# enable controls

#func check_stage_clear_delayed():
	#await F.wait(1)
	#check_stage_clear()

func enemy_died():
	await F.wait(0.1)
	stage_node.check_wave_clear()

#func check_stage_clear():
	#var enemies = get_tree().get_nodes_in_group("enemy")
	#if enemies.size() == 0:
		#stage_cleared()

var stage_clear := false
func stage_cleared():
	if not stage_clear:
		stage_clear = true
		%StageArrow.blink()


func to_end_screen():
	G.playtime_stopwatch.stop()
#	Score.new_score(score)
	Score.new_score(score, stats)

	stats.play_time = G.playtime_stopwatch.time

	F.change_scene(Config.scenes.ending, { 'stats': stats })
	#F.change_scene(Config.scenes.leaderboard)


func add_score(change):
	score += change

	updade_ui()

	# basic score animation
#	var s = %Score
#	var label = s.get_node("Value")
#   when you scale, you have to adjust pivot offset first
#	label.pivot_offset = label.size / 2.0
#	var t = create_tween().set_trans(Tween.TRANS_SINE)
#	t.tween_property(label, "scale", Vector2(1.8, 1.8), 0.1)
#	t.tween_property(label, "scale", Vector2.ONE , 0.1)


func pay(amount):
	add_score(-amount)


func updade_ui():
	%Score.value = score

