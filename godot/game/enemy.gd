extends CharacterBody2D


enum State {
	IDLE,
	ATTACK,
	CHASE,
	HOVER,
	HIT,
	APPROACH,
	RETREAT,
	STUN,
	WANDER,
}

enum Type {
	RAMMER,
	SURROUNDER,
}

var state_start_time := 0.0
var state_start_position := Vector2.ZERO
var state_time := 0.0
var state: State = State.HOVER:
	set = set_state

var type: Type = Type.SURROUNDER

var player: Node2D = null:
	get: return G.game.player

@onready var anim: AnimationPlayer = %AnimationPlayer
@onready var sprite: AnimatedSprite2D = %AnimatedSprite2D
@onready var raycast: RayCast2D = %RayCast2D
@onready var circle_angle = F.rng.randf() * TAU
var touching_distance = 5 * 4 * 2 + 4# 5px radius, x4 scale, x2 bodies, + touching zone in px
var pause := 0.0

var speed := 200
var attack_speed := 200
var retreat_speed := 400
var hover_distance := 200
var wiggle_speed := 10
var attack_direction
var knockback_speed := 800
var knockback_duration := 0.05
var distance_keeping_speed := 10
var distance_keeping_distance := 60
var health := 3
var vulnerable := false
var attack_knockback_power := 300
var attack_time := 1
var max_simultaneous_attacks := 1
var player_too_close_timer := 0.0
var attack_after_hover_randf := 0.0
var last_attack_time := 0.0
#var lunging := false

func _ready() -> void:
	set_physics_process(false)
	state = State.APPROACH
	anim.play("RESET")
	#print(touching_distance)
	# this is retarded. why would i have to do this. seriously what the fuck godot
	# okay actually this makes the shader compile again and causes stuttering
	# the solution was to make the material resource "local to scene"
	#sprite.material = sprite.material.duplicate(true)


func activate():
	set_physics_process(true)
	if G.game.stage >= 3:
		max_simultaneous_attacks = 2

func set_state(to_state: State):
	var from_state = state
	if to_state == from_state:
		Log.w("trying to change state to itself but we don't allow it. bug?", to_state)
		return
	Log.d("changing state to", State.keys()[to_state])
	state_start_time = F.time()
	#state_time = 0.0
	state_start_position = global_position
	state = to_state

	if from_state == State.ATTACK:
		G.enemy_attackers_num -= 1

	if to_state == State.HOVER:
		attack_after_hover_randf = F.rng.randf()

	#if to_state == State.ATTACK:
		#lunging = false


func _physics_process(delta: float) -> void:
	state_time = F.time() - state_start_time
	#F.process_print(self, state_time, 0.2)
	if pause > 0:
		pause -= delta
		return

	raycast.look_at(player.global_position)

	# for some reason, flipping Enemy, the character controller, doesn't work
	# it seems like it gets reset to original scale, maybe during move_and_collide
	# godot bug?
	if state != State.ATTACK:
		if player_direction().x < 0:
			%Flip.scale.x = -1
		else:
			%Flip.scale.x = 1

	match state:
		State.IDLE:
			pass
		State.CHASE:
			chase(delta)
		State.APPROACH:
			approach(delta)
		State.ATTACK:
			attack(delta)
		State.RETREAT:
			retreat(delta)
		State.HOVER:
			hover(delta)
		State.HIT:
			hit_behaviour(delta)
		State.STUN:
			stun(delta)
		State.WANDER:
			pass

	if velocity.length() > 5:
		if sprite.animation == "walk":
			pass
		else:
			sprite.play("walk")
	else:
		if sprite.animation == "idle":
			pass
		else:
			sprite.play("idle")


func chase(delta):
	var diff := player_diff()
	if diff.length() < hover_distance:
		state = State.HOVER
	else:
		move(player.global_position, delta)



func _unhandled_input(event: InputEvent) -> void:
	pass
#	Utils.log_event(event, "_unhandled_input", name)

	#if Base.debug_build and event is InputEventKey and event.is_pressed():
		#if event.keycode == KEY_A:
			#start_attack()


func start_attack():
	var diff := player_diff()
	attack_direction = null
	G.enemy_last_attack = F.time()
	last_attack_time = F.time()
	G.enemy_attackers_num += 1
	Log.d("attacking at", G.enemy_last_attack)
	anim.play("into_attack_stance")
	SoundManager.play("weapon-prep")
	attack_sound_played = false
	#attack_finished = false
	state = State.ATTACK


#var attack_finished = false
var attack_sound_played = false
func attack(delta):
	# only once, set attack line
	if state_time >= 0.1:
		pass
		#enable raycast

	if state_time >= 0.5 and attack_direction == null:
		attack_direction = player_direction()

	if state_time >= 1 and not attack_sound_played:
		SoundManager.play("lunge")
		attack_sound_played = true

	if attack_direction == null:
		%SwordLookAt.look_at(player.global_position)
	#else:
		#%SwordLookAt.look_at(attack_direction * 10)

	if F.time() > last_attack_time + 2:
		reset_attack_anim()
		state = State.STUN
		return

	# prepare time
	if state_time < 1:
		velocity = Vector2.ZERO
		add_wiggle()
	else:
		# if you do += you get acceleration which is nice
		# but shouldn't we do it better than that?
		velocity = attack_direction * attack_speed


	if move_and_slide():
		if state_time > 0.1:
			#print(get_slide_collision_count())
			for i in get_slide_collision_count():
				var collision = get_slide_collision(i)
				var coll = collision.get_collider()
				#print(coll)

				#if coll == player and not attack_finished:
				if coll == player:
					#attack_finished = true
					if player.is_blocking:
						coll.is_hit(self)
						reset_attack_anim()
						state = State.STUN
						return
					else:
						coll.is_hit(self)
						#print("hit")
						state = State.RETREAT

						await F.wait(0.2)
						anim.play_backwards("into_attack_stance")
						%SwordLookAt.rotation = 0
						return



	#if raycast.is_colliding():
		#var coll = raycast.get_collider()
		#print(coll)
		#if coll == player:
			#if player.is_blocking:
				#state = State.STUN
				#SoundManager.play("shield-hit")
				#G.shake.add_trauma(0.5)
			#else:
				#coll.is_hit()
				#state = State.RETREAT

	##F.process_print(self, player_diff().length(), 0.2)
	#print(player_diff().length())
	#if player_diff().length() < touching_distance:
		#state = State.RETREAT




func retreat(delta):
	var diff := player_diff()
	if diff.length() > hover_distance * 0.9 and diff.length() < hover_distance * 1.1:
		state = State.HOVER
		return
	else:
		if attack_direction:
			velocity = -attack_direction * attack_speed
		else:
			velocity = -diff.normalized() * attack_speed
		move_and_slide()


func approach(delta):
	var diff := player_diff()
	var direction = player_direction()

	if diff.length() < hover_distance * 0.9:
		player_too_close_timer += delta
	else:
		player_too_close_timer = 0.0

	if player_too_close_timer > 2.5:
		player_too_close_timer = 0.0
		state = State.HOVER
		return
	if diff.length() > hover_distance * 0.9 and diff.length() < hover_distance * 1.1:
		state = State.HOVER
		return
	else:
		move(player.global_position - (direction * hover_distance), delta)


func player_diff() -> Vector2:
	return player.global_position - global_position

func player_direction() -> Vector2:
	return player_diff().normalized()

func add_wiggle():
	var direction = player_direction()

	# wiggle in place
	#var hover_pos = player.global_position - (direction * hover_distance)
	var wiggle = sin(state_time * TAU) * -direction
	#var wiggle = sin(F.time() * TAU) * directiont

	#move(state_start_position + wiggle, delta)

	velocity += -(wiggle.normalized()) * wiggle_speed


func add_distance_keeping():
	for enemy in get_tree().get_nodes_in_group("enemy"):
		if enemy == self:
			return
		var diff = enemy.global_position - global_position
		#print(diff)
		if diff.length() < distance_keeping_distance:
			velocity += -diff.normalized() * distance_keeping_speed


func play_stun_effect():
	sprite.material.set_shader_parameter("active", true)
	await F.wait(0.05)
	sprite.material.set_shader_parameter("active", false)
	await F.wait(0.05)
	sprite.material.set_shader_parameter("active", true)
	await F.wait(0.05)
	sprite.material.set_shader_parameter("active", false)


var stun_effect_done = false
func stun(delta):
	sprite.play("idle")
	velocity = Vector2.ZERO

	if not stun_effect_done:
		play_stun_effect()
		stun_effect_done = true
		return

	if state_time > 1:
		stun_effect_done = false
		state = State.APPROACH


func hover(delta):
	if state_time > attack_time + attack_after_hover_randf * 1.0:
		if not player.is_dead and G.enemy_attackers_num < max_simultaneous_attacks:
			if F.time() > G.enemy_last_attack + attack_after_hover_randf * 0.5:
				start_attack()
				return

	#move(get_circle_position(G.game.player), delta)
	var diff := player_diff()
	var direction = player_direction()

	if diff.length() > hover_distance * 1.5:
		state = State.APPROACH
		return
	#if diff.length() < hover_distance * 0.75:
		#velocity = -direction * speed
		#move_and_slide()

	velocity = Vector2.ZERO
	add_wiggle()
	add_distance_keeping()
	move_and_slide()


func reset_attack_anim():
	%SwordLookAt.rotation = 0
	anim.play("RESET")

func is_hit():
	if state == State.ATTACK or state == State.STUN:
		SoundManager.play("enemy-hit")
		#$Sprite2D.modulate = Color(10,10,10,10)
		health -= 1
		if health <= 0:
			queue_free()
			#G.game.call_deferred("check_game_over")
			G.game.enemy_died()

		if state == State.ATTACK:
			reset_attack_anim()

		#var tween: Tween = create_tween()
		#tween.tween_property($Sprite2D, "modulate:v", 1, 0.25).from(15)
		#state = State.HIT
		state = State.HIT

		sprite.material.set_shader_parameter("active", true)
		await F.wait(0.1)
		sprite.material.set_shader_parameter("active", false)
	else:
		SoundManager.play("enemy-block")


func hit_behaviour(delta):
	velocity = -player_direction() * knockback_speed
	move_and_slide()
	if state_time > knockback_duration:
		velocity = Vector2.ZERO
		move_and_slide()
		state = State.APPROACH
		pause = 0.2


func move(target: Vector2, delta):
	var direction = (target - global_position).normalized()
	var desired_velocity =  direction * speed
	#var steering = (desired_velocity - velocity) * delta * 2.5
	var steering = (desired_velocity - velocity) * delta
	velocity += steering
	add_distance_keeping()
	move_and_slide()

func get_circle_position(center: Node2D):
	var kill_circle_centre = G.game.player.global_position
	var radius = 40
	 #Distance from center to circumference of circle
	var angle = circle_angle
	var x = kill_circle_centre.x + cos(angle) * radius;
	var y = kill_circle_centre.y + sin(angle) * radius;

	return Vector2(x, y)
