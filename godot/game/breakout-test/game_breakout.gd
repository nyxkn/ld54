extends Node2D


# this is your main game loop
# if you are changing levels or entering menus, they should probably be added and removed from here
# rather than changing scene
# you only want to change scene to non-game things like main menu and end screen
# i.e. when you can afford to unload everything game-related


var playtime_stopwatch: Stopwatch = Stopwatch.new()
var score: float = 0.0

var cheats_enabled := false
var cheats := {}

var stats := {
	"play_time": 0.0,
}

var state: State
class State:
	var score: float = 0.0


func _enter_tree() -> void:
	pass


func _ready() -> void:
	Pause.allow_from(self)
	# to set a background color without adding colorrects
	RenderingServer.set_default_clear_color(Color("9bbc0f"))

	# it's good practice to start logic in its own function, so that it can be called on restart
	# _ready should only be for node initialization things
	# it's also good practice to let logic run decoupled from _ready
	# sometimes other functions wait for _ready signal to know if loading is complete
	# but if you start the logic before running _ready, it might throw things off
	call_deferred("game_setup")


func _exit_tree() -> void:
	pass


#func _input(event: InputEvent) -> void:
#	pass
##	Utils.log_event(event, "_input", name)


#func _gui_input(event: InputEvent) -> void:
#	pass
##	Utils.log_event(event, "_gui_input", name)


func _unhandled_input(event: InputEvent) -> void:
	pass
#	Utils.log_event(event, "_unhandled_input", name)

	if Base.debug_build and event is InputEventKey and event.is_pressed():
		if event.keycode == KEY_K:
			get_tree().reload_current_scene()
		if event.keycode == KEY_E:
			game_over()
		if event.keycode == KEY_H: # haxxor!
			cheats_enabled = !cheats_enabled
#		if event.keycode == KEY_: # moar hax!
#			add_score(100)



#func _unhandled_key_input(event: InputEvent) -> void:
#	pass
##	Utils.log_event(event, "_unhandled_key_input", name)


func _process(delta: float) -> void:
	pass


# always assume you start from a freshly initialized scene
# absolutely avoid restarting by manually deconstructing and reconstructing
# just queue_free and re instance
func game_setup() -> void:
	state = State.new()
	G.gamestate = state
	G.game = self
	game_start()


func game_start() -> void:
	add_child(playtime_stopwatch)
	playtime_stopwatch.start()

	G.music_player.goto_section("Main", NDef.When.LOOP)

	# play music
	# enable controls

func check_game_over():
	if %BlocksBoard.get_child_count() == 0:
		game_over()

func block_is_hit(block):
	add_score(100)
	block.queue_free()
	check_game_over()

func game_over():
	playtime_stopwatch.stop()
#	Score.new_score(score)
	Score.new_score(score, stats)

	stats.play_time = playtime_stopwatch.time

	F.change_scene(Config.scenes.ending, { 'stats': stats })
	#F.change_scene(Config.scenes.leaderboard)


func add_score(change):
	score += change

	updade_ui()

	# basic score animation
#	var s = %Score
#	var label = s.get_node("Value")
#   when you scale, you have to adjust pivot offset first
#	label.pivot_offset = label.size / 2.0
#	var t = create_tween().set_trans(Tween.TRANS_SINE)
#	t.tween_property(label, "scale", Vector2(1.8, 1.8), 0.1)
#	t.tween_property(label, "scale", Vector2.ONE , 0.1)


func pay(amount):
	add_score(-amount)


func updade_ui():
	%Score.value = score
