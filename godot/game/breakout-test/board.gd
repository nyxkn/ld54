extends Node2D

const Block = preload("res://game/block.tscn")

func _ready() -> void:
	setup()


func _process(delta: float) -> void:
	pass


func setup():
	for x in 10:
		for y in 10:
			var block = Block.instantiate()
			block.position.x = x * 52 + (1280 / 2) - (10 * 52 / 2) + (48 / 2)
			#block.position.y = y * 20 + (720 / 2) - (10 * 20 / 2)
			block.position.y = y * 20 + 64
			block.name = "Block"
			add_child(block)
