extends StaticBody2D


const SPEED = 500.0

var velocity := Vector2.ZERO

func _ready() -> void:
	pass


func shoot(dir):
	pass
	#velocity.x = F.rng.randf()
	#velocity.y = F.rng.randf()
	#velocity = velocity.normalized()
	velocity = dir.normalized()


func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_click"):
		var diff = get_global_mouse_position() - global_position
		shoot(diff)


func _physics_process(delta: float) -> void:
	var collision := move_and_collide(velocity * SPEED * delta)

	if collision:
		var collider := collision.get_collider()
		var n := collision.get_normal()
		print(collider.name)
		#if collider.name.begins_with("Wall"):
		velocity = velocity.bounce(n)

		if collider.is_in_group("block"):
			G.game.block_is_hit(collider)
			#collider.is_hit()
