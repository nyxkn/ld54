extends Sprite2D

var blink_speed = 0.5
var time = 0.0
func _ready() -> void:
	visible = false
	set_process(false)


func _process(delta: float) -> void:
	if F.time() > time + blink_speed:
		visible = not visible
		time = F.time()


func blink():
	visible = true
	time = F.time()
	set_process(true)
