extends Node

## class that represents game state, accessible from anywhere
## it doesn't have to be a separate file. a class block in game.gd is also fine. but this gives you better completion
## this differs from using global in that it's specific to game state
## and not meant to survive outside of the game (e.g. other screens)
## came from an idea i saw in ld52 forked up. not sure about its usefulness but adding it here so maybe we can try
## this should not be an autoload, but rather you can create an instance and add it to Global autoload
## making this an autoload means it's hard to reset
