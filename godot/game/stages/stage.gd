extends Node

var wave = 0
@onready var waves = %Waves.get_children()


func _ready() -> void:
	for child in %Waves.get_children():
		child.show()

	await F.wait(1)
	activate_wave(0)


func activate_wave(n):
	for enemy in waves[n].get_children():
		enemy.activate()

func _process(delta: float) -> void:
	pass


func check_wave_clear():
	if waves[wave].get_child_count() == 0:
		wave += 1
		if wave >= waves.size():
			G.game.stage_cleared()
		else:
			activate_wave(wave)
