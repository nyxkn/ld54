extends Node

# this is meant to be an autoload
# define global variables in here
# this is also useful for static variables for classes (can't have them inside the actual classes)

# for stateless definitions, use definitions.gd instead


var totaltime_stopwatch := StopwatchRT.new()

var playtime_stopwatch: Stopwatch = Stopwatch.new()

var game
var gamestate
var shake

var enemy_last_attack := 0.0
var enemy_attackers_num := 0

@onready var music_player: MusicPlayer = $MusicPlayer

func _ready() -> void:
	pass
	# waiting for setup complete, because this otherwise gets loaded before nframework finishes setup
	await F.setup_completed

	add_child(playtime_stopwatch)

	await F.wait(1)
	G.music_player.play_and_switch("Song", "Intro")


func start_stopwatch():
	playtime_stopwatch.start()

