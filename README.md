# Game Title

*This is my Compo submission for [Ludum Dare 54](https://ldjam.com/events/ludum-dare/54/corridor-duels). Theme: Limited Space.*

*Play it on [itch.io](https://nyxkn.itch.io/corridor-duels).*

## License

### Source code

All source code is licensed under the terms of the [GPL-3.0-only License](https://spdx.org/licenses/GPL-3.0-only.html).

### Assets

All assets (images and audio files) are licensed under the [CC-BY-SA 4.0 License](https://creativecommons.org/licenses/by-sa/4.0/).

This includes everything in the *assets-source*, *media*, and *godot/assets* folders.
